﻿using System;
using System.Collections.Generic;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;

namespace PpM3sAddIn.DocAdapter.ContentBridge
{
    public class PpContentBridgeUtf8 : OfficeCommon.IContentBridge
    {
        private readonly PpTextRangeIdMgr _idFragmentMgr;

        public PpContentBridgeUtf8(PpTextRangeIdMgr idFragmentMgr)
        {
            this._idFragmentMgr = idFragmentMgr;
        }

        public string ToBase64(int id)
        {
            IdRange<Microsoft.Office.Interop.PowerPoint.TextRange> idRange = _idFragmentMgr.GetTextRangeById(id);
            string str = Helper.RemoveTerminatingLineBreak(idRange.TextRange.Text).Trim();
            return Helper.EncodeUtf8ToBase64(str);
        }


        public void Replace(int id, Realization real)
        {

            String base64Str = real.Base64Data;
            String text = Helper.DecodeBase64ToUtf8(base64Str);

            IdRange<Microsoft.Office.Interop.PowerPoint.TextRange> texTRangeWithId = _idFragmentMgr.GetTextRangeById(id);
            texTRangeWithId.TextRange.Text = text;

        }

        public void Insert(List<int> previousFragmentIds, int insertFragmentId, Realization real, int level, List<IdRangeDescription> idRangeDescs)
        {
            Logger.Log("PpContentBridgeUtf8 Insert  throw new System.NotImplementedException()", LogLevel.Error);
        }

    }
}