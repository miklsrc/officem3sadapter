﻿using System;
using System.Collections.Generic;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement;

namespace PpM3sAddIn.DocAdapter.ContentBridge
{
    public class PpContentBridgeImage : OfficeCommon.IContentBridge
    {
        private readonly PpNoTextShapeIdMgr _idFragmentMgr;

        public PpContentBridgeImage(PpNoTextShapeIdMgr idFragmentMgr)
        {
            this._idFragmentMgr = idFragmentMgr;
        }

        public string ToBase64(int id)
        {
            IdShape<Microsoft.Office.Interop.PowerPoint.Shape> idShape = _idFragmentMgr.getIdShape(id);
            if (!PpHelper.shapeHasPicture(idShape.Shape))
            {
                throw new Exception("no image found in shape");
            }

            String shapeImgBase64Data = PpHelper.ConvertShapeToJpgBase64(idShape.Shape);
            return shapeImgBase64Data;
        }

        public void Replace(int id, Realization real)
        {
            //TODO
        }

        public void Insert(List<int> previousFragmentIds, int insertFragmentId, Realization real, int level, List<IdRangeDescription> idRangeDescs)
        {
            throw new NotImplementedException();
        }


    }
}