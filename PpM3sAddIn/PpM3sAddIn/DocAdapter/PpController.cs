﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OfficeCommon;
using PpM3sAddIn.DocAdapter.IdManagement;
using PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;
using PpM3sAddIn.DocAdapter.Persistence;
using OfficeCommon.Gui;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace PpM3sAddIn.DocAdapter
{
    class PpController : Controller<PowerPoint.Presentation, PpIdFragmentMgrFacade>
    {
        private PpPresentationAdapter _ppPresentationAdapter;
        private PpIdFragmentMgrFacade _ppIdFragmentMgrFacade;

        public PpController(PpPresentationAdapter ppPresentationAdapter, PpIdFragmentMgrFacade ppIdFragmentMgrFacade) : base(ppPresentationAdapter)
        {
            this._ppPresentationAdapter = ppPresentationAdapter;
            this._ppIdFragmentMgrFacade = ppIdFragmentMgrFacade;

            actions.Add("Assign H1 to Titles", ActionAssignH1ToTitles);

           
        }
 


        private void ActionAssignH1ToTitles(object sender, EventArgs e)
        {
            _ppIdFragmentMgrFacade.RefreshFragments();

            List<PpTextBox> ppTextBoxs = _ppIdFragmentMgrFacade.TextRangeIdMgr.TextBoxes;
            foreach (var textBox in ppTextBoxs)
            {
                if (PpHelper.IsTitleShape(textBox.Shape) && textBox.TextRanges.Count == 1)
                {
                    _ppPresentationAdapter.FragmentPropertiesMngr.GetPropertiesFor(textBox.TextRanges.First().Id).HeadingLevel = 1;
                }
            }
        }



    }
}
