using System;
using OfficeCommon;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.Service_References.M3s;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace PpM3sAddIn.DocAdapter.IdManagement
{

    /// <summary>
    /// usually a plain IdFragment has no connection to its containing shape or slide. This Wrapper is required, when it is necessary to navigate from an IdFragment to its containing shape or slide.
    /// </summary>
    public abstract class IdFragmentAffiliation
    {
        public PowerPoint.Shape Shape;
        public PowerPoint.Slide Slide;

        public abstract IdFragment IdFragment { get; }

        public abstract Sharedcontent CreateRealization();

        protected IdFragmentAffiliation(PowerPoint.Shape shape, PowerPoint.Slide slide)
        {
            Shape = shape;
            Slide = slide;
        }

        public int Id => IdFragment.Id;
    }

    public class IdRangeAffiliation : IdFragmentAffiliation
    {
        private IdRange<PowerPoint.TextRange> _idRange;

        public IdRangeAffiliation(PowerPoint.Shape shape, PowerPoint.Slide slide, IdRange<PowerPoint.TextRange> idRange) : base(shape,slide)
        {
            _idRange = idRange;
        }

        public override Sharedcontent CreateRealization()
        {
            Sharedcontent sc = new Sharedcontent();
            sc.type = "utf8";

            String str = Helper.RemoveTerminatingLineBreak(_idRange.TextRange.Range.Text);
            String data = Helper.EncodeUtf8ToBase64(str);

            sc.data = data;
            sc.version = 0;
            return sc;
        }

        public override IdFragment IdFragment => _idRange;
    }

    public class IdShapeAffiliation : IdFragmentAffiliation
    {
        protected IdShape<PowerPoint.Shape> _idShape;

        public IdShapeAffiliation(PowerPoint.Shape shape, PowerPoint.Slide slide, IdShape<PowerPoint.Shape> idShape)
            : base(shape, slide)
        {
            _idShape = idShape;
        }

        public override Sharedcontent CreateRealization()
        {
            Sharedcontent sc = new Sharedcontent();
            sc.type = "unknown_powerpoint_type";
            
            sc.data = "";
            sc.version = 0;
            return sc;
        }

        public override IdFragment IdFragment => _idShape;
    }

    public class IdImageAffiliation : IdShapeAffiliation
    {
        public IdImageAffiliation(PowerPoint.Shape shape, PowerPoint.Slide slide, IdShape<PowerPoint.Shape> idShape)
            : base(shape, slide, idShape)
        {
        }

        public override Sharedcontent CreateRealization()
        {
            Sharedcontent sc = new Sharedcontent();
            sc.type = "image";

            String data = PpHelper.ConvertShapeToJpgBase64(_idShape.Shape);
            
            sc.data = data;
            sc.version = 0;
            return sc;
        }
    }
}