﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.PowerPoint;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;

namespace PpM3sAddIn.DocAdapter.IdManagement
{

    public class PpNoTextShapeIdMgr : AbstractIdFragmentMgr
    {
        public Presentation Presentation { get; private set; }
        private List<IdShape<Shape>> _otherShapes = new List<IdShape<Shape>>();

        public PpNoTextShapeIdMgr(Presentation presentation,IdCounter idCounter) : base(idCounter)
        {
            Presentation = presentation;
        }

        public List<IdShape<Shape>> OtherShapes => _otherShapes;

        public override void RefreshFragments()
        {
            List<KeyValuePair<Shape, Slide>> otherShapes = new List<KeyValuePair<Shape, Slide>>();

            foreach (Slide sl in Presentation.Slides)
            {
                foreach (Shape shape in sl.Shapes)
                {
                    if (shape.HasTextFrame != Microsoft.Office.Core.MsoTriState.msoTrue)
                    {
                        otherShapes.Add(new KeyValuePair<Shape, Slide>(shape, sl));
                    }
                }
            }
            RefreshOtherShapes(otherShapes);
        }

        public override HashSet<int> GetAllFragmentIds()
        {
            HashSet<int> fragmentIds = new HashSet<int>();

            foreach (var fragment in _otherShapes)
            {
                if (!fragmentIds.Add(fragment.Id))
                {
                    throw new Exception("duplicate ids");
                }
            }

            return fragmentIds;
        }

        private void RefreshOtherShapes(List<KeyValuePair<Shape, Slide>> currentOtherShapes)
        {
            // remove old
            for (int i = _otherShapes.Count - 1; i >= 0; i--)
            {
                Shape oldOtherShape = _otherShapes[i].Shape;
                if (currentOtherShapes.Count(n => n.Key == oldOtherShape) == 0)
                {
                    int removeId = _otherShapes[i].Id;
                    _otherShapes.RemoveAt(i);
                    FireIdFragmentRemovedEvent(removeId);
                }
            }

            //add new
            foreach (KeyValuePair<Shape, Slide> curOtherShapeOnSlide in currentOtherShapes)
            {
                if (_otherShapes.Count(n => n.Shape == curOtherShapeOnSlide.Key) == 0)
                {
                    int newId = IdCounter.getNewId();
                    _otherShapes.Add(new IdShape<Shape>(curOtherShapeOnSlide.Key, newId));
                    FireIdFragmentAddedEvent(newId);
                }
            }
        }



        public void assignIdToOtherShape(int id, int slideindex, string name)
        {
            IdShape<Shape> otherShape = _otherShapes.Find(n => PpHelper.GetSlideFromShape(n.Shape).SlideIndex == slideindex && n.Shape.Name == name);

            if (otherShape == null)
            {
                throw new Exception("other shape not found");
            }
            otherShape.Id = id;

        }

        public IdShape<Shape> getIdShape(Shape shape)
        {
            return _otherShapes.Find(n => n.Shape == shape);
        }

        internal IdShape<Shape> getIdShape(int id)
        {
            return _otherShapes.Find(n => n.Id == id);
        }
    }

    
}
