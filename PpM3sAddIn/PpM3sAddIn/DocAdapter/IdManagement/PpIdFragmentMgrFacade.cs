using System;
using System.Collections.Generic;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;
using Shape = Microsoft.Office.Interop.PowerPoint.Shape;

namespace PpM3sAddIn.DocAdapter.IdManagement
{
    public class PpIdFragmentMgrFacade : IdFragmentMgrFacade
    {
        public PpNoTextShapeIdMgr NoTextShapeIdMgr { get; private set; }
        public PpTextRangeIdMgr TextRangeIdMgr { get; private set; }

        public PpIdFragmentMgrFacade(Presentation presentation)
        {
            TextRangeIdMgr = new PpTextRangeIdMgr(presentation, IdCounter);
            NoTextShapeIdMgr = new PpNoTextShapeIdMgr(presentation, IdCounter);

            AddIdFragmentMgr(NoTextShapeIdMgr);
            AddIdFragmentMgr(TextRangeIdMgr);
        }

        public void FetchSortedRealizations(out Dictionary<int, Realization> realizations, out Dictionary<int, int> slideRelations)
        {

            realizations = new Dictionary<int, Realization>();
            slideRelations = new Dictionary<int, int>(); //<id,slidenr>

            foreach (Slide sl in TextRangeIdMgr.Presentation.Slides)
            {
                //copying all shapes to a list, it's nessecary to sort them
                List<Shape> shapes = new List<Shape>();
                foreach (Shape shape in sl.Shapes)
                {
                    shapes.Add(shape);
                }
                shapes.Sort((x, y) => x.Top.CompareTo(y.Top));

                //
                foreach (Shape shape in shapes)
                {
                    //leave out footer elements
                    if (PpHelper.IsFooterHeaderShape(shape))
                    {
                        continue;
                    }

                    if (shape.HasTextFrame == MsoTriState.msoTrue)
                    {
                        PpTextBox textBox = TextRangeIdMgr.GetTextBoxForShape(shape);
                        foreach (IdRange<TextRange> range in textBox.TextRanges)
                        {
                            realizations.Add(range.Id, Utf8Realization.FromUtf8String(range.TextRange.Text));
                            slideRelations.Add(range.Id, sl.SlideNumber);
                        }

                    }
                    else
                    {
                        IdShape<Shape> idShape = NoTextShapeIdMgr.getIdShape(shape);

                        if (PpHelper.shapeHasPicture(shape))
                        {
                            //TODO : at the moment  each call of this method triggers a expensive hard drive image function
                            // refactor: derive from ImageRealization a new PpImageRealization which gets a shape-reference and only calls the base64 conversione when really commiting
                            String shapeImgBase64Data = PpHelper.ConvertShapeToJpgBase64(shape);
                            realizations.Add(idShape.Id, ImageRealization.FromBase64(shapeImgBase64Data));

                        }
                        else
                        {
                            String nodeType = "unknown_powerpoint_type";
                            realizations.Add(idShape.Id, new EmptyRealization("", nodeType));

                        }
                        slideRelations.Add(idShape.Id, sl.SlideNumber);
                    }


                }
            }
        }


    }
}