﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Office.Core;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing
{

    public class PpTextRangeIdMgr : AbstractTextRangeIdMgr<PowerPoint.TextRange>
    {
        private List<PpTextBox> ppTextBoxes = new List<PpTextBox>();
        private PowerPoint.Presentation _presentation;
        private PowerPoint.Shape _currentProcessedShape;

        public PpTextRangeIdMgr(PowerPoint.Presentation presentation, IdCounter idCounter)
            : base(idCounter)
        {
            _presentation = presentation;
        }

        public PowerPoint.Presentation Presentation => _presentation;

        public List<PpTextBox> TextBoxes => ppTextBoxes;

        public PpTextBox GetTextBoxForShape(PowerPoint.Shape shape)
        {
            return ppTextBoxes.Find(n => n.Shape == shape);
        }

        protected override void  DoUpdateFragments()
        {
            var currentTextFrameShapes = GetCurrentTextFrameShapes();

            List<PpTextBox> currentTextRangesPerBox = getCurrentTextRangesPerBox(currentTextFrameShapes);
            List<KeyValuePair<PpTextBox, PpTextBox>> assignedOldAndCurTextBoxes = assignOldWithCurrentTextBoxes(currentTextRangesPerBox);

            //add missing new ones
            addNewTextBoxes(currentTextRangesPerBox);

            //add missing new ones
            clearRemovedTextBoxes(ppTextBoxes);

            //repair old text range base
            deleteEmptyRanges(); //not needed, because empty ranges get merged in the combineMergedRanges step

            var newAddedRangesPerBoxes = addNewInsertedRanges(assignedOldAndCurTextBoxes);

            fixAtRageStartInsertedText(newAddedRangesPerBoxes);

            combineMergedRanges(assignedOldAndCurTextBoxes);
            CutOffOverlappingRanges(assignedOldAndCurTextBoxes);

            checkTextRangeLegality(assignedOldAndCurTextBoxes);
        }

        protected override void PostProcessAssignForcedIds(IdManager idManager)
        {

            foreach (ShapeIdRangeDescription forcedIdDesc in idManager.GetForcedIdAssignments<ShapeIdRangeDescription>())
            {
                var ppTextBox = this.ppTextBoxes.Find(n => n.Shape == forcedIdDesc.Shape);
                var idRange = ppTextBox.GetIdRange(forcedIdDesc.Start, forcedIdDesc.End);
                idRange.Id = forcedIdDesc.Id;
            }
        }

        protected override IdFragmentDescription FindAndRemoveIdRangeAssignment(IdRange<PowerPoint.TextRange> curRange, IdManager idManager)
        {
            var forcedIdDescs = idManager.GetForcedIdAssignments<ShapeIdRangeDescription>(); ;
            ShapeIdRangeDescription idRangeDesc = forcedIdDescs.Find(n => n.Start == curRange.TextRange.Start && n.End == curRange.TextRange.End && _currentProcessedShape == n.Shape);
            idManager.RemoveForcedIdAssignment(idRangeDesc);
            return idRangeDesc;
        }

        public override IdRange<PowerPoint.TextRange> GetTextRangeById(int searchId)
        {
            return ppTextBoxes.SelectMany(ppTextBox => ppTextBox.TextRanges).FirstOrDefault(idRange => idRange.Id == searchId);
        }

        public PowerPoint.Shape GetRelatedShape(int id)
        {
            foreach (var textBox in ppTextBoxes)
            {
                if (textBox.TextRanges.Any(x => x.Id == id))
                {
                    return textBox.Shape;
                }
            }
            return null;
        }

        public override HashSet<int> GetAllFragmentIds()
        {
             HashSet<int> fragmentIds = new HashSet<int>();

            foreach (var ppTextBox in ppTextBoxes)
            {
                foreach (var fragment in ppTextBox.TextRanges)
                {
                    if (!fragmentIds.Add(fragment.Id))
                    {
                        throw new Exception("duplicate ids");
                    }
                }
            }
            return fragmentIds;
        }


        private List<KeyValuePair<PowerPoint.Shape, PowerPoint.Slide>> GetCurrentTextFrameShapes()
        {
            var currentTextFrameShapes = new List<KeyValuePair<PowerPoint.Shape, PowerPoint.Slide>>();

            foreach (PowerPoint.Slide sl in _presentation.Slides)
            {
                foreach (PowerPoint.Shape shape in sl.Shapes)
                {
                    if (shape.HasTextFrame == MsoTriState.msoTrue && !PpHelper.IsFooterHeaderShape(shape))
                    {
                        currentTextFrameShapes.Add(new KeyValuePair<PowerPoint.Shape, PowerPoint.Slide>(shape, sl));
                    }
                }
            }
            return currentTextFrameShapes;
        }

        private void clearRemovedTextBoxes(List<PpTextBox> ppTextBoxes)
        {
            PpTextBox currentTextBox;
            for (int i = ppTextBoxes.Count-1; i>=0; i--)
            {
                currentTextBox = ppTextBoxes[i];
                if (!PpHelper.ShapeExistOnSlide(currentTextBox.Shape, currentTextBox.RelatedSlide))
                {
                    foreach (IdRange<PowerPoint.TextRange> idRange in currentTextBox.TextRanges)
                    {
                        base.FireIdFragmentRemovedEvent(idRange.Id);
                    }

                    ppTextBoxes.RemoveAt(i);
                }
            }

        }

        private List<PpTextBox> getCurrentTextRangesPerBox(List<KeyValuePair<PowerPoint.Shape, PowerPoint.Slide>> textFrameShapes)
        {
            List<PpTextBox> rangesPerBox = new List<PpTextBox>();

            foreach (KeyValuePair<PowerPoint.Shape, PowerPoint.Slide> shapeOnSlide in textFrameShapes)
            {
                rangesPerBox.Add(new PpTextBox(shapeOnSlide.Key, shapeOnSlide.Value));
            }
            return rangesPerBox;
        }

        ///
        /// returns a list of the old textboxes assigned with their current equivalent
        /// 
        private List<KeyValuePair<PpTextBox, PpTextBox>> assignOldWithCurrentTextBoxes(List<PpTextBox> curRangesPerBox)
        {
            List<KeyValuePair<PpTextBox, PpTextBox>> assignedBoxes = new List<KeyValuePair<PpTextBox, PpTextBox>>();

            foreach (PpTextBox curTextBox in curRangesPerBox)
            {
                foreach (PpTextBox oldTextBox in ppTextBoxes)
                {
                    if (curTextBox.Shape == oldTextBox.Shape)
                    {
                        assignedBoxes.Add(new KeyValuePair<PpTextBox, PpTextBox>(oldTextBox, curTextBox));
                    }
                }
            }

            return assignedBoxes;
        }

        private void deleteEmptyRanges()
        {
            foreach (PpTextBox textBox in ppTextBoxes)
            {
                base.DeleteEmptyRanges(textBox);
            }
        }

        private void checkTextRangeLegality(List<KeyValuePair<PpTextBox, PpTextBox>> assignedOldAndCurTextBoxes)
        {
            foreach (KeyValuePair<PpTextBox, PpTextBox> oldAndNewTextbox in assignedOldAndCurTextBoxes)
            {
                PpTextBox oldTextBox = oldAndNewTextbox.Key;
                PpTextBox curTextBox = oldAndNewTextbox.Value;

                base.CheckTextRangeLegality(oldTextBox, curTextBox);
            }
        }

        /// <summary>
        /// example: when there are two fallowing old ranges OldR1 and OldR2, they are splittet with a LineBreak-char at the end of OldR1.
        /// after deleting this LineBreak-char, the text of the Fallowing OldR2 jumps up and the both Ranges get merged to a new Range NewR.
        /// 
        /// this method deletes all old ranges involved in the merge and assigns to the new emerged range an ID from a old ranges
        /// </summary>
        private void combineMergedRanges(List<KeyValuePair<PpTextBox, PpTextBox>> assignedOldAndCurTextBoxes)
        {
            foreach (KeyValuePair<PpTextBox, PpTextBox> oldAndNewTextbox in assignedOldAndCurTextBoxes)
            {
                PpTextBox oldTextBox = oldAndNewTextbox.Key;
                PpTextBox curTextBox = oldAndNewTextbox.Value;

                base.combineMergedRanges(oldTextBox, curTextBox);

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>a pseudo map with current textbox and additionally all new inserted ranges</returns>
        private List<KeyValuePair<PpTextBox, List<IdRange<PowerPoint.TextRange>>>> addNewInsertedRanges(List<KeyValuePair<PpTextBox, PpTextBox>> assignedOldAndCurTextBoxes)
        {
            List<KeyValuePair<PpTextBox, List<IdRange<PowerPoint.TextRange>>>> newAddedRangesPerBox = new List<KeyValuePair<PpTextBox, List<IdRange<PowerPoint.TextRange>>>>();

            foreach (KeyValuePair<PpTextBox, PpTextBox> oldAndNewTextbox in assignedOldAndCurTextBoxes)
            {
                PpTextBox oldTextBox = oldAndNewTextbox.Key;
                PpTextBox curTextBox = oldAndNewTextbox.Value;

                _currentProcessedShape= oldTextBox.Shape;

                List<IdRange<PowerPoint.TextRange>> newAddedRanges = base.AddNewInsertedRanges(oldTextBox, curTextBox);

                if (newAddedRanges.Count > 0)
                {
                    newAddedRangesPerBox.Add(new KeyValuePair<PpTextBox, List<IdRange<PowerPoint.TextRange>>>(curTextBox, newAddedRanges));
                }

            }
            return newAddedRangesPerBox;
        }

        private void fixAtRageStartInsertedText(List<KeyValuePair<PpTextBox, List<IdRange<PowerPoint.TextRange>>>> newAddedRangesPerBoxes)
        {
            foreach (KeyValuePair<PpTextBox, List<IdRange<PowerPoint.TextRange>>> newRangesPerBox in newAddedRangesPerBoxes)
            {
                PowerPoint.Shape textBoxShape = newRangesPerBox.Key.Shape;
                List<IdRange<PowerPoint.TextRange>> newRanges = newRangesPerBox.Value;
                PpTextBox oldTextBox = ppTextBoxes.Find(n => n.Shape == textBoxShape);

                base.FixAtRageStartInsertedText(newRanges, oldTextBox);
            }
        }

        private string removeAfterLineBreak(string rangeText)
        {
            if (rangeText.Contains(Constants.LINE_BREAK) && !rangeText.EndsWith(Constants.LINE_BREAK))
            {
                rangeText = rangeText.Substring(0, rangeText.IndexOf(Constants.LINE_BREAK) + Constants.LINE_BREAK.Length);
            }
            return rangeText;
        }

        private void addNewTextBoxes(List<PpTextBox> currentTextRangesPerBox)
        {
            foreach (PpTextBox curTextBox in currentTextRangesPerBox)
            {
                if (!ppTextBoxes.Contains(curTextBox))
                {
                    foreach (IdRange<PowerPoint.TextRange> textRange in curTextBox.TextRanges)
                    {
                        int newId = IdCounter.getNewId();
                        textRange.Id = newId;
                        FireIdFragmentAddedEvent(newId);
                    }

                    ppTextBoxes.Add(curTextBox);
                }
            }
        }

        private void CutOffOverlappingRanges(List<KeyValuePair<PpTextBox, PpTextBox>> assignedOldAndCurTextBoxes)
        {
            foreach (KeyValuePair<PpTextBox, PpTextBox> oldAndNewTextbox in assignedOldAndCurTextBoxes)
            {
                PpTextBox oldTextBox = oldAndNewTextbox.Key;
                PpTextBox curTextBox = oldAndNewTextbox.Value;

                base.CutOffOverlappingRanges(oldTextBox, curTextBox);
            }
        }

    }
}
