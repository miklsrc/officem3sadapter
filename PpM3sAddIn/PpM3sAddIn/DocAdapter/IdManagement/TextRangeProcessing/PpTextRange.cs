﻿using System;
using OfficeCommon.IdManagement.TextRangeProcessing;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing
{
    class PpTextRange : AbstractTextRange<PowerPoint.TextRange>
    {
        private PowerPoint.TextRange range;

        public PpTextRange(PowerPoint.TextRange textRange)
        {
            range = textRange;
        }

        public override String Text
        {
            get => range.Text;
            set => range.Text = value;
        }

        public override int End => range.Start + range.Length;

        public override int Start => range.Start;

        public override int Length => range.Length;

        public override PowerPoint.TextRange Range
        {
            get => range;
            set => range = value;
        }
    }
}
