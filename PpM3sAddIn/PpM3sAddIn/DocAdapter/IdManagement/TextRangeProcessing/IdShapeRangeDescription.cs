﻿using OfficeCommon.IdManagement;

namespace PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing
{
    public class ShapeIdRangeDescription : IdRangeDescription
    {
        public Microsoft.Office.Interop.PowerPoint.Shape Shape { set; get; }

        public ShapeIdRangeDescription(int start, int end, int id, Microsoft.Office.Interop.PowerPoint.Shape shape)
            : base(start, end, id)
        {
            this.Start = start;
            this.End = end;
            this.Id = id;
            this.Shape = shape;
        }
    }
}