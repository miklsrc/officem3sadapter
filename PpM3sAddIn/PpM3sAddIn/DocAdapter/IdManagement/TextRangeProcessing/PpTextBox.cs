﻿using System;
using System.Linq;
using Microsoft.Office.Core;
using OfficeCommon.IdManagement.TextRangeProcessing;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing
{
    public class PpTextBox : TextRangesContainer<PowerPoint.TextRange>
    {
        private PowerPoint.Shape _shape;
        private PowerPoint.Slide _slide;


        public PowerPoint.Slide RelatedSlide => _slide;

        public PowerPoint.Shape Shape
        {
            get => _shape;
            set { _shape = value; }
        }

        public PpTextBox(PowerPoint.Shape shape, PowerPoint.Slide slide)
        {
            _shape = shape;

            foreach (PowerPoint.TextRange textRange in shape.TextFrame.TextRange.Paragraphs())
            {
                textRanges.Add(new IdRange<PowerPoint.TextRange>(new PpTextRange(textRange)));
            }

            _slide = slide;
        }

        public override bool Equals(Object other)
        {
            if (other == null)
                return false;

            PpTextBox otherTextBox = other as PpTextBox;
            if ((Object)otherTextBox == null)
            {
                return false;
            }

            return otherTextBox._shape == _shape;
        }


    }
}
