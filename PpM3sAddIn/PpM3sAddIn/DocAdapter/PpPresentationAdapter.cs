﻿using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Microsoft.Office.Core;
using OfficeCommon;
using OfficeCommon.Gui;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement;
using PpM3sAddIn.DocAdapter.Gui;
using PpM3sAddIn.DocAdapter.Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PpM3sAddIn.DocAdapter.ContentBridge;
using CustomTaskPane = Microsoft.Office.Tools.CustomTaskPane;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace PpM3sAddIn.DocAdapter
{
    //
    public class PpPresentationAdapter : InstanceAdapter
    {
        private PpSideBar _sideBar;
        private PowerPoint.Presentation _ppInstance;
        public PpIdFragmentMgrFacade PpIdFragmentMgrFacade { get; }
        public override AbstractIdFragmentMgr IdFragmentMgr => PpIdFragmentMgrFacade;

        public PpPresentationAdapter(PowerPoint.Presentation instance)
            : this(new PpIdFragmentMgrFacade(instance), new FragmentPropertiesMngr())
        {
            _ppInstance = instance;
        }

        private PpPresentationAdapter(PpIdFragmentMgrFacade ppIdFragmentMgrFacade, FragmentPropertiesMngr fragmentPropertiesMngr)
            : base(new PpPresentationPersistence(fragmentPropertiesMngr, ppIdFragmentMgrFacade), fragmentPropertiesMngr)
        {
            this.PpIdFragmentMgrFacade = ppIdFragmentMgrFacade;
            this.FragmentPropertiesMngr = fragmentPropertiesMngr;

            ContentBridgeInvoker.RegisterContentBridge(Utf8Realization.TYPE, new PpContentBridgeUtf8(PpIdFragmentMgrFacade.TextRangeIdMgr));
            ContentBridgeInvoker.RegisterContentBridge(ImageRealization.TYPE, new PpContentBridgeImage(PpIdFragmentMgrFacade.NoTextShapeIdMgr));
        }



        public void SlideChanged(PowerPoint.SlideRange sldrange)
        {
            _sideBar.slideChanged(sldrange);
        }

        public override string GetFilePath()
        {
            return _ppInstance.FullName;
        }

        public override DocumentProperties GetDocumentProperties()
        {
            return (Microsoft.Office.Core.DocumentProperties)_ppInstance.BuiltInDocumentProperties;
        }


        protected override CustomTaskPane InitTaskPane()
        {
            PpController controller = new PpController(this, PpIdFragmentMgrFacade);

            _sideBar = new PpSideBar(this);
            _sideBar.AddUserActions(controller.GetUserActions());

            return Globals.ThisAddIn.CustomTaskPanes.Add(_sideBar, "M3S Extension");
        }

        protected override void RefreshM3SDocsStructureBase()
        {
            IdFragmentMgr.RefreshFragments();
            PpIdFragmentMgrFacade.FetchSortedRealizations(out Dictionary<int, Realization> realizations, out Dictionary<int, int> slideRelations);
            var headings = new Dictionary<int, int>();
            var groups = new Dictionary<int, int>();

            foreach (var fragment in realizations)
            {
                FragmentProperties fragmentProperties = FragmentPropertiesMngr.GetPropertiesFor(fragment.Key);
                fragmentProperties.SlideNr = slideRelations[fragment.Key];

                int outlineLevel = fragmentProperties.HeadingLevel;
                if (outlineLevel > 0)
                {
                    headings.Add(fragment.Key, outlineLevel);
                }

                int groupNr = fragmentProperties.GroupNr;
                if (groupNr > 0)
                {
                    groups.Add(fragment.Key, groupNr);
                }
            }
            M3SStructureBase.Refresh(realizations, headings, groups);
        }

    }
}
