﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;
using Microsoft.Office.Interop.PowerPoint;
using OfficeCommon.Gui.RightClickInteraction;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement;

namespace PpM3sAddIn.DocAdapter.Gui
{
    class PpTextMeasureClickInteraction : IRightClickInteraction
    {

        private IdRange<TextRange> _selectedTextRange;
        private PpIdFragmentMgrFacade _fragmentPropertiesMngr;

        public PpTextMeasureClickInteraction(PpIdFragmentMgrFacade fragmentPropertiesMngr)
        {
            _fragmentPropertiesMngr = fragmentPropertiesMngr;
        }

        public void OnRightClicked(object sender, CellRightClickEventArgs e, ContextMenu contextMenu)
        {
            if (sender is TreeListView)
            {
                TreeListView treeView = (TreeListView)sender;

                if (treeView.SelectedObjects.Count == 1 && treeView.SelectedObjects[0] is M3SNode)
                {
                    var m3sNode = (M3SNode)treeView.SelectedObjects[0];
                    _selectedTextRange = _fragmentPropertiesMngr.TextRangeIdMgr.GetTextRangeById(m3sNode.Id);

                    if (_selectedTextRange != null)
                    {
                        contextMenu.MenuItems.Add(new MenuItem("Measure Text", CMTextMeasureClick));
                    }
                }
            }

  
        }

        private void CMTextMeasureClick(object sender, EventArgs e)
        {
            TextRange textRange = _selectedTextRange.TextRange.Range;


            Debug.Print("width:" + textRange.BoundWidth + "    height:" + textRange.BoundHeight + "   text:" + textRange.Text);
        }
    }
}
