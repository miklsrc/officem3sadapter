﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeCommon;
using OfficeCommon.Gui;
using System.Windows.Forms;
using BrightIdeasSoftware;
using OfficeCommon.Gui.RightClickInteraction;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement;
using PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using OfficeCommon.IdManagement;
using System.Collections;
using Microsoft.Office.Core;

namespace PpM3sAddIn.DocAdapter.Gui
{

    class PpSideBar : SideBar<PowerPoint.Presentation, PpIdFragmentMgrFacade>
    {
        private readonly PpIdFragmentMgrFacade _ppIdFragmentMgrFacade;

        public PpSideBar(PpPresentationAdapter ppPresentationAdapter)
            : base(ppPresentationAdapter)
        {
            _ppIdFragmentMgrFacade = ppPresentationAdapter.PpIdFragmentMgrFacade;
            M3SOutline.RightClickInteractionManager.registerInteraction(new RightClickGroupInteraction(InstanceAdapter.FragmentPropertiesMngr));
            M3SOutline.RightClickInteractionManager.registerInteraction(new PpStructureRightClickInteraction(InstanceAdapter.FragmentPropertiesMngr));
            M3SOutline.RightClickInteractionManager.registerInteraction(new PpTextMeasureClickInteraction(_ppIdFragmentMgrFacade));

            M3SOutline.TreeList.AddDecoration(new TreeSlideDecoration(InstanceAdapter.FragmentPropertiesMngr));
            M3SOutline.TreeList.MouseDoubleClick += this.treeListView1_MouseDoubleClick;
        }

        private void treeListView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = M3SOutline.TreeList.GetItemAt(e.X, e.Y).Index;
            OLVListItem item = M3SOutline.TreeList.GetItem(index);
            if (item != null && item.RowObject != null && item.RowObject is M3SNode)
            {
                int fragmentId = ((M3SNode)item.RowObject).Id;
                int slideNr = InstanceAdapter.FragmentPropertiesMngr.GetPropertiesFor(fragmentId).SlideNr;
                Globals.ThisAddIn.Application.ActiveWindow.View.GotoSlide(slideNr);

                //select fragment
                IdShape<PowerPoint.Shape> noTextShape = _ppIdFragmentMgrFacade.NoTextShapeIdMgr.getIdShape(fragmentId);
                if (noTextShape != null)
                {
                    noTextShape.Shape.Select(MsoTriState.msoTrue);
                } else {
                    IdRange<PowerPoint.TextRange> idRange = _ppIdFragmentMgrFacade.TextRangeIdMgr.GetTextRangeById(fragmentId);
                    idRange.TextRange.Range.Select();
                }
            }
        }

        protected override void HandleModelCanDrop(object sender, ModelDropEventArgs e)
        {
            e.Handled = true;
            e.Effect = DragDropEffects.None;

            if (e.DropTargetLocation == DropTargetLocation.AboveItem && !DragNDropHelper.IsDropBeforeItself(e) &&
                !DragNDropHelper.IsDropAfterItself(e))
            {
                if (e.TargetModel is M3SNode)
                {
                    M3SNode m3SNode = (M3SNode)e.TargetModel;
                    IdRange<PowerPoint.TextRange> textRangeWithId =
                        _ppIdFragmentMgrFacade.TextRangeIdMgr.GetTextRangeById(m3SNode.Id);
                    if (textRangeWithId != null)
                    {

                        IEnumerable<M3SNode> sourceModels = e.SourceModels.Cast<M3SNode>();
                        int count = sourceModels.Count(x => _ppIdFragmentMgrFacade.TextRangeIdMgr.GetTextRangeById(x.Id) != null);
                        if (count ==
                            1)
                        {
                            e.Effect = DragDropEffects.Move;
                        }
                    }
                }
            }
        }

        protected override void HandleModelDropped(object sender, ModelDropEventArgs e)
        {
            e.Handled = true;

            IEnumerable<M3SNode> sourceModels = e.SourceModels.Cast<M3SNode>();
            M3SNode source = sourceModels.First(x => _ppIdFragmentMgrFacade.TextRangeIdMgr.GetTextRangeById(x.Id) != null);
            M3SNode target = (M3SNode)e.TargetModel;


            IdRange<PowerPoint.TextRange> sourceIdRange =
                _ppIdFragmentMgrFacade.TextRangeIdMgr.GetTextRangeById(source.Id);
            IdRange<PowerPoint.TextRange> afterTargetIdRange =
                _ppIdFragmentMgrFacade.TextRangeIdMgr.GetTextRangeById(target.Id);

            MoveRangeBeforeTarget(sourceIdRange, afterTargetIdRange);
        }

        private void MoveRangeBeforeTarget(IdRange<PowerPoint.TextRange> sourceIdRange, IdRange<PowerPoint.TextRange> afterTargetIdRange)
        {
            PowerPoint.TextRange afterTargetRange = afterTargetIdRange.TextRange.Range;
            int afterTargetId = afterTargetIdRange.Id;
            PowerPoint.Shape containingTargetShape = _ppIdFragmentMgrFacade.TextRangeIdMgr.GetRelatedShape(afterTargetIdRange.Id);


            //the 'p' is only a placeholer, to create a new paragraph
            PowerPoint.TextRange targetRange = afterTargetRange.InsertBefore("p");

            int sourceId = sourceIdRange.Id;

            //if sourceRange contains NO terminating lineBreak (last row i na textbox), the text gets inserted
            //at the begin of the existing targetrange without a new line
            if (!Helper.ContainsLineBreak(sourceIdRange.TextRange.Text))
            {
                // to fix this, a lineBreak gets inserted at the end of the source
                // info: this means after removing the last row, there exist no a "empty" row at the end, which wasn't there before
                sourceIdRange.TextRange.Text += Constants.LINE_BREAK;
            }

            sourceIdRange.TextRange.Range.Cut();
            targetRange.Paste();

            var forcedIds = new List<IdFragmentDescription>();
            forcedIds.Add(new ShapeIdRangeDescription(targetRange.Start, targetRange.Length + targetRange.Start, sourceId,
                containingTargetShape));

            //after the paste-step the afterTargetRange contains "the new pasted-text + the old afterTargetRange-text" 
            //this are 2 paragraphs, next line cuts off the new pasted-text, so only the "real" afterTargetRange-text remains
            afterTargetRange = afterTargetRange.Paragraphs(afterTargetRange.Count, 1);

            forcedIds.Add(new ShapeIdRangeDescription(afterTargetRange.Start, afterTargetRange.Start + afterTargetRange.Length,
                afterTargetId, containingTargetShape));
            _ppIdFragmentMgrFacade.TextRangeIdMgr.RefreshFragments(forcedIds);

            //correct group Id of dropped fragment
            int groupNr = InstanceAdapter.FragmentPropertiesMngr.GetPropertiesFor(afterTargetId).GroupNr;
            InstanceAdapter.FragmentPropertiesMngr.GetPropertiesFor(sourceId).GroupNr = groupNr;
            //
            ActionRefreshM3SOutline(null, null);
        }

        public void slideChanged(PowerPoint.SlideRange SldRange)
        {
            if (Globals.ThisAddIn.Application.ActivePresentation == _ppIdFragmentMgrFacade.TextRangeIdMgr.Presentation)
            {
                ActionRefreshM3SOutline(null,null);
            }
        }


        protected override void ActionRefreshM3SOutline(object sender, EventArgs e)
        {
            base.ActionRefreshM3SOutline(sender,e);
            try
            {
                int slideNr = ((PowerPoint.Slide)Globals.ThisAddIn.Application.ActiveWindow.View.Slide).SlideNumber;
                expandNodesWithSlideNr(slideNr, (ArrayList)M3SOutline.TreeList.Roots);
            }
            catch
            {
                //happens when powerpoint can't 
            }
        }

        private void expandNodesWithSlideNr(int slideNr, IList items)
        {


            for (int i = 0; i < items.Count; i++)
            {
                int fragmentId = ((M3SNode)items[i]).Id;
                int itemSlideNr = InstanceAdapter.FragmentPropertiesMngr.GetPropertiesFor(fragmentId).SlideNr;

                int nextItemSlideNr = -1;
                if (i < items.Count - 1)
                {
                    int nextFragmentId = ((M3SNode)items[i+1]).Id;
                    nextItemSlideNr = InstanceAdapter.FragmentPropertiesMngr.GetPropertiesFor(nextFragmentId).SlideNr;
                }

                if (slideNr >= itemSlideNr && slideNr < nextItemSlideNr && ((M3SNode)items[i]).Children.Any())
                {
                    M3SOutline.TreeList.Expand(items[i]);
                    expandNodesWithSlideNr(slideNr, ((M3SNode)items[i]).Children);
                }
            }
        }
       



    }
}
