﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;
using Microsoft.Office.Interop.PowerPoint;
using OfficeCommon;
using OfficeCommon.Gui.RightClickInteraction;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;

namespace PpM3sAddIn.DocAdapter.Gui
{
    class PpStructureRightClickInteraction : IRightClickInteraction
    {
        private FragmentPropertiesMngr _fragmentPropertiesMngr;
        private M3SNode _selectedNode;

        public PpStructureRightClickInteraction(FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            this._fragmentPropertiesMngr = fragmentPropertiesMngr;
        }

        private void CMHeading3Click(object sender, EventArgs e)
        {
            SetFragmentHeading(3);
        }

        private void CMHeading2Click(object sender, EventArgs e)
        {
            SetFragmentHeading(2);
        }

        private void CMHeading1Click(object sender, EventArgs e)
        {
            SetFragmentHeading(1);
        }


         private void CMStandardClick(object sender, EventArgs eventArgs)
        {
            SetFragmentHeading(-1);
        }

        private void SetFragmentHeading(int headingLevel)
        {
            int fragmentId = _selectedNode.Id;
            _fragmentPropertiesMngr.GetPropertiesFor(fragmentId).HeadingLevel = headingLevel;
        }

        public void OnRightClicked(object sender, CellRightClickEventArgs e, ContextMenu contextMenu)
        {
            if (sender is TreeListView)
            {
                TreeListView treeView = (TreeListView) sender;

                if (treeView.SelectedObjects.Count == 1 && treeView.SelectedObjects[0] is M3SNode)
                {
                    this._selectedNode = (M3SNode) treeView.SelectedObjects[0];

                    contextMenu.MenuItems.Add(new MenuItem("Standard", CMStandardClick));
                    contextMenu.MenuItems.Add(new MenuItem("Heading 1", CMHeading1Click));
                    contextMenu.MenuItems.Add(new MenuItem("Heading 2", CMHeading2Click));
                    contextMenu.MenuItems.Add(new MenuItem("Heading 3", CMHeading3Click));
                }
            }
        }
    }
}
