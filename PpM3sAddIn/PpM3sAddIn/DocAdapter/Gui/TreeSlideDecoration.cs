using System.Drawing;
using BrightIdeasSoftware;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement;

namespace PpM3sAddIn.DocAdapter.Gui
{
    class TreeSlideDecoration : AbstractDecoration
    {
        private FragmentPropertiesMngr fragmentPropertiesMngr;

        public TreeSlideDecoration(FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            this.fragmentPropertiesMngr = fragmentPropertiesMngr;
        }

        public override void Draw(ObjectListView olv, Graphics g, Rectangle r)
        {
            int lastSlideNr = -1;

            for (int i = 0; i < olv.GetItemCount(); i++)
            {
                OLVListItem item = olv.GetItem(i);
                Rectangle bounds = olv.GetItemRect(i);

                if (item.RowObject is M3SNode == false)
                    continue;

                M3SNode m3SNode = (M3SNode)item.RowObject;

                int slideNr = fragmentPropertiesMngr.GetPropertiesFor(m3SNode.Id).SlideNr;

                Point point1 = new Point(olv.Width - 10, bounds.Top);
                Point point2 = new Point(10, bounds.Top);

                if (slideNr < 0 || slideNr != lastSlideNr)
                {
                    lastSlideNr = slideNr;

                    Rectangle rectangle = new Rectangle(olv.Width - 60, bounds.Top, 30, 15);
                    g.FillRectangle(System.Drawing.Brushes.Cyan, rectangle);

                    g.DrawLine(new System.Drawing.Pen(Color.Black, 2), point1, point2);

                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;

                    g.DrawString(slideNr.ToString(), new Font("Arial", 10), new SolidBrush(Color.Black),
                        olv.Width - 45,
                        bounds.Top, drawFormat);
                }
            }



        }

    }
}