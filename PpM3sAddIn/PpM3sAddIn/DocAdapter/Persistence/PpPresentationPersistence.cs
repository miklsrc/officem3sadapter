﻿using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Office.Interop.PowerPoint;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using OfficeCommon.m3s.structure;
using PpM3sAddIn.DocAdapter.IdManagement;
using PpM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;

namespace PpM3sAddIn.DocAdapter.Persistence
{
    public class PpPresentationPersistence : InstanceXmlPersistence
    {

        private readonly PpIdFragmentMgrFacade _ppIdFragmentMgrFacade;
        protected override string PersistenceType => "PowerPointPresentation";

        public PpPresentationPersistence(FragmentPropertiesMngr fragmentPropertiesMngr, PpIdFragmentMgrFacade ppIdFragmentMgrFacade)
            : base(fragmentPropertiesMngr)
        {
            _ppIdFragmentMgrFacade = ppIdFragmentMgrFacade;

            PropertyAccessors[FragmentProperties.Types.HeadingLevel.ToString("G")] = FragmentProperties.Types.HeadingLevel;
        }

        public override bool isCurrentDocReadyToSave(AbstractIdFragmentMgr managerFacade)
        {
            if (ExistDuplicateShapeNames(_ppIdFragmentMgrFacade))
            {
                Logger.Log("shapes with duplice name found. Fixed shape names to save meta data.", LogLevel.Info);
                PpHelper.RenameShapeNames(_ppIdFragmentMgrFacade.NoTextShapeIdMgr.Presentation); 
            }
            return true;
        }


        protected override XElement CreateFragmentIdsXml()
        {
            XElement fragmentIdsRoot = new XElement("fragmentIds");

            fragmentIdsRoot.Add(CreateOthershapesFragemntIdsXml(_ppIdFragmentMgrFacade));
            fragmentIdsRoot.Add(CreateTextboxesFragmentIdsXml(_ppIdFragmentMgrFacade));

            return fragmentIdsRoot;
        }

        private int LoadFragmentIdsForShapes(XDocument xmlDoc, PpNoTextShapeIdMgr noTextShapeIdMgr)
        {
            int maxId = 0;
            foreach (var xmlOtherShape in xmlDoc.Descendants("othershape"))
            {
                int slideindex = Convert.ToInt32(xmlOtherShape.Attribute("slideindex").Value.ToString());
                String name = xmlOtherShape.Attribute("name").Value.ToString();
                int id = Convert.ToInt32(xmlOtherShape.Attribute("id").Value.ToString());

                maxId = Math.Max(maxId, id);
                noTextShapeIdMgr.assignIdToOtherShape(id, slideindex, name);
            }
            return maxId;
        }

        private XElement CreateOthershapesFragemntIdsXml(PpIdFragmentMgrFacade managerFacade)
        {
            XElement otherShapesRoot = new XElement("othershapes");

            foreach (var otherShapeWithId in managerFacade.NoTextShapeIdMgr.OtherShapes)
            {
                int id = otherShapeWithId.Id;
                Shape shape = otherShapeWithId.Shape;
                int slideIndex = PpHelper.GetSlideFromShape(shape).SlideIndex;

                otherShapesRoot.Add(new XElement("othershape",
                    CreateFragmentCorePropertiesXmlAttributes(id),
                    new XAttribute("slideindex", slideIndex.ToString()),
                    new XAttribute("name", shape.Name)
                    ));
            }

            return otherShapesRoot;
        }

        private int LoadFragmentIdsForTextboxes(XDocument xmlDoc, PpTextRangeIdMgr textRangeIdMgr)
        {
            int maxId = 0;
            foreach (var xmlTextBox in xmlDoc.Descendants("textbox"))
            {
                int slideindex = Convert.ToInt32(xmlTextBox.Attribute("slideindex").Value);
                String name = xmlTextBox.Attribute("name").Value;

                foreach (var xmlTextRange in xmlTextBox.Descendants("textrange"))
                {
                    int id = Convert.ToInt32(xmlTextRange.Attribute("id").Value);
                    int start = Convert.ToInt32(xmlTextRange.Attribute("start").Value);
                    int end = Convert.ToInt32(xmlTextRange.Attribute("end").Value);

                    PpTextBox textbox = textRangeIdMgr.TextBoxes.Find(n => PpHelper.GetSlideFromShape(n.Shape).SlideIndex == slideindex && n.Shape.Name == name);
                    textRangeIdMgr.AssignIdToTextRange(new IdRangeDescription(start, end, id), textbox);

                    maxId = Math.Max(maxId, id);
                }
            }
            return maxId;
        }

        private XElement CreateTextboxesFragmentIdsXml(PpIdFragmentMgrFacade managerFacade)
        {
            XElement textboxesRoot = new XElement("textboxes");

            foreach (var textbox in managerFacade.TextRangeIdMgr.TextBoxes)
            {
                var slideIndex = PpHelper.GetSlideFromShape(textbox.Shape).SlideIndex;

                XElement textboxNode = new XElement("textbox",
                    new XAttribute("slideindex", slideIndex.ToString()),
                    new XAttribute("name", textbox.Shape.Name));
                textboxesRoot.Add(textboxNode);

                foreach (var idRange in textbox.TextRanges)
                {
                    textboxNode.Add(new XElement("textrange",
                        CreateFragmentCorePropertiesXmlAttributes(idRange.Id),
                        new XAttribute("start", idRange.TextRange.Start.ToString()),
                        new XAttribute("end", idRange.TextRange.End.ToString())
                        ));
                }
            }
            return textboxesRoot;
        }

        protected override void LoadFragmentIds(XDocument xmlDoc)
        {
            int maxIdTextranges = LoadFragmentIdsForTextboxes(xmlDoc, _ppIdFragmentMgrFacade.TextRangeIdMgr);
            int maxIdShapes = LoadFragmentIdsForShapes(xmlDoc, _ppIdFragmentMgrFacade.NoTextShapeIdMgr);

            _ppIdFragmentMgrFacade.IdCounter.setCounter(Math.Max(maxIdTextranges, maxIdShapes));
        }


        private bool ExistDuplicateShapeNames(PpIdFragmentMgrFacade managerFacade)
        {
            Dictionary<int, HashSet<String>> shapeNames = new Dictionary<int, HashSet<string>>();

            foreach (var otherShapeWithId in managerFacade.NoTextShapeIdMgr.OtherShapes)
            {
                Shape shape = otherShapeWithId.Shape;

                Slide slide = PpHelper.GetSlideFromShape(shape);
                if (!shapeNames.ContainsKey(slide.SlideIndex))
                {
                    shapeNames.Add(slide.SlideIndex, new HashSet<string>());
                }

                if (shapeNames[slide.SlideIndex].Contains(shape.Name))
                {
                    return true;
                }
                shapeNames[slide.SlideIndex].Add(shape.Name);
            }

            foreach (var textbox in managerFacade.TextRangeIdMgr.TextBoxes)
            {
                Shape shape = textbox.Shape;
                Slide slide = PpHelper.GetSlideFromShape(shape);
                if (!shapeNames.ContainsKey(slide.SlideIndex))
                {
                    shapeNames.Add(slide.SlideIndex, new HashSet<string>());
                }

                if (shapeNames[slide.SlideIndex].Contains(shape.Name))
                {
                    return true;
                }
                shapeNames[slide.SlideIndex].Add(shape.Name);
            }
            return false;
        }
    }

    
}
