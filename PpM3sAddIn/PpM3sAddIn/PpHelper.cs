﻿using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using OfficeCommon;
using Core = Microsoft.Office.Core;

namespace PpM3sAddIn
{
    class PpHelper
    {

        private static readonly PpPlaceholderType[] FooterPlaceholders = { PpPlaceholderType.ppPlaceholderDate, PpPlaceholderType.ppPlaceholderSlideNumber, PpPlaceholderType.ppPlaceholderFooter };
        private static readonly PpPlaceholderType[] TitlePlaceholders = { PpPlaceholderType.ppPlaceholderTitle, PpPlaceholderType.ppPlaceholderCenterTitle};


        public static bool IsFooterHeaderShape(Shape shape)
        {
            return shape.Type == Core.MsoShapeType.msoPlaceholder &&
                   FooterPlaceholders.Contains(shape.PlaceholderFormat.Type);
        }
        

        public static bool ShapeExistOnSlide(Shape searchShape, Slide slide)
        {
            try
            {
                foreach (Shape shape in slide.Shapes)
                {
                    if (searchShape == shape)
                    {
                        return true;
                    }
                }
            }
            catch (COMException e) //dirty workaround to catch if slide doesn't exist (it throws a exception on slide access)
            {
                return false;
            }
            return false;
        }


        public static bool IsTitleShape(Shape shape)
        {
            return shape.Type == Core.MsoShapeType.msoPlaceholder &&
                   TitlePlaceholders.Contains(shape.PlaceholderFormat.Type);

            // old impl
            //return containingSlide.Shapes.HasTitle == Core.MsoTriState.msoTrue && containingSlide.Shapes.Title == shape;
        }


     
        public static Slide GetSlideFromShape(Shape searchShape)
        {
            //don't know if this works for any cases
            if (searchShape.Parent is Slide)
                return searchShape.Parent;
            

            //fall back if somecase is not included in the code above
            foreach (Slide slide in Globals.ThisAddIn.Application.ActivePresentation.Slides)
            {
                foreach (Shape shape in slide.Shapes)
                {
                    if (shape == searchShape)
                    {
                        return slide;
                    }
                }
            }

            return null;
        }

        public static bool shapeHasPicture(Shape shape)
        {
            if (shape.Type == Core.MsoShapeType.msoLinkedPicture || shape.Type == Core.MsoShapeType.msoPicture) return true;

            if (shape.Type == Core.MsoShapeType.msoPlaceholder)
            {
                if (shape.PlaceholderFormat.ContainedType == Core.MsoShapeType.msoPicture)
                {
                    return true;
                }
            }

            return false;
        }

        public static void ExportShapeAsPicture(Shape shape, String path)
        {
            shape.Export(path, PpShapeFormat.ppShapeFormatJPG, 0, 0, PpExportMode.ppScaleXY);
        }



        public static String ConvertShapeToJpgBase64(Shape shape)
        {
            String shapeImgFilePath =OfficeCommon.Helper.CreateTempFolder(Globals.ThisAddIn.Application.ActivePresentation.FullName) +"\\shapeimg.jpg";

            try
            {
                shape.Export(shapeImgFilePath, PpShapeFormat.ppShapeFormatJPG, 0, 0, PpExportMode.ppScaleXY);
            }
            catch (Exception e)
            {
                Logger.Log(e.ToString(), LogLevel.Error);
            }




            String imgJpgBase64 = OfficeCommon.FileHelper.BinaryToBase64String(shapeImgFilePath);
            return imgJpgBase64;
        }


        internal static void RenameShapeNames(Presentation presentation)
        {
            int shapeCounter = 0;

            foreach (Slide sl in presentation.Slides)
            {
                foreach (Shape shape in sl.Shapes)
                {
                    shape.Name = "shape" + shapeCounter;
                    shapeCounter++;
                }
            }
        }
    }
}
