using Microsoft.Office.Interop.PowerPoint;
using OfficeCommon;
using OfficeCommon.IdManagement;
using PpM3sAddIn.DocAdapter;
using PpM3sAddIn.DocAdapter.IdManagement;
using PpM3sAddIn.DocAdapter.Persistence;

namespace PpM3sAddIn
{
    class PpPresentationManager : InstanceManager<Presentation>
    {

        protected override InstanceAdapter CreateConcreteInstanceAdapter(Presentation instance)
        {
            return new PpPresentationAdapter(instance);
        }

        public override string GetFilePath(Presentation presentation)
        {
            return presentation.FullName;
        }

        public override string GetFolderPath(Presentation presentation)
        {
            return presentation.Path;
        }


        protected override string GetNameForNewInstance()
        {
            return "PowerPoint";
        }

        public void SlideChangedEvent(SlideRange sldrange)
        {
            Presentation activePres = Globals.ThisAddIn.Application.ActivePresentation;
            if (Adapters.ContainsKey(activePres))
            {
               var instanceAdapter = Adapters[activePres];
                if (instanceAdapter is PpPresentationAdapter)
                {
                    ((PpPresentationAdapter)instanceAdapter).SlideChanged(sldrange);
                }
            }
        }
    }
}