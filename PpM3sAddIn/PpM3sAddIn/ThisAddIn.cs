﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using OfficeCommon;
using OfficeCommon.IdManagement;
using PpM3sAddIn.DocAdapter;
using PpM3sAddIn.DocAdapter.IdManagement;
using PpM3sAddIn.DocAdapter.Persistence;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Office = Microsoft.Office.Core;
using System.IO;

namespace PpM3sAddIn
{

    public partial class ThisAddIn
    {
        private PpPresentationManager _ppPresentationManager = new PpPresentationManager();

//        private bool init = true;

        public InstanceManager<PowerPoint.Presentation> PpPresentationManager => _ppPresentationManager;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //Globals.ThisAddIn.Application.WindowSelectionChange += WindowSelectionChangedEvent;

            Globals.ThisAddIn.Application.SlideSelectionChanged += _ppPresentationManager.SlideChangedEvent;
            Globals.ThisAddIn.Application.PresentationSave += _ppPresentationManager.InstanceSavedEvent;
            Globals.ThisAddIn.Application.AfterPresentationOpen += _ppPresentationManager.InstanceOpenedEvent;
        }

//        private void WindowSelectionChangedEvent(PowerPoint.Selection Sel)
//        {
//            // when powerpoint starts, it opens a window with a empty presentation
//            // this is a dirty workaround to open a default document in the existing window
//
//            String text = Globals.ThisAddIn.Application.ActivePresentation.Slides[1].Shapes[1].TextFrame.TextRange.Text;
//            int count = Globals.ThisAddIn.Application.ActivePresentation.Slides.Count;
//            if (count == 1 && init == true && text.Length == 0)
//            {
//                Globals.ThisAddIn.Application.Presentations.Open(@"D:\temp\doksemIII.pptx");
//                init = false;
//            }
//        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new PpM3sRibbon();
        }

        #region Von VSTO generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
