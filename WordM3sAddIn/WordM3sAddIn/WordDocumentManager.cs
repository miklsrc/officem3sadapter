﻿using Microsoft.Office.Interop.Word;
using OfficeCommon;
using WordM3sAddIn.DocAdapter;
using WordM3sAddIn.DocAdapter.IdManagement;

namespace WordM3sAddIn
{
    class WordDocumentManager : InstanceManager<Document>
    {

        protected override InstanceAdapter CreateConcreteInstanceAdapter(Document instance)
        {
            return new WordDocumentAdapter(instance);
        }

        public override string GetFilePath(Document document)
        {
            return document.FullName;
        }

        public override string GetFolderPath(Document presentation)
        {
            return presentation.Path;
        }

        protected override string GetNameForNewInstance()
        {
            return "Word";
        }
    }
}