﻿using OfficeCommon.IdManagement.TextRangeProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.TextRangeProcessing
{
    public class WordTextRange : AbstractTextRange<Word.Range>
    {
        private Word.Range range;

        public WordTextRange(Word.Range range)
        {
            this.range = range;
        }

        public override String Text
        {
            get { return range.Text; }
            set { range.Text = value; }
        }

        public override int End
        {
            get { return range.End; }
        }

        public override int Start
        {
            get { return range.Start; }
        }

        public override int Length
        {
            get { return range.End - range.Start; }
        }

        public override Word.Range Range
        {
            get { return range; }
        }
    }
}
