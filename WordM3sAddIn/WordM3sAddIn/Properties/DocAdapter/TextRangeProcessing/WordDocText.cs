﻿using OfficeCommon.IdManagement.TextRangeProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.TextRangeProcessing
{
    public class WordDocText : TextRangesContainer<Word.Range>
    {

        public WordDocText()
        {
        }

        public WordDocText(Word.Document document)
        {
            foreach (Word.Paragraph para in document.Paragraphs)
            {
                textRanges.Add(new IdRange<Word.Range>(new WordTextRange(para.Range)));
            }
        }

    }
}
