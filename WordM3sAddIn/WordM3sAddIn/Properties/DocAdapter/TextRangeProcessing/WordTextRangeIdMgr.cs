﻿using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.TextRangeProcessing
{

    public class WordTextRangeIdMgr : AbstractTextRangeIdMgr<Word.Range>
    {
        private WordDocText _wordDocText = new WordDocText();
        private Word.Document _document;
        public static readonly string FRAGMENT_TYPE = "WordTextRange";

        public WordTextRangeIdMgr(Word.Document document)
            : base(new IdCounter())
        {
            this._document = document;
        }

        public Word.Document Document
        {
            get { return _document; }
        }

        public WordDocText WordDocText
        {
            get { return _wordDocText; }
        }

        public override  void UpdateFragments()
        {
            WordDocText currentWordDocText = new WordDocText(_document);

            //repair old text range base
            base.DeleteEmptyRanges(_wordDocText); //not needed, because empty ranges get merged in the combineMergedRanges step

            removeNeedlessLineBreaks(_wordDocText, currentWordDocText);

            var newAddedRangesPerBoxes = AddNewInsertedRanges(_wordDocText, currentWordDocText);

            base.FixAtRageStartInsertedText(newAddedRangesPerBoxes, _wordDocText);

            // combineMergedRanges(_wordDocText, currentWordDocText);
            base.CutOffOverlappingRanges(_wordDocText, currentWordDocText);

            base.CheckTextRangeLegality(_wordDocText, currentWordDocText);
        }

        public override HashSet<int> GetAllFragmentIds()
        {
            HashSet<int> fragmentIds = new HashSet<int>();

            foreach (var fragment in _wordDocText.TextRanges)
            {
                if (!fragmentIds.Add(fragment.Id))
                {
                    throw new Exception("duplicate ids");
                }
            }

            return fragmentIds;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldTextBox"></param>
        /// <param name="curTextBox"></param>
        private void removeNeedlessLineBreaks(WordDocText oldTextBox, WordDocText curTextBox)
        {

            int indexOld = 1;
            int indexCur = 1;
            List<IdRange<Word.Range>> oldRanges = oldTextBox.TextRanges;
            List<IdRange<Word.Range>> curRanges = curTextBox.TextRanges;

            IdRange<Word.Range> oldRange;
            IdRange<Word.Range> curRange;

            while (indexOld < oldRanges.Count && indexCur < curRanges.Count)
            {
                oldRange = oldRanges[indexOld];
                curRange = curRanges[indexCur];

                //line breaks at the beginning of a range. example '\rNew Line'
                if ((curRange.TextRange.Start == oldRange.TextRange.Start + Constants.LINE_BREAK.Length
                    && curRange.TextRange.End == oldRange.TextRange.End
                    && oldRange.TextRange.Text.StartsWith(Constants.LINE_BREAK)
                    && oldRange.TextRange.Text.Length > Constants.LINE_BREAK.Length)
                ||  //line breaks at the end of a range. example 'New Line\r'
                  (curRange.TextRange.Start == oldRange.TextRange.Start
                    && curRange.TextRange.End == oldRange.TextRange.End + Constants.LINE_BREAK.Length
                    && curRange.TextRange.Text.EndsWith(Constants.LINE_BREAK)
                    && curRange.TextRange.Text.Length > Constants.LINE_BREAK.Length))
                {
                    oldRange.TextRange = curRange.TextRange;
                    indexOld++;
                    indexCur++;
                }
                else
                {
                    if (curRange.TextRange.Start > oldRange.TextRange.Start)
                    {
                        indexOld++;
                    }
                    else if (curRange.TextRange.Start < oldRange.TextRange.Start)
                    {
                        indexCur++;
                    }
                    else
                    {
                        indexOld++;
                        indexCur++;
                    }
                }
            }

        }

        public void AssignIdsToTextRanges(List<IdRangeDescription> textRangeDescriptions)
        {
            base.AssignIdsToTextRangesContainer(textRangeDescriptions, _wordDocText);
        }

        public IdRange<Word.Range> GetTexTRangeWithId(int searchId)
        {
                foreach (var idRange in _wordDocText.TextRanges)
                {
                    if (idRange.Id == searchId)
                    {
                        return idRange;
                    }
                }
            return null;
        }
    }
}
