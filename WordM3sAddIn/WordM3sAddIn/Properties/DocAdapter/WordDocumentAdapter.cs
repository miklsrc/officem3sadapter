﻿using System.IO;
using Microsoft.Office.Tools;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using M3s = OfficeCommon.M3s;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WordM3sAddIn.DocAdapter.Gui;
using WordM3sAddIn.DocAdapter.Persistence;
using WordM3sAddIn.DocAdapter.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter
{

    public class WordDocumentAdapter : InstanceAdapter<Word.Document, WordTextRangeIdMgr>
    {
        private WordSideBar _sideBar;

        public WordDocumentAdapter(Word.Document document)
            : base(document, new WordTextRangeIdMgr(document), new WordDocumentPersistence())
        {
            _sideBar = new WordSideBar(this);
            MyCustomTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(_sideBar, "mikl");
            Controller core = new Controller(_sideBar);
        }

    }
}
