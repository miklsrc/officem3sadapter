﻿using OfficeCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WordM3sAddIn.DocAdapter.Persistence;
using WordM3sAddIn.DocAdapter.Structure;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter
{
    public interface IController
    {
        Word.Range moveParagraph(StructureNode sourceNode, StructureNode targetNode, Target direction);
        void refreshDocStructure();
    }

    class Controller : IController
    {
        private Gui.View view;
        private StructureBuilder structureBuilder;
        private Gui.WordSideBar sideBar;

        public Controller(Gui.View gui)
        {
            this.view = gui;
            gui.setController(this);

            Globals.ThisAddIn.Application.WindowSelectionChange += Application_WindowSelectionChange;

            structureBuilder = new StructureBuilder(Globals.ThisAddIn.Application.ActiveDocument);
        }

        private void Application_WindowSelectionChange(Word.Selection Sel)
        {
            //Debug.WriteLine("selection changed");
            view.refreshStructureTreeSelection(Sel.Paragraphs.First);

        }


        public void refreshDocStructure()
        {
            Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;
            if (document.Content.Paragraphs.Count > 0)
            {
                structureBuilder = new StructureBuilder(document);
                structureBuilder.build();

                view.refreshStructureTree(structureBuilder);
            }
        }

        private Word.Range insertBeforeTarget(Moveable targetMoveable, Moveable sourceMoveable)
        {
            Word.Range insertTargetRange = null;
            Word.Range range = sourceMoveable.getRange();

            if (targetMoveable is TableNode)
            {
                //there are big problems inserting text BEFORE a table without inserting INTO it
                //main idea from here: http://go4answers.webhost4life.com/Example/word-2007vba-insert-text-before-59715.aspx
                Word.Table table = ((TableNode)targetMoveable).Table;
                table.Rows.Add(table.Rows[1]);

                //splits the table into 2 tables, there appears a empty paragraph between both tables 
                //(the new first seperated empty 1-row table belongs now to the "table" var. the "old" table isn't any longer referenced by this variable)
                table.Split(2);

                // get the "old" table
                Word.Range oldTableRange = table.Range.Next().Next();

                //delete the empty "new" table
                table.Delete();

                oldTableRange.Previous().InsertParagraphBefore();
                insertTargetRange = oldTableRange.Previous().Previous();
                insertTargetRange.FormattedText = range.FormattedText;

                //don't know why, but a empty paragraph is left => delete
                insertTargetRange.Next().Delete();
            }
            else
            {

                if (sourceMoveable is TableNode)
                {
                    targetMoveable.getRange().InsertParagraphBefore();
                    insertTargetRange = targetMoveable.getRange().Previous(Word.WdUnits.wdParagraph);
                }
                else
                {
                    //remove the last char (line break), because copying this thing causes some strange ID errors
                    range.SetRange(range.Start, range.End - 1);
                    targetMoveable.getRange().InsertParagraphBefore();
                    targetMoveable.getRange().InsertParagraphBefore();
                    insertTargetRange = targetMoveable.getRange().Previous(Word.WdUnits.wdParagraph).Previous(Word.WdUnits.wdParagraph);
                }



                insertTargetRange.FormattedText = range.FormattedText;
            }
            return insertTargetRange;
        }


        private void deleteSource(Moveable sourceMoveable, bool sourceIsDocEnd)
        {
            Word.Range range = sourceMoveable.getRange();

            if (!sourceIsDocEnd && (bool)range.Next().Information[Word.WdInformation.wdWithInTable])
            {
                //for some strange reason deletion directly before a table leaves an empty line => delete this too
                range.Previous().Delete();
                range.Delete();
            }
            else
            {
                sourceMoveable.delete();
            }
        }

        public Word.Range moveParagraph(StructureNode sourceNode, StructureNode targetNode, Target direction)
        {

            if (sourceNode is Moveable && targetNode is Moveable && direction == Target.Top)
            {
                Moveable targetMoveable = (Moveable)targetNode;
                Moveable sourceMoveable = (Moveable)sourceNode;

                Word.Range sourceRange = sourceMoveable.getRange();
                bool sourceIsDocEnd = sourceRange.Paragraphs.Last.Next() == null;

                Word.Paragraph nextToTargetParagraph = targetMoveable.getRange().Paragraphs.First;
                Word.Paragraph nextToSourceParagraph = sourceIsDocEnd ? null : sourceRange.Paragraphs.Last.Next();
                string nextToTargetParaId = nextToTargetParagraph.ID;
                string nextToSourceParaId = sourceIsDocEnd ? "" : nextToSourceParagraph.ID;

                Word.Range insertTargetRange = insertBeforeTarget(targetMoveable, sourceMoveable);

                //there are some paragraphs who loosing the id during moving:
                //1. the paragraph after the target location(the paragraph which wins a new previous paragrpagh through inserting)
                //2. the paragraph after source location (the paragraph which looses the previous paragrpagh through deleting)
                //3. see copyParagraphIds

                copyParagraphIds(sourceRange, insertTargetRange);

                deleteSource(sourceMoveable, sourceIsDocEnd);

                if (!sourceIsDocEnd)
                {
                    nextToSourceParagraph.ID = nextToSourceParaId; //case2
                }

                nextToTargetParagraph.ID = nextToTargetParaId; //case 1


                return insertTargetRange;
            }

            return null;
        }

        private void copyParagraphIds(Word.Range sourceRange, Word.Range targetRange)
        {
            //TODO
            //check if it is maybe enough only reassign the ID of first paragraph
            //because it looks like only the first paragraph gets destroyed through the FormattedText copy

            Word.Paragraphs sourceParagraphs = sourceRange.Paragraphs;
            Word.Paragraphs targetParagrpahs = targetRange.Paragraphs;

            if (sourceParagraphs.Count != targetParagrpahs.Count)
            {
                throw new Exception("should never happen, different amount o paragraphs to assign IDs");
            }

            for (int i = 1; i <= sourceParagraphs.Count; i++)
            {
                targetParagrpahs[i].ID = sourceParagraphs[i].ID;
            }
        }


        private void startTimer()
        {
            // refreshStructureTimer = new System.Threading.Timer(RefreshStructureCallback);
            // refreshStructureTimer.Change(0, 500);
        }

        private void stopTimer()
        {
            //if (refreshStructureTimer != null)
            //{
            //    refreshStructureTimer.Dispose();
            //}
        }

        private void RefreshStructureCallback(object state)
        {
            //stopTimer();
            //Debug.WriteLine("timer");
            //if (!structureBuilder.checkIntegrity())
            //{
            //    Debug.Write("integrity defect");
            //    buttonRefresh.Invoke((MethodInvoker)(() => buttonRefresh.PerformClick()));
            //}
            //startTimer();
        }
    }
}
