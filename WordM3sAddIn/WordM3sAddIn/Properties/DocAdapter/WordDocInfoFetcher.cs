﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter
{
    class WordDocInfoFetcher
    {
        private TreeView treeViewInfo;


        public WordDocInfoFetcher()
        {

        }

        public WordDocInfoFetcher(TreeView treeViewInfo)
        {
            this.treeViewInfo = treeViewInfo;
        }


        public delegate String ToTextDelegate<TypeEntry>(TypeEntry entry);

        public int GetEnumerableCount(IEnumerable Enumerable)
        {
            return (from object Item in Enumerable
                    select Item).Count();
        }

        private void AddNode<TypeEntry>(IEnumerable collection, String name, ToTextDelegate<TypeEntry> ToText)
        {
            //
            if (GetEnumerableCount(collection) > 0)
            {
                TreeNode rootNode = new TreeNode(name);
                this.treeViewInfo.Nodes.Add(rootNode);
                foreach (TypeEntry entry in collection)
                {

                    TreeNode newChildNode = new TreeNode(ToText(entry));
                    rootNode.Nodes.Add(newChildNode);
                }
            }
        }

        public void refreshWordDocInfoTree()
        {
            Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;
            this.treeViewInfo.Nodes.Clear();


            ToTextDelegate<Word.Range> RangeToText = delegate(Word.Range range) { return range.Text.ToString(); };
            ToTextDelegate<Word.Paragraph> ParagraphToText = delegate(Word.Paragraph paragraph) { return RangeToText(paragraph.Range); };


            AddNode<Word.XMLNode>(document.XMLNodes, "XML Nodes", (Word.XMLNode xmlNode) => { return xmlNode.ToString(); });
            AddNode<Word.Paragraph>(document.Content.Paragraphs, "paragraphs", ParagraphToText);
            AddNode<Word.Table>(document.Content.Tables, "tables", (Word.Table table) => { return RangeToText(table.Range); });
            AddNode<Word.Range>(document.Content.Sentences, "Sentences", RangeToText);
            AddNode<Word.Range>(document.Content.ShapeRange, "shape Range", RangeToText);
            AddNode<Word.Section>(document.Content.Sections, "Sections", (Word.Section section) => { return RangeToText(section.Range); });
            AddNode<Word.Paragraph>(document.Content.ListParagraphs, "ListParagraphs", ParagraphToText);
            AddNode<Word.InlineShape>(document.Content.InlineShapes, "InlineShapes", (Word.InlineShape shape) => { return "page: " + ((int)shape.Range.Information[Word.WdInformation.wdActiveEndAdjustedPageNumber]).ToString(); });
            AddNode<Word.Field>(document.Content.Fields, "Fields", (Word.Field field) => { return field.Code.Start.ToString() + " " + field.Code.End.ToString(); });
            AddNode<Word.Frame>(document.Content.Frames, "Frames", (Word.Frame frame) => { return RangeToText(frame.Range); });
            AddNode<Word.Shape>(document.Content.ShapeRange, "ShapeRanges", (Word.Shape shape) => { return shape.ToString(); });
            AddNode<Word.TableOfContents>(document.TablesOfContents, "TOCs", (Word.TableOfContents field) => { return field.Range.Start.ToString() + " " + field.Range.End.ToString(); });


            if (document.Content.Paragraphs.Count > 0)
            {
                TreeNode rootNode = new TreeNode("paragraphs with attributes");
                this.treeViewInfo.Nodes.Add(rootNode);
                foreach (Word.Paragraph paragraph in document.Content.Paragraphs)
                {

                    TreeNode newChildNode = new TreeNode(ParagraphToText(paragraph));
                    rootNode.Nodes.Add(newChildNode);

                    //  WdInformation
                    TreeNode wdInfoNode = new TreeNode("WdInformation");
                    newChildNode.Nodes.Add(wdInfoNode);

                    foreach (Word.WdInformation val in Enum.GetValues(typeof(Word.WdInformation)))
                    {
                        object wdInfoResult = paragraph.Range.Information[val];
                        TreeNode newInfoNode = new TreeNode(val.ToString() + ":" + wdInfoResult.ToString());
                        wdInfoNode.Nodes.Add(newInfoNode);
                    }

                    // Style
                    Microsoft.Office.Interop.Word.Style style = paragraph.Range.get_Style() as Microsoft.Office.Interop.Word.Style;
                    if (style != null)
                    {

                        TreeNode styleNode = new TreeNode("Style: " + style.NameLocal);
                        newChildNode.Nodes.Add(styleNode);
                    }
                    //outline level
                    TreeNode outlineLevelNode = new TreeNode("Level: " + paragraph.OutlineLevel.ToString());
                    newChildNode.Nodes.Add(outlineLevelNode);

                    //ID
                    TreeNode idLevelNode = new TreeNode("ID: " + paragraph.ID);
                    newChildNode.Nodes.Add(idLevelNode);
                }
            }

        }
    }
}
