﻿namespace WordM3sAddIn.DocAdapter.Gui
{
    partial class WordSideBar
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.btnShowInfo = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRefreshInfoTree = new System.Windows.Forms.Button();
            this.btnM3sCommit = new System.Windows.Forms.Button();
            this.btnM3sPing = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.treeViewStructure = new System.Windows.Forms.TreeView();
            this.treeViewInfo = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonRefresh.Location = new System.Drawing.Point(0, 0);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(400, 23);
            this.buttonRefresh.TabIndex = 0;
            this.buttonRefresh.Text = "refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // btnShowInfo
            // 
            this.btnShowInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnShowInfo.Location = new System.Drawing.Point(3, 3);
            this.btnShowInfo.Name = "btnShowInfo";
            this.btnShowInfo.Size = new System.Drawing.Size(194, 24);
            this.btnShowInfo.TabIndex = 4;
            this.btnShowInfo.Text = "session info";
            this.btnShowInfo.UseVisualStyleBackColor = true;
            this.btnShowInfo.Click += new System.EventHandler(this.btnShowInfo_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnShowInfo, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnRefreshInfoTree, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnM3sCommit, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnM3sPing, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnUpdate, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 510);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(400, 90);
            this.tableLayoutPanel2.TabIndex = 5;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // btnRefreshInfoTree
            // 
            this.btnRefreshInfoTree.Location = new System.Drawing.Point(203, 3);
            this.btnRefreshInfoTree.Name = "btnRefreshInfoTree";
            this.btnRefreshInfoTree.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshInfoTree.TabIndex = 5;
            this.btnRefreshInfoTree.Text = "refresh info";
            this.btnRefreshInfoTree.UseVisualStyleBackColor = true;
            this.btnRefreshInfoTree.Click += new System.EventHandler(this.btnRefreshInfo_Click);
            // 
            // btnM3sCommit
            // 
            this.btnM3sCommit.Location = new System.Drawing.Point(3, 33);
            this.btnM3sCommit.Name = "btnM3sCommit";
            this.btnM3sCommit.Size = new System.Drawing.Size(129, 24);
            this.btnM3sCommit.TabIndex = 6;
            this.btnM3sCommit.Text = "m3s commit";
            this.btnM3sCommit.UseVisualStyleBackColor = true;
            this.btnM3sCommit.Click += new System.EventHandler(this.btnM3sCommit_Click);
            // 
            // btnM3sPing
            // 
            this.btnM3sPing.Location = new System.Drawing.Point(203, 33);
            this.btnM3sPing.Name = "btnM3sPing";
            this.btnM3sPing.Size = new System.Drawing.Size(121, 24);
            this.btnM3sPing.TabIndex = 7;
            this.btnM3sPing.Text = "m3s ping";
            this.btnM3sPing.UseVisualStyleBackColor = true;
            this.btnM3sPing.Click += new System.EventHandler(this.btnM3sPing_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(3, 63);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(106, 24);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 23);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.treeViewInfo);
            this.splitContainer1.Size = new System.Drawing.Size(400, 487);
            this.splitContainer1.SplitterDistance = 195;
            this.splitContainer1.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.treeViewStructure);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 195);
            this.panel1.TabIndex = 4;
            // 
            // treeViewStructure
            // 
            this.treeViewStructure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewStructure.Location = new System.Drawing.Point(0, 0);
            this.treeViewStructure.Name = "treeViewStructure";
            this.treeViewStructure.Size = new System.Drawing.Size(400, 195);
            this.treeViewStructure.TabIndex = 6;
            // 
            // treeViewInfo
            // 
            this.treeViewInfo.AllowDrop = true;
            this.treeViewInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewInfo.ItemHeight = 16;
            this.treeViewInfo.Location = new System.Drawing.Point(0, 0);
            this.treeViewInfo.Name = "treeViewInfo";
            this.treeViewInfo.Size = new System.Drawing.Size(400, 288);
            this.treeViewInfo.TabIndex = 3;
            // 
            // WordSideBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.buttonRefresh);
            this.Name = "WordSideBar";
            this.Size = new System.Drawing.Size(400, 600);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button btnShowInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeViewInfo;
        private System.Windows.Forms.Button btnRefreshInfoTree;
        private System.Windows.Forms.TreeView treeViewStructure;
        private System.Windows.Forms.Button btnM3sCommit;
        private System.Windows.Forms.Button btnM3sPing;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Panel panel1;
    }
}
