﻿using M3s = OfficeCommon.M3s;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WordM3sAddIn.DocAdapter.Gui
{

    

    public partial class ChooseSourceDocument : Form
    {
        public ChooseSourceDocument()
        {
            InitializeComponent();
        }

        public void setDocuments(M3s.Document[] documents)
        {
             GuiDocument[] guiDocuments = Array.ConvertAll(documents,new Converter<M3s.Document,GuiDocument>(d => {return new GuiDocument(d);}));
             listDocuments.DataSource = guiDocuments;
             listDocuments.SelectedIndex = 0;
        }

        public M3s.Document GetSelectedDocument()
        {
            GuiDocument guiDoc = (GuiDocument)listDocuments.SelectedItem;
            return guiDoc.document;
        }


        public static M3s.Document SelectDocument(M3s.Document[] documents)
        {
            ChooseSourceDocument chooseM3sDialog = new ChooseSourceDocument();
            chooseM3sDialog.setDocuments(documents);
            if (chooseM3sDialog.ShowDialog() != DialogResult.OK)
            {
                return null;
            }
            M3s.Document selectedDoc = chooseM3sDialog.GetSelectedDocument();
            chooseM3sDialog.Dispose();
            return selectedDoc;
        }





        class GuiDocument
        {
            public M3s.Document document;

            public GuiDocument(M3s.Document document)
            {
                this.document = document;
            }

            public override string ToString()
            {
                return document.name;
            }
        }



    }
}
