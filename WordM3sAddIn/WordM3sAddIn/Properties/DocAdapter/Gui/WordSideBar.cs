﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OfficeCommon.Gui;
using OfficeCommon.IdManagement.TextRangeProcessing;
using WordM3sAddIn.DocAdapter.Persistence;
using Word = Microsoft.Office.Interop.Word;
using OfficeCommon;
using WordM3sAddIn.DocAdapter.TextRangeProcessing;
using WordM3sAddIn.DocAdapter.Structure;
using System.Diagnostics;
using OfficeCommon.M3s;

namespace WordM3sAddIn.DocAdapter.Gui
{
    public interface View
    {
        void refreshStructureTreeSelection(Word.Paragraph paragraph);
        void setController(IController controller);
        void refreshStructureTree(StructureBuilder structureBuilder);
    }


    public partial class WordSideBar : UserControl, View
    {
        private ExtDragNDropBehaviour dragNDropBehaviour = new ExtDragNDropBehaviour();
        private WordDocInfoFetcher _docInfo;

        private TreeBuilder _treeBuilder;
        private IController _controller;



        public WordSideBar(WordDocumentAdapter docAdapter)
        {
            _docAdapter = docAdapter;

            InitializeComponent();

            _docInfo = new WordDocInfoFetcher(this.treeViewInfo);

            dragNDropBehaviour.BindToTreeView(this.treeViewStructure);
            dragNDropBehaviour.BeforeDrop += beforeDropHandler;
            dragNDropBehaviour.DropAllowed += dropAllowed;
            dragNDropBehaviour.AfterDrop += afterDrop;

            _docAdapter.Instance.Application.WindowSelectionChange += SelectionChanged;
        }


        private void SelectionChanged(Word.Selection sel)
        {
            if (Globals.ThisAddIn.Application.ActiveDocument == _docAdapter.Instance)
            {


                // _controller.refreshDocStructure();

                this.treeViewStructure.Nodes.Clear();

                TreeNode rootNode = new TreeNode("root");
                this.treeViewStructure.Nodes.Add(rootNode);

                _docAdapter.IdFragmentMgr.UpdateFragments();
                Dictionary<int, List<Document>> rangeDocsMap =_docAdapter.FragmentDocAssigner.FragmentDocsMap;
                foreach (IdRange<Word.Range> curRange in _docAdapter.IdFragmentMgr.WordDocText.TextRanges)
                {
                    List<Document> rangeDocs = rangeDocsMap[curRange.Id];
                    string docsStr = String.Join(",", rangeDocs.ConvertAll(n=>n.id));
                    TreeNode newChildNode = new TreeNode("[" + curRange.Id + "] " + curRange.TextRange.Text+ " [" + docsStr+"]");
                    rootNode.Nodes.Add(newChildNode);
                }
                rootNode.ExpandAll();


            }
        }


        private void mouseEntered(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn.Application.ActiveDocument == _docAdapter.Instance)
            {
                //_controller.refreshDocStructure();
                //idManager.updateTextRanges(document);

                this.treeViewStructure.Nodes.Clear();
                WordDocText ranges = _docAdapter.IdFragmentMgr.WordDocText;
                foreach (IdRange<Word.Range> curRange in ranges.TextRanges)
                {
                    TreeNode newChildNode = new TreeNode("[" + curRange.Id + "] " + curRange.TextRange.Start + " - " + curRange.TextRange.End + "  |  " + curRange.TextRange.Text);
                    this.treeViewStructure.Nodes.Add(newChildNode);
                }

                this.treeViewInfo.Nodes.Clear();
                foreach (Word.Paragraph para in _docAdapter.Instance.Paragraphs)
                {
                    TreeNode newChildNode = new TreeNode(para.Range.Start + " - " + para.Range.End + "  |  " + para.Range.Text);
                    this.treeViewInfo.Nodes.Add(newChildNode);
                }
            }
        }



        void View.setController(IController controller)
        {
            this._controller = controller;
        }


        void View.refreshStructureTree(StructureBuilder structureBuilder)
        {
            treeViewStructure.Invoke((MethodInvoker)(() => treeViewStructure.Nodes.Clear()));

            _treeBuilder = new TreeBuilder();
            TreeNodeCollection nodes = _treeBuilder.build(structureBuilder);

            foreach (TreeNode treeNode in nodes)
            {
                treeViewStructure.Invoke((MethodInvoker)(() => treeViewStructure.Nodes.Add(treeNode)));
            }
        }


        private bool dropAllowed(Tuple<TreeNode, Target> target, TreeNode source)
        {
            StructureNode sourceNode = (StructureNode)source.Tag;
            StructureNode targetNode = (StructureNode)target.Item1.Tag;
            return (sourceNode is Moveable && targetNode is Moveable && target.Item2 == Target.Top);
        }

        private bool beforeDropHandler(Tuple<TreeNode, Target> target, TreeNode source)
        {
            StructureNode sourceNode = (StructureNode)source.Tag;
            StructureNode targetNode = (StructureNode)target.Item1.Tag;
            if (sourceNode is Moveable && targetNode is Moveable && target.Item2 == Target.Top)
            {
                Word.Range targetRange = _controller.moveParagraph(sourceNode, targetNode, target.Item2);
                if (targetRange != null)
                {
                    _controller.refreshDocStructure();
                    //its ok, that node will not get moved, because complete tree gets refreshed

                    refreshStructureTreeSelection(targetRange.Paragraphs.First);
                }
            }
            return false;
        }

        private void afterDrop(TreeNode dropped)
        {
            _controller.refreshDocStructure();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            _controller.refreshDocStructure();
        }





        private void btnRefreshInfo_Click(object sender, EventArgs e)
        {
            _docInfo.refreshWordDocInfoTree();
        }

        List<Word.Range> ranges = new List<Word.Range>();
        private WordDocumentAdapter _docAdapter;



        public void refreshStructureTreeSelection(Word.Paragraph paragraph)
        {
            treeViewStructure.HideSelection = false;
            //treeViewStructure.SelectedNode = treeViewStructure.Nodes[treeViewStructure.Nodes.Count - 1].Nodes[treeViewStructure.Nodes.Count - 1];

            if (_treeBuilder == null)
            {
                return;
            }

            foreach (KeyValuePair<Word.Paragraph, TreeNode> pair in _treeBuilder.para2nodeMap)
            {
                if (paragraph.Range.Start == pair.Key.Range.Start)
                {
                    treeViewStructure.SelectedNode = pair.Value;
                }
            }

        }


        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }



        private void btnM3sPing_Click(object sender, EventArgs e)
        {
            SingleSourceXWebServiceClient service = new SingleSourceXWebServiceClient();
            Stopwatch watch = new Stopwatch();
            watch.Start();
            String pong = service.ping();
            watch.Stop();
            MessageBox.Show(pong + " (" + watch.Elapsed + " sec)");
        }



        private void btnShowInfo_Click(object sender, EventArgs e)
        {
            InfoForm infoForm = new InfoForm();
            var docAdapter = Globals.ThisAddIn.WordDocumentManager.ActivateDocumentAdapter(_docAdapter.Instance);
            infoForm.AddSessionInfo(docAdapter.SessionInfo);
            infoForm.Show();
        }

        private void btnM3sCommit_Click(object sender, EventArgs e)
        {
            M3sCommiter m3sUpdater = new M3sCommiter();
            m3sUpdater.Commit(_docAdapter);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            String m3sPath;
            if (string.IsNullOrEmpty(_docAdapter.SessionInfo.M3sFileLocation))
            {
                m3sPath = _docAdapter.Instance.FullName + "." + Constants.M3S_FILE_SUFFIX;
            }
            else
            {
                m3sPath =_docAdapter.SessionInfo.M3sFileLocation;
            }

            ChooseFileDialog chooseM3sDialog = new ChooseFileDialog(m3sPath);
            if (chooseM3sDialog.ShowDialog() == DialogResult.OK)
            {
                var docAdapter = Globals.ThisAddIn.WordDocumentManager.ActivateDocumentAdapter(_docAdapter.Instance);
            
                StructUpdater updater = new StructUpdater();
                updater.Update(chooseM3sDialog.getFilePath(), docAdapter);
                _docAdapter.SessionInfo.M3sFileLocation = chooseM3sDialog.getFilePath();

            }
            chooseM3sDialog.Dispose();
        }



    }
}
