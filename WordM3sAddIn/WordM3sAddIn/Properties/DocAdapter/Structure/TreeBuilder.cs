﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Structure
{
    public class TreeBuilder
    {
        private TreeNode root;

        public Dictionary<Word.Paragraph, TreeNode> para2nodeMap = new Dictionary<Word.Paragraph, TreeNode>();

        public TreeBuilder()
        {
            this.root = new TreeNode();
        }

        public TreeNodeCollection build(StructureBuilder structBuilder)
        {
            var topLevelParagraphs = structBuilder.getTopLevelParagraphs();
            foreach (StructureNode topLevelParagraph in topLevelParagraphs)
            {
                addNewParagraphNode(root, topLevelParagraph);
            }

            return root.Nodes;
        }

        public void addNewParagraphNode(TreeNode parent, StructureNode structureNode)
        {
            TreeNode newNode = new TreeNode(structureNode.ToString());
            newNode.NodeFont = structureNode.GetFont();
            newNode.ForeColor = structureNode.GetColor();
            newNode.Tag = structureNode;

            parent.Nodes.Add(newNode);

            if (structureNode is ParagraphNode)
            {
                para2nodeMap.Add(((ParagraphNode)structureNode).paragraph, newNode);
            }

            foreach (StructureNode child in structureNode.children)
            {
                addNewParagraphNode(newNode, child);
            }
        }

    }
}
