﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Structure
{
    public struct ParagraphStructureBind
    {
        public Word.Paragraph paragraph;
        public ParagraphNode structureNode;
    }

    public class StructureBuilder
    {

        private bool buildFinished = false;

        private StructureNode LastAppearedHeading;
        private TableNode LastAppearedTable;

        private StructureNode Root = new StructureNode();

        private Word.Document Document;
        private List<Word.Table> tables;
        private List<Word.Range> inlineShapeRanges;

        private List<ParagraphStructureBind> paragraphStructureMap;

        public StructureBuilder(Word.Document Document)
        {
            //
            this.Document = Document;
        }

        private StructureNode registerParagraphStructure(ParagraphNode paragraphNode)
        {
            ParagraphStructureBind paraStruct = new ParagraphStructureBind();
            paraStruct.structureNode = paragraphNode;
            paraStruct.paragraph = paragraphNode.paragraph;
            this.paragraphStructureMap.Add(paraStruct);
            return paragraphNode;

        }

        public List<StructureNode> getTopLevelParagraphs()
        {
            return Root.children;
        }

        private void InitTables(Word.Document Document)
        {
            this.tables = new List<Word.Table>();
            foreach (Word.Table table in Document.Tables)
            {
                tables.Add(table);
            }
        }

        private void InitInlineShapes(Word.Document Document)
        {
            this.inlineShapeRanges = new List<Word.Range>();
            foreach (Word.InlineShape inlineShape in Document.Content.InlineShapes)
            {
                inlineShapeRanges.Add(inlineShape.Range);
            }

        }

        private bool IsaInlineShape(Word.Paragraph paragraph)
        {
            Word.Range range = paragraph.Range;

            foreach (Word.Range inlineShpaeRange in inlineShapeRanges)
            {
                if (range.Start <= inlineShpaeRange.Start && range.End >= inlineShpaeRange.End)
                {
                    return true;
                }
            }
            return false;
        }

        private Word.Table IsInTable(Word.Paragraph paragraph)
        {
            //not used becasue Information access is really slow

            //bool inTable = (bool)paragraph.Range.Information[Word.WdInformation.wdWithInTable];
            //if (!inTable)
            //{
            //    return null;
            //}


            Word.Range range = paragraph.Range;
            foreach (Word.Table table in tables)
            {
                if (range.Start >= table.Range.Start && range.End <= table.Range.End)
                {
                    return table;
                }
            }

            return null;
        }

        public void build()
        {
            //LastAppearedTable = null;
            //Root = new StructureNode();
            //paragraphStructureMap = new List<ParagraphStructureBind>();
            //LastAppearedHeading = Root;

            ////
            ////this copy is needed to check references with == operator.
            ////it looks like the document.tables and document.inlineShapes are recreated on every call.
            ////this is the reason why i copy all stuff before in additional lists
            //InitTables(Document);
            //InitInlineShapes(Document);
            ////
            //foreach (Word.Paragraph paragraph in Document.Paragraphs)
            //{
            //    AddParagraph(paragraph);

            //    if (paragraph.ID == null || paragraph.ID.Length == 0)
            //    {
            //        paragraph.ID = OfficeCommon.IdCounter.getNewId().ToString();
            //    }
            //}

            //buildFinished = true;
        }

        private void AddParagraph(Word.Paragraph paragraph)
        {
            int outlineLevel = WordHelper.GetOutlineLevel(paragraph);

            if (outlineLevel > 0) //check if Heading
            {
                InsertHeading(paragraph, outlineLevel);
            }
            else // no Heading
            {
                Word.Table table = IsInTable(paragraph);
                if (table != null)
                {
                    AddParagraphInTable(paragraph, table);
                }
                else
                    if (IsaInlineShape(paragraph))
                    {
                        LastAppearedHeading.children.Add(registerParagraphStructure(new ShapeNode(paragraph)));
                    }
                    else
                    {
                        LastAppearedHeading.children.Add(registerParagraphStructure(new ParagraphNode(paragraph)));
                    }
            }
        }

        private void AddParagraphInTable(Word.Paragraph paragraph, Word.Table table)
        {
            if (LastAppearedTable == null || LastAppearedTable.Table != table)
            {
                LastAppearedTable = new TableNode(table);
                LastAppearedHeading.children.Add(LastAppearedTable);
            }
            LastAppearedTable.children.Add(registerParagraphStructure(new ParagraphNode(paragraph)));
        }

        private void InsertHeading(Word.Paragraph paragraph, int outlineLevel)
        {
            StructureNode currentLastNodeOnLevel = Root;

            for (int curLevel = 1; curLevel < outlineLevel; curLevel++)
            {
                if (currentLastNodeOnLevel.children.Count == 0 || !(currentLastNodeOnLevel.children.Last() is Heading))
                {
                    currentLastNodeOnLevel.children.Add(new EmptyHeadingNode());
                }
                currentLastNodeOnLevel = currentLastNodeOnLevel.children.Last();
            }

            StructureNode newHeading = registerParagraphStructure(new HeadingNode(paragraph));
            currentLastNodeOnLevel.children.Add(newHeading);
            LastAppearedHeading = newHeading;
        }



        public bool checkIntegrity()
        {
            Debug.WriteLine("check integrity");


            if (!buildFinished || Document == null || paragraphStructureMap == null)
            {
                return true;
            }

            try
            {
                if (Document.Paragraphs.Count != this.paragraphStructureMap.Count)
                {
                    return false;
                }


                Word.Paragraph curPara = Document.Paragraphs.First;

                foreach (ParagraphStructureBind psb in paragraphStructureMap)
                {
                    if (curPara == null || curPara.ID != psb.paragraph.ID || curPara.OutlineLevel != psb.structureNode.outlineLevel)
                    {
                        return false;
                    }
                    curPara = curPara.Next();
                }


                return true;

            }
            catch
            {
                return true;
            }
        }
    }
}
