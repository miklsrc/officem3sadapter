﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Structure
{
    public interface Heading
    {

    }

    public interface Moveable
    {
        void delete();
        Word.Range getRange();
    }

    public class StructureNode
    {
        public List<StructureNode> children { get; set; }

        public StructureNode()
        {
            children = new List<StructureNode>();
        }

        public virtual Font GetFont()
        {
            return new Font("Arial", 9, FontStyle.Regular);
        }


        public virtual Color GetColor()
        {
            return Color.Black;
        }
    }

    public class ParagraphNode : StructureNode, Moveable
    {
        public Word.Paragraph paragraph { get; set; }
        public Word.WdOutlineLevel outlineLevel { get; set; }

        public ParagraphNode(Word.Paragraph paragraph)
        {
            this.paragraph = paragraph;
            outlineLevel = paragraph.OutlineLevel;
        }

        public override string ToString()
        {
            return paragraph.Range.Text.ToString() + " [id:" + paragraph.ID + " - " + paragraph.Range.Start.ToString() + "/" + paragraph.Range.End.ToString() + "]";
        }
        public void delete()
        {
            getRange().Delete();
        }

        public virtual Word.Range getRange()
        {
            return paragraph.Range;
        }

    }

    public class HeadingNode : ParagraphNode, Heading
    {
        public HeadingNode(Word.Paragraph paragraph)
            : base(paragraph)
        {

        }

        public override Font GetFont()
        {
            return new Font("Arial", 9, FontStyle.Bold);
        }

        public override Word.Range getRange()
        {
            int start = base.getRange().Start;
            int end = -1;

            for (int i = children.Count - 1; i >= 0; i--)
            {
                if (children[i] is Moveable)
                {
                    end = ((Moveable)children[i]).getRange().End;
                    break;
                }
            }


            if (end >= 0)
            {
                return Globals.ThisAddIn.Application.ActiveDocument.Range(start, end);
            }
            else
            {
                return base.getRange();
            }
        }
    }


    public class EmptyHeadingNode : StructureNode, Heading
    {

        public EmptyHeadingNode()
            : base()
        {

        }

        public override string ToString()
        {
            return "---------";
        }

        public override Font GetFont()
        {
            return new Font("Arial", 9, FontStyle.Bold);
        }
    }


    public class TableNode : StructureNode, Moveable
    {
        public Word.Table Table { get; private set; }

        public TableNode(Word.Table table)
        {
            this.Table = table;
        }

        public override string ToString()
        {
            return "TABLE";
        }

        public override Color GetColor()
        {
            return Color.Green;
        }

        public void delete()
        {
            Table.Delete();
        }
        public Word.Range getRange()
        {
            return Table.Range;
        }
    }

    public class ShapeNode : ParagraphNode, Moveable
    {

        public ShapeNode(Word.Paragraph paragraph)
            : base(paragraph)
        {
        }

        public override string ToString()
        {
            return "SHAPE " + base.ToString();
        }

        public override Color GetColor()
        {
            return Color.Red;
        }
    }
}
