﻿using OfficeCommon;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.M3s;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using WordM3sAddIn.DocAdapter.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Persistence
{
    public class WordDocumentPersistence : InstanceXmlPersistence<WordTextRangeIdMgr>
    {
      
        private List<IdRangeDescription> readFragmentIdsFromFile(string paragraphIdFile)
        {
            List<IdRangeDescription> savedParaIds = new List<IdRangeDescription>();

            if (!File.Exists(paragraphIdFile))
            {
                Debug.WriteLine("paragraph id file does not exist:" + paragraphIdFile);
                return savedParaIds;
            }

            XDocument xmlDoc = XDocument.Load(paragraphIdFile);
            var xmlParagraphs = xmlDoc.Descendants("paragraph");
            foreach (var xmlParagraph in xmlParagraphs)
            {
                int id = Convert.ToInt32(xmlParagraph.Descendants("ID").First().Value.ToString());
                int start = Convert.ToInt32(xmlParagraph.Descendants("Start").First().Value.ToString());
                int end = Convert.ToInt32(xmlParagraph.Descendants("End").First().Value.ToString());
                savedParaIds.Add(new IdRangeDescription(start, end, id));
            }

            return savedParaIds;
        }

        public override void Save(WordTextRangeIdMgr wordTextRangeIdMgr, SessionInfo sessionInfo, string xmlFilePath)
        {

            using (XmlWriter writer = XmlWriter.Create(xmlFilePath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("document");
                WriteDocMetaData(sessionInfo, writer);


                foreach (var idRange in wordTextRangeIdMgr.WordDocText.TextRanges)
                {
                    writer.WriteStartElement("paragraph"); // <-- Write employee element

                    writer.WriteElementString("ID", idRange.Id.ToString());
                    writer.WriteElementString("Start", idRange.TextRange.Start.ToString());
                    writer.WriteElementString("End", idRange.TextRange.End.ToString());

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }



        public override void LoadFragmentIds(WordTextRangeIdMgr wordTextRangeIdMgr, string xmlFilePath)
        {
            List<IdRangeDescription> textRangeDescriptions = readFragmentIdsFromFile(xmlFilePath);
            textRangeDescriptions.Sort();

            //TODO: akternativ implementeruung
            //            if (textRangeDescriptions.Count != _idManager.Document.Paragraphs.Count)
            //            {
            //                throw new Exception("saved paragraph ids count (" + textRangeDescriptions.Count + ")does not fit with paragraph count (" + _idManager.Document.Paragraphs.Count + ")");
            //            }


            int maxId = textRangeDescriptions.Max(n => n.id);
            wordTextRangeIdMgr.IdCounter.setCounter(maxId);

            //
            wordTextRangeIdMgr.AssignIdsToTextRanges(textRangeDescriptions);
        }

        protected override string GetRootElementName()
        {
            return "document";
        }
    }
}
