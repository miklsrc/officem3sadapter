﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.Office.Interop.Word;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using WordM3sAddIn.DocAdapter.TextRangeProcessing;
using M3s = OfficeCommon.M3s;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Persistence
{
    public class StructUpdater
    {

        private SessionInfo _sessionInfo;
        private WordTextRangeIdMgr _wordTextRangeIdMgr;

        public void Update(string m3sFilePath, InstanceAdapter<Document, WordTextRangeIdMgr> docAdapter)
        {
            _wordTextRangeIdMgr = docAdapter.IdFragmentMgr;
            _sessionInfo = docAdapter.SessionInfo;

            var m3sService = new M3s.SingleSourceXWebServiceClient();

            M3s.StructContainer structContainer = m3sService.update(m3sFilePath);
            RecurseStructure(structContainer.rootNode);
        }

        private void RecurseStructure(M3s.StructNode node)
        {
            if (node.representations != null && node.representations.Any())
            {
                if (node.representations[0].version > _sessionInfo.Version)
                {

                    if (node.representations[0].type.Equals("utf8"))
                    {
                        String base64Str = node.representations[0].data;
                        String text = Helper.DecodeBase64ToUtf8(base64Str);

                        IdRange<Word.Range> texTRangeWithId = _wordTextRangeIdMgr.GetTexTRangeWithId(node.id);
                        //TODO: hier wird der zeilenumbruch weggelöscht
                        var range = texTRangeWithId.TextRange.Range;
                        range.SetRange(range.Start, range.End-1);
                        range.Text = text;

                    }
                }
            }

            if (node.children != null)
            {
                foreach (M3s.StructNode child in node.children)
                {
                    RecurseStructure(child);
                }
            }

        }
    }
}