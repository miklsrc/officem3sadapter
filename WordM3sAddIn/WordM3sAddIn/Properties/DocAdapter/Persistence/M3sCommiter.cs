﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OfficeCommon;
using OfficeCommon.Gui;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.M3s;
using WordM3sAddIn.DocAdapter.Gui;
using WordM3sAddIn.DocAdapter.TextRangeProcessing;

namespace WordM3sAddIn.DocAdapter.Persistence
{
    class M3sCommiter
    {
        public void Commit(WordDocumentAdapter docAdapter)
        {
            SingleSourceXWebServiceClient service = new SingleSourceXWebServiceClient();

            StructNode root = new StructNode();
            root.description = "container all content";
            root.children = new StructNode[docAdapter.IdFragmentMgr.WordDocText.TextRanges.Count];


            docAdapter.IdFragmentMgr.UpdateFragments();
            Dictionary<int, List<Document>> rangeDocsMap = docAdapter.FragmentDocAssigner.FragmentDocsMap;
            for (int i = 0; i < docAdapter.IdFragmentMgr.WordDocText.TextRanges.Count; i++)
            {
                var curRange = docAdapter.IdFragmentMgr.WordDocText.TextRanges[i];
                StructNode rangeNode = new StructNode();
                rangeNode.id = curRange.Id;
                rangeNode.representations = new Representation[1];

                String text = Helper.RemoveTerminatingLineBreak(curRange.TextRange.Text);
                String data = Helper.EncodeUtf8ToBase64(text);

                int representationIndex = 0;
                rangeNode.representations[representationIndex] = createRepresentation(data, "utf8");

                assignPepresentationToDoc(rangeNode, rangeDocsMap[rangeNode.id], representationIndex);

                root.children[i] = rangeNode;
            }

            StructContainer structContainer = new StructContainer();
            structContainer.rootNode = root;

            //initial commit, documetns have to set
            structContainer.documents = new Document[docAdapter.SessionInfo.OtherDocuments.Count + 1];
            structContainer.documents[0] = docAdapter.SessionInfo.CurrentDocument;
            for (int i=0;i<docAdapter.SessionInfo.OtherDocuments.Count; i++)
            {
                structContainer.documents[i+1] = docAdapter.SessionInfo.OtherDocuments[i];
            }

            //
            String m3sPath;
            if (string.IsNullOrEmpty(docAdapter.SessionInfo.M3sFileLocation))
            {
                m3sPath = docAdapter.Instance.FullName + "." + Constants.M3S_FILE_SUFFIX;
            }
            else 
            {
                m3sPath = docAdapter.SessionInfo.M3sFileLocation;
            }
            ChooseFileDialog chooseM3sDialog = new ChooseFileDialog(m3sPath);
            if (chooseM3sDialog.ShowDialog() == DialogResult.OK)
            {
                service.commit(chooseM3sDialog.getFilePath(), structContainer);
                docAdapter.SessionInfo.M3sFileLocation = chooseM3sDialog.getFilePath();
            }
            chooseM3sDialog.Dispose();
        }

        private static void assignPepresentationToDoc(StructNode node, List<Document> docs, int representationIndex)
        {
            node.docRepresentations = new DocumentRepresentationPair[docs.Count];

            for (int i = 0; i < docs.Count; i++)
            {
                DocumentRepresentationPair docRepPair = new DocumentRepresentationPair();
                docRepPair.documentId = docs[i].id;
                docRepPair.repIndex = representationIndex;
                node.docRepresentations[i] = docRepPair;
            }
        }

        private static Representation createRepresentation(String data, String type)
        {
            Representation rep = new Representation();
            rep.type = type;
            rep.data = data;
            return rep;
        }

        
    }
}
