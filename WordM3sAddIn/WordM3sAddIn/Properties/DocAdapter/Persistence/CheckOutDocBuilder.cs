﻿using OfficeCommon;
using OfficeCommon.IdManagement.TextRangeProcessing;
using M3s = OfficeCommon.M3s;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using WordM3sAddIn.DocAdapter.Gui;

namespace WordM3sAddIn.DocAdapter.Persistence
{
    class CheckOutDocBuilder
    {

        private Word.Document _wordDoc;
        private List<IdRangeDescription> _idRanges;
        private M3s.Document _sourceM3sDoc;
        private FragmentDocAssigner _fragmentDocAssigner = new FragmentDocAssigner();


        public void CreateFromM3s(string m3sFilePath, string docName)
        {
            try
            {
                var m3sService = new M3s.SingleSourceXWebServiceClient();

                M3s.StructContainer structContainer = m3sService.update(m3sFilePath);
                int maxId=m3sService.getMaxId(structContainer.rootNode);

                M3s.Document newDocument = m3sService.addNewDocument(m3sFilePath,docName);

                //
                this._sourceM3sDoc = ChooseSourceDocument.SelectDocument(structContainer.documents);
                if (this._sourceM3sDoc == null)
                {
                    return;
                }

                object missing = System.Reflection.Missing.Value;
                _wordDoc = Globals.ThisAddIn.Application.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                _idRanges = new List<IdRangeDescription>();

                int level = 0;
                RecurseStructure(structContainer.rootNode, level);

                //
                WordDocumentAdapter docAdapter = (WordDocumentAdapter)Globals.ThisAddIn.WordDocumentManager.ActivateDocumentAdapter(_wordDoc, newDocument);
                docAdapter.IdFragmentMgr.IdCounter.setCounter(maxId);
                docAdapter.IdFragmentMgr.UpdateFragments();
                docAdapter.IdFragmentMgr.AssignIdsToTextRanges(_idRanges);
                docAdapter.SessionInfo.Version = structContainer.version;
                docAdapter.SessionInfo.M3sFileLocation = m3sFilePath;


                foreach (int id in _fragmentDocAssigner.FragmentDocsMap.Keys)
                {
                    if (!docAdapter.FragmentDocAssigner.FragmentDocsMap.ContainsKey(id))
                    {
                        docAdapter.FragmentDocAssigner.FragmentDocsMap.Add(id, new List<M3s.Document>());
                    }
                    docAdapter.FragmentDocAssigner.FragmentDocsMap[id].Add(newDocument);
                    docAdapter.FragmentDocAssigner.FragmentDocsMap[id].AddRange(_fragmentDocAssigner.FragmentDocsMap[id]);
                }


            }
            catch (CommunicationException cex)
            {
                MessageBox.Show("An exception occurred: "+ cex.Message);
            }
        }

        private void RecurseStructure(M3s.StructNode node, int level)
        {
            bool hasChildren = node.children != null && node.children.Count() > 0;

            if (node.representations !=null && node.representations.Count() == 1)
            {
                if (node.representations[0].type.Equals("utf8"))
                {
                    String base64Str = node.representations[0].data;
                    String text = Helper.DecodeBase64ToUtf8(base64Str);
                    int id = node.id;

                    //
                    var assignedDocs=new List<M3s.Document>();
                    assignedDocs.Add(_sourceM3sDoc);
                    _fragmentDocAssigner.FragmentDocsMap.Add(id, assignedDocs);


                    //
                    Microsoft.Office.Interop.Word.Paragraph para = _wordDoc.Content.Paragraphs.Add(System.Reflection.Missing.Value);
                    para.Range.InsertParagraphAfter();
                    para.Range.Text = text;

                    if (hasChildren)
                    {
                        object styleHeading1 = Localisation.GetHeadingName(level);
                        
                        para.Range.set_Style(ref styleHeading1);
                    }

                    _idRanges.Add(new IdRangeDescription(para.Range.Start, para.Range.End, id));
                }
            }
            if (hasChildren)
            {
                foreach (M3s.StructNode child in node.children)
                {
                    RecurseStructure(child, level + 1);
                }
            }

        }

    }
}
