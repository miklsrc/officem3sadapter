﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using OfficeCommon.Gui;
using OfficeCommon.IdManagement;
using WordM3sAddIn.DocAdapter.IdManagement;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.IO;
using WordM3sAddIn.DocAdapter.Persistence;
using Microsoft.Office.Interop.Word;
using WordM3sAddIn.DocAdapter.Gui;
using System.Windows.Forms;
using System.Diagnostics;
using OfficeCommon;


namespace WordM3sAddIn
{
    public partial class ThisAddIn
    {
        private readonly WordDocumentManager _wordDocumentManager = new WordDocumentManager();

        public InstanceManager<Word.Document> WordDocumentManager
        {
            get { return _wordDocumentManager; }
        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {

            Globals.ThisAddIn.Application.DocumentOpen += _wordDocumentManager.InstanceOpenedEvent;

            //helper to be able to use additional save events
            WordSaveHandler wsh = new WordSaveHandler(Globals.ThisAddIn.Application);
            wsh.AfterAutoSaveEvent += DocumentSaved;
            wsh.AfterSaveEvent += DocumentSaved;
            wsh.AfterUiSaveEvent += DocumentSaved;

            Globals.ThisAddIn.Application.DocumentBeforeSave += DocumentSaved;


            //Globals.ThisAddIn.Application.Documents.Open(@"D:\temp\m3s\word.odt");
            //this.Application.Documents.Open(@"D:\temp\draft.docx");
            //this.Application.Documents.Open(@"D:\temp\empty.docx");




        }

        private void DocumentSaved(Word.Document doc, ref bool SaveAsUI, ref bool Cancel)
        {
            Logger.Log("fired file save event by DocumentBeforeSave event", LogLevel.Trace);
            _wordDocumentManager.InstanceSavedEvent(doc);
        }


        private void DocumentSaved(Word.Document doc, bool isClosed)
        {
            Logger.Log("fired file save event by WordSaveHandler", LogLevel.Trace);
            _wordDocumentManager.InstanceSavedEvent(doc);

        }

        public void CreateDocumentFromM3s()
        {

            ChooseFileDialog chooseM3sDialog = new ChooseFileDialog();
            if (chooseM3sDialog.ShowDialog() == DialogResult.OK)
            {
                CheckOutDocBuilder docBuilder = new CheckOutDocBuilder();
                docBuilder.CreateFromM3S(chooseM3sDialog.getFilePath());

            }
            chooseM3sDialog.Dispose();


        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new WordM3sRibbon();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region Von VSTO generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion

        
    }
}
