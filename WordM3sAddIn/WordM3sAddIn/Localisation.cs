﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Core;

namespace WordM3sAddIn
{
    class Localisation
    {

        private static readonly Localisation instance = new Localisation();

        Dictionary<MsoLanguageID, string> languageHeadings = new Dictionary<MsoLanguageID, string>();

        private Localisation()
        {
            init();
        }

        public static Localisation Instance
        {
            get
            {
                return instance;
            }
        }

        private void init()
        {
            string german = "Überschrift";
            languageHeadings[MsoLanguageID.msoLanguageIDGerman] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDGermanAustria] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDGermanLiechtenstein] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDGermanLuxembourg] = german;

            string english = "Heading";
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishAUS] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishBelize] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishCanadian] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishCaribbean] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishIndonesia] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishIreland] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishJamaica] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishNewZealand] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishPhilippines] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishSouthAfrica] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishTrinidadTobago] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishUK] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishUS] = german;
            languageHeadings[MsoLanguageID.msoLanguageIDEnglishZimbabwe] = german;
        }

        public string GetHeadingName(int level)
        {
            var language = Globals.ThisAddIn.Application.Language;
            if (languageHeadings.ContainsKey(language))
            {
                return languageHeadings[language]+" " + level;
            }
            else
            {
                throw new Exception("language not found");
            }

        }
    }
}
