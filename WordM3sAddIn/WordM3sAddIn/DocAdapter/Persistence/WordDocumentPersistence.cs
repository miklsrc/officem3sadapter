﻿using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using WordM3sAddIn.DocAdapter.IdManagement;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Persistence
{
    public class WordDocumentPersistence : InstanceXmlPersistence
    {
        private readonly WordIdFragmentMgrFacade _wordIdFragmentMgrFacade;

        protected override string PersistenceType => "wordDocument";

        public WordDocumentPersistence(FragmentPropertiesMngr fragmentPropertiesMngr, WordIdFragmentMgrFacade wordIdFragmentMgrFacade)
            : base(fragmentPropertiesMngr)
        {
            _wordIdFragmentMgrFacade = wordIdFragmentMgrFacade;
        }

        private int readTextRangesFromXml(XDocument xmlDoc)
        {
            int maxId = 0;
            foreach (var idRangeNode in xmlDoc.Descendants("idRange"))
            {
                int id = Convert.ToInt32(idRangeNode.Attribute("id").Value);
                int start = Convert.ToInt32(idRangeNode.Attribute("start").Value);
                int end = Convert.ToInt32(idRangeNode.Attribute("end").Value);

                maxId = Math.Max(maxId, id);
                _wordIdFragmentMgrFacade.WordTextRangeIdMgr.AssignIdToTextRange(new IdRangeDescription(start, end, id), _wordIdFragmentMgrFacade.WordTextRangeIdMgr.WordDocText);
            }

            return maxId;
        }

        protected override XElement CreateFragmentIdsXml()
        {
            XElement fragmentIdsRoot = new XElement("fragmentIds");

            foreach (var idRange in _wordIdFragmentMgrFacade.WordTextRangeIdMgr.WordDocText.TextRanges)
            {
                fragmentIdsRoot.Add(new XElement("idRange",
                     CreateFragmentCorePropertiesXmlAttributes(idRange.Id),
                     new XAttribute("start", idRange.TextRange.Start.ToString()),
                     new XAttribute("end", idRange.TextRange.End.ToString())
                ));
            }

            return fragmentIdsRoot;
        }

        protected override void LoadFragmentIds(XDocument xmlDoc)
        {
            int maxTextRangeId = readTextRangesFromXml(xmlDoc);
            int maxCounter = EvaluateMaxId(maxTextRangeId);
            _wordIdFragmentMgrFacade.IdCounter.setCounter(maxCounter);
//            _wordIdFragmentMgrFacade.WordTextRangeIdMgr.AssignIdsToTextRanges(textRangeDescriptions);
        }

        private int EvaluateMaxId(int maxTextRangeId)
        {
            int maxIdShapes = 0;


            if (_wordIdFragmentMgrFacade.WordShapeIdMgr.WordShapes.Any())
            {
                maxIdShapes = _wordIdFragmentMgrFacade.WordShapeIdMgr.WordShapes.Max(n => n.Id);
            }

            var maxCounter = Math.Max(maxTextRangeId, maxIdShapes);
            return maxCounter;
        }
    }
}
