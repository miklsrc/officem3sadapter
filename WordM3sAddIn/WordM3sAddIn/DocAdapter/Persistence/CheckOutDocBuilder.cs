﻿using System.Diagnostics;
using System.Drawing;
using System.IO;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using OfficeCommon.m3s.structure;
using OfficeCommon.Service_References.M3s;
using Word = Microsoft.Office.Interop.Word;
using WordM3sAddIn.DocAdapter.Gui;

namespace WordM3sAddIn.DocAdapter.Persistence
{
    class CheckOutDocBuilder
    {

        private Word.Document _wordDoc;
        private List<IdRangeDescription> _idRanges;
        private Dictionary<int, Word.InlineShape> _idShapes;

        //<id,baseversion>
        private Dictionary<int, int> _shapeBaseVersions;
        private ContentBridgeInvoker _contentBridgeInvoker;

        public void CreateFromM3S(string m3sFilePath)
        {
            M3sWebServiceClient m3sService = new M3sWebServiceClient();
            //            M3sServiceProxy.Using((delegate (M3sWebServiceClient m3sService)
            //            {
            StructNode structNode = m3sService.update(m3sFilePath);
            int maxId = m3sService.getMaxId(structNode);

            object missing = System.Reflection.Missing.Value;
            _wordDoc = Globals.ThisAddIn.Application.Documents.Add(ref missing, ref missing, ref missing, ref missing);
            _idRanges = new List<IdRangeDescription>();
            _idShapes = new Dictionary<int, Word.InlineShape>();
            _shapeBaseVersions = new Dictionary<int, int>();



            WordDocumentAdapter docAdapter = (WordDocumentAdapter)Globals.ThisAddIn.WordDocumentManager.ActivateDocumentAdapter(_wordDoc);
            this._contentBridgeInvoker = docAdapter.ContentBridgeInvoker;

            int level = 0;
//            RecurseStructure(structNode, level);

            docAdapter.WordIdFragmentMgrFacade.IdCounter.setCounter(maxId);
            docAdapter.WordIdFragmentMgrFacade.RefreshFragments();
            docAdapter.WordIdFragmentMgrFacade.WordShapeIdMgr.AssignIdsToShapes(_idShapes);

            foreach (var wordShape in docAdapter.WordIdFragmentMgrFacade.WordShapeIdMgr.WordShapes)
            {
                wordShape.Shape.SetBaseversionAlternativeText(_shapeBaseVersions[wordShape.Id]);
            }

            docAdapter.SessionInfo.M3sFileLocation = m3sFilePath;
            docAdapter.UpdateFromM3S(structNode);
            //            }));
        }

        private void RecurseStructure(StructNode node, int level)
        {
            bool hasChildren = node.children != null && node.children.Any();

            if (node.sharedcontent != null)
            {



//                _contentBridgeInvoker.Insert(node.id, RealizationFactory.Create(node.sharedcontent));
                //                string base64Str = node.sharedcontent.data;
                //                int id = node.id;
                //                //
                //                Word.Paragraph para = _wordDoc.Content.Paragraphs.Add(System.Reflection.Missing.Value);
                //
                //                if (node.sharedcontent.type.Equals("utf8"))
                //                {
                //                    para.Range.InsertParagraphAfter();
                //                    string text = Helper.DecodeBase64ToUtf8(base64Str);
                //                    para.Range.Text = text;
                //
                //                    if (hasChildren)
                //                    {
                //                        object styleHeading1 = Localisation.Instance.GetHeadingName(level);
                //
                //                        para.Range.set_Style(ref styleHeading1);
                //                    }
                //
                //                    _idRanges.Add(new IdRangeDescription(para.Range.Start, para.Range.End, id));
                //                }
                //                else
                //                if (node.sharedcontent.type.Equals("image"))
                //                {
                //                    Image image = Helper.Base64ToImage(base64Str);
                //                    string imagePath = "d:/temp/img.img"; //TODO generic folder
                //                    image.Save(imagePath);
                //
                //                    WordHelper.SetInlineshapeAlternativetext(para.Range, node.sharedcontent.version.ToString());
                //
                //                    Word.InlineShape inlineShape = para.Range.InlineShapes.AddPicture(imagePath, Type.Missing, Type.Missing, Type.Missing);
                //
                //                    _idShapes.Add(id, inlineShape);
                //                    _shapeBaseVersions.Add(id, node.sharedcontent.version);
                //                    Debug.Print("image added with range: " + idRangeDescription);
                //
                //                }
            }
            if (hasChildren)
            {
                foreach (StructNode child in node.children)
                {
                    RecurseStructure(child, level + 1);
                }
            }

        }

    }
}
