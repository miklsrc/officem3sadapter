﻿using System.Collections.Generic;
using Microsoft.Office.Interop.Word;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;

namespace WordM3sAddIn.DocAdapter.ContentBridge
{
    public abstract class WordContentBridge : OfficeCommon.IContentBridge
    {
        protected readonly WordTextRangeIdMgr IdFragmentMgr;
        protected readonly Document WordDocument;
        protected readonly WordDocumentAdapter WordDocumentAdapter;

        protected WordContentBridge(WordDocumentAdapter wordDocumentAdapter)
        {
            this.IdFragmentMgr = wordDocumentAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr;
            this.WordDocument = wordDocumentAdapter.WordInstance;
            this.WordDocumentAdapter = wordDocumentAdapter;
        }



        public abstract string ToBase64(int id);

        public void Replace(int id, Realization real)
        {
            IdRange<Range> idRangeById = IdFragmentMgr.WordDocText.GetIdRangeById(id);//TODO works only for UTF8 not for image
            if (idRangeById == null)
            {
                //TODO: insert new element
                return; //document don't have a fragment with the aked insertFragmentId -> no replacement -> insert
            }
            AbstractTextRange<Range> textRange = idRangeById.TextRange;

            //if the line has a ending line break (and there should be one, except the last line in the document) 
            //we want leave this line break and only change the content
            int lengthTextBreak = 0;
            if (Helper.HasExactlyOneLineBreakAtEnd(textRange.Text))
            {
                lengthTextBreak = Constants.LINE_BREAK.Length;
            }

            //select text WITHOUT line break
            var range = textRange.Range;
            range.SetRange(range.Start, range.End - lengthTextBreak);

            DoReplaceContent(real, textRange, lengthTextBreak);
        }

        public abstract void Insert(List<int> previousFragmentIds, int insertFragmentId, Realization real, int level, List<IdRangeDescription> idRangeDescs);

        
        protected abstract void DoReplaceContent(Realization realization,AbstractTextRange<Microsoft.Office.Interop.Word.Range> textRange, int lengthTextBreak);
    }
}