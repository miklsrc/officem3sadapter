﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Office.Interop.Word;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;

namespace WordM3sAddIn.DocAdapter.ContentBridge
{
    public class WordContentBridgeUtf8 : WordContentBridge
    {
        private WordDocumentAdapter _wordDocumentAdapter;

        public WordContentBridgeUtf8(WordDocumentAdapter wordDocumentAdapter) : base(wordDocumentAdapter)
        {
            _wordDocumentAdapter = wordDocumentAdapter;
        }

        public override string ToBase64(int id)
        {
            IdRange<Range> idRange = IdFragmentMgr.WordDocText.GetIdRangeById(id);
            string str = Helper.RemoveTerminatingLineBreak(idRange.TextRange.Text).Trim();
            return Helper.EncodeUtf8ToBase64(str);
        }


        public override void Insert(List<int> previousFragmentIds, int insertFragmentId, Realization real, int level, List<IdRangeDescription> idRangeDescs)
        {
            Range insertHereRange = null;
            if (!previousFragmentIds.Any())
            {
                //case 1: insert at beginning of document

                if (WordDocumentAdapter.WordInstance.Paragraphs.Count <= 1)
                {
                    //case 1a : document is empty (has one starting paragraph)
                    insertHereRange = WordDocumentAdapter.WordInstance.Range(0, 0);
                    insertHereRange.InsertParagraphAfter();
                }
                else
                {
                    //case 1b : document is not empty
                    //TODO refactor

                    //when inserting a new paragraph at the begin of a document I couldn't find a possibilty
                    // to split this new range form the following range (previous first paragraaph). this always results in a "big" 
                    // range which starting by index 0 and ending t the end of the second paragrpah.
                    //to fix this error, I set the of the second paragraph manually
                    //step 1: get range of the "old" first paragraph
                    var wordTextRangeIdMgr = WordDocumentAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr;
                    var idRange = wordTextRangeIdMgr.WordDocText.TextRanges.Find(a => a.TextRange.Start == 0);

                    //insert empty range begin
                    WordDocumentAdapter.WordInstance.Paragraphs.First.Range.InsertParagraphBefore();
                    Range range = WordDocumentAdapter.WordInstance.Range(0, 0);
                    Paragraph para = range.Paragraphs.Add(System.Reflection.Missing.Value);
                    para.Range.InsertParagraphAfter();
                    insertHereRange = range.Next(WdUnits.wdParagraph);
                    WordDocumentAdapter.WordInstance.Paragraphs.First.Range.Delete();
                    //insert empty range end

                    //step2: split the range manually by leave out the 2 inserted empty paragrpahs (\r\r)
                    idRange.TextRange = new WordTextRange(wordTextRangeIdMgr.Document.Range(2,idRange.TextRange.End));
                }

                Debug.Print("case 1:"+ insertFragmentId);
              
            }
            else
            {
                Range range = null;
                for (int i= previousFragmentIds.Count-1; i>=0 ; i-- )
                {
                    int fragmentId=previousFragmentIds[i];
                    range = WordDocumentAdapter.WordIdFragmentMgrFacade.GetRangeById(fragmentId);
                    if (range != null)
                    {
                        break;
                    }
                    else
                    {
                        IdRangeDescription idRangeDesc = idRangeDescs.Find(a => a.Id == fragmentId);
                        if (idRangeDesc != null)
                        {
                            range = IdFragmentMgr.Document.Range(idRangeDesc.Start, idRangeDesc.End);
                            break;
                        }
                    }
                }

                
                if (range != null)
                {
                    //case 2: predecessorFragment exist, insert after this
                    //TODO : scheint zu funktionieren
                    Paragraph para = range.Paragraphs.Add(System.Reflection.Missing.Value);
                    para.Range.InsertParagraphAfter();
                    insertHereRange = range.Next(WdUnits.wdParagraph);



                    Debug.Print("case 2:" + insertFragmentId);
                }
                else
                {
                    //case 3: predecessorFragment couldn't be found -> insert at end of document
                    //this can happen during initial check out when ...
                    range = WordDocumentAdapter.WordInstance.Range();
                    range.InsertParagraphAfter();
                    Paragraph para =WordDocumentAdapter.WordInstance.Content.Paragraphs.Add(System.Reflection.Missing.Value);
                    insertHereRange = para.Range;
                    Debug.Print("case 3:" + insertFragmentId);
                }
            }

            insertHereRange.Text = real.Data;
            if (level > 0)
            {
                object styleHeading1 = Localisation.Instance.GetHeadingName(level);
                insertHereRange.set_Style(ref styleHeading1);
            }
            

            var newIdRangeDesc = new IdRangeDescription(insertHereRange.Start, insertHereRange.End, insertFragmentId);
            IdFragmentMgr.AssignIdToTextRange(newIdRangeDesc, IdFragmentMgr.WordDocText);
            idRangeDescs.Add(newIdRangeDesc);
        }

        protected override void DoReplaceContent(Realization realization, AbstractTextRange<Range> textRange, int lengthTextBreak)
        {
            string base64Str = realization.Base64Data;
            var range = textRange.Range;
            string text = Helper.DecodeBase64ToUtf8(base64Str);

            if (!Helper.RemoveTerminatingLineBreak(textRange.Text).Equals(text)
            ) //only apply different content (this preserve for example specific formattings)
            {
                //set text
                range.Text = text;

                //select text WITH line break
                range.SetRange(range.Start, range.End + lengthTextBreak);
                textRange.Range = range;
            }
        }
    }
}