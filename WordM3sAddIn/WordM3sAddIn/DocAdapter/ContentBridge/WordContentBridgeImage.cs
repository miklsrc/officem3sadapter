﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using WordM3sAddIn.DocAdapter.IdManagement;
using WordM3sAddIn.DocAdapter.IdManagement.ShapeProcessing;
using WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;

namespace WordM3sAddIn.DocAdapter.ContentBridge
{
    public class WordContentBridgeImage : WordContentBridge
    {
        private WordIdFragmentMgrFacade _idFragmentMgr;

        public WordContentBridgeImage(WordDocumentAdapter wordDocumentAdapter) : base(wordDocumentAdapter)
        {
            this._idFragmentMgr = wordDocumentAdapter.WordIdFragmentMgrFacade;
        }

        public override string ToBase64(int id)
        {
            IdShape<WordShape> idShape = _idFragmentMgr.WordShapeIdMgr.GetIdShapeById(id);
            string shapeImgBase64Data = WordHelper.ConvertInlineShapeToJpgBase64(idShape.Shape, IdFragmentMgr.Document);
            return shapeImgBase64Data;
        }


        public override void Insert(List<int> previousFragmentIds, int insertFragmentId, Realization real, int level, List<IdRangeDescription> idRangeDescs)
        {
            Range insertHereRange = null;
            if (!previousFragmentIds.Any())
            {
                //case 1: insert at beginning of document
                insertHereRange = WordDocumentAdapter.WordInstance.Range(0, 0);
            }
            else
            {
                Range range = null;
                for (int i = previousFragmentIds.Count - 1; i >= 0; i--)
                {
                    int fragmentId = previousFragmentIds[i];
                    range = WordDocumentAdapter.WordIdFragmentMgrFacade.GetRangeById(fragmentId);
                    if (range != null)
                    {
                        break;
                    }
                }

                if (range != null)
                {
                    //TODO : NOT TESTED
                    //case 2: predecessorFragment exist, insert after this
                    range.InsertParagraphAfter(); 
                    range.Start = range.End;
                    insertHereRange = range;
                }
                else
                {
                    //case 3: predecessorFragment couldn't be found -> insert at end of document
                    //this can happen during initial check out when ...

                    WordDocumentAdapter.WordInstance.Content.Paragraphs.Add(System.Reflection.Missing.Value);
                    insertHereRange = WordDocumentAdapter.WordInstance.Range();
                    insertHereRange.Start = insertHereRange.End;
                }
            }
            Image image = Helper.Base64ToImage(real.Base64Data);
            string imagePath = "d:/temp/img.img"; //TODO generic folder
            image.Save(imagePath);

            InlineShape inlineShape = insertHereRange.InlineShapes.AddPicture(imagePath, Type.Missing, Type.Missing, Type.Missing);
            _idFragmentMgr.WordShapeIdMgr.AssignIdToShape(insertFragmentId, inlineShape);
        }

        protected override void DoReplaceContent(Realization realization, AbstractTextRange<Microsoft.Office.Interop.Word.Range> textRange, int lengthTextBreak)
        {
            throw new NotImplementedException();

            //            string base64Str = realization.Base64Data;
            //            var range = textRange.Range;
            //            var inlineshapeHelper = new InlineshapeHelper(IdFragmentMgr.Document);
            //            Microsoft.Office.Interop.Word.InlineShape inlineShape = inlineshapeHelper.GetInlineShapeAtRange(textRange);
            //
            //            if (!inlineShape.AlternativeText.Equals(realization.BaseVersion.ToString()))
            //            {
            //                                inlineShape.Delete();
            //                //                Image image = Helper.Base64ToImage(base64Str);
            //                //                Clipboard.SetImage(image);
            //                //                range.Paste();
            //
            //                string imagePath = @"D:\temp\img.jpg";
            //                Microsoft.Office.Interop.Word.InlineShape iShape = range.InlineShapes.AddPicture(imagePath, Type.Missing, Type.Missing, Type.Missing);
            //
            //                WordHelper.SetInlineshapeAlternativetext(textRange.Range, realization.BaseVersion.ToString());
            //            }
        }
    }
}