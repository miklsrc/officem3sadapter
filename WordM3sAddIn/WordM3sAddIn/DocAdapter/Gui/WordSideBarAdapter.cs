﻿using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using OfficeCommon.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeCommon.Gui.RightClickInteraction;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using WordM3sAddIn.DocAdapter.IdManagement;
using WordM3sAddIn.DocAdapter.Structure;
using BrightIdeasSoftware;
using System.Collections;
using System.Drawing;
using System.Runtime.Remoting;
using OfficeCommon.Gui.Debug;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Gui
{
    class WordSideBarAdapter : SideBar<Word.Document, WordIdFragmentMgrFacade>
    {
        private Outline _outlineView;
        private WordDocInfoFetcher _docInfoFetcher;
        private TreeStructBuilder _treeStructBuilder;
        private TreeView _shapeTreeView;
        private WordDocumentAdapter _wordDocAdapter;

        public WordSideBarAdapter(WordDocumentAdapter wordDocAdapter)
            : base(wordDocAdapter)
        {
            _wordDocAdapter = wordDocAdapter;
            _outlineView = new Outline(InstanceAdapter.SessionInfo);
            _treeStructBuilder = new TreeStructBuilder(wordDocAdapter);

            M3SOutline.RightClickInteractionManager.registerInteraction(new RightClickGroupInteraction(wordDocAdapter.FragmentPropertiesMngr));

            M3SOutline.TreeList.MouseDoubleClick += TreeList_DoubleClick;
            M3SOutline.TreeList.HideSelection = false;
            M3SOutline.TreeList.UseCustomSelectionColors = true;
            M3SOutline.TreeList.UnfocusedHighlightBackgroundColor = Color.LightBlue;

            AddTab(_outlineView, "Outline");
            //
            //wordDocAdapter.Instance.Application.WindowSelectionChange += SelectionChanged;


            //
            var treeViewInfo = new System.Windows.Forms.TreeView();
            _docInfoFetcher = new WordDocInfoFetcher(treeViewInfo);
            AddTab(treeViewInfo, "info");
            AddUserAction("refresh info", ActionRefreshInfo);

            //
            AddUserAction("refresh outline", ActionRefreshOutline);
            AddUserAction("refresh m3s outline", ActionRefreshM3SOutline);


            _shapeTreeView = new TreeView();
            AddTab(_shapeTreeView, "Shape Outline");

            AddUserAction("refresh shapes  outline", ActionRefreshShapesOutline);
        }



        //TODO check paragraphmover!!!! this one supports tables
        protected override void HandleModelDropped(object sender, ModelDropEventArgs e)
        {
            e.Handled = true;

            IEnumerable<M3SNode> sourceModels = e.SourceModels.Cast<M3SNode>();
            M3SNode source = sourceModels.First(x => _wordDocAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr.GetTextRangeById(x.Id) != null);
            M3SNode target = (M3SNode)e.TargetModel;


            IdRange<Word.Range> sourceIdRange =
                _wordDocAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr.GetTextRangeById(source.Id);
            IdRange<Word.Range> afterTargetIdRange =
                _wordDocAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr.GetTextRangeById(target.Id);

            Word.Range sourceRange = sourceIdRange.TextRange.Range;
            bool sourceIsDocEnd = sourceRange.Paragraphs.Last.Next() == null;

            //after copying, all the new inserted ranges AND the fallowing (old target) range have wrong IDs
            //for this reason, we need to memorize these IDs and reassign them manually after the copy-step
            int targetId = afterTargetIdRange.Id;
            Queue<int> sourceIds = new Queue<int>();
            foreach (Word.Paragraph paragraph in sourceRange.Paragraphs)
            {
                sourceIds.Enqueue(_wordDocAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr.GetIdRange(paragraph.Range).Id);
            }

            //1. copy new and 2. delete old

            //1. copy new
            //Word.Range insertTargetRange = insertBeforeTarget(targetMoveable, sourceMoveable);
            afterTargetIdRange.TextRange.Range.InsertParagraphBefore();
            //inserting a paragrpah before a range, has the consequence, 
            //that the range addresses afterwars itself and the before added paragraph.
            //this means that after adding the paragraph before, the range contains 2 paragrpahs
            Word.Range insertTargetRange = afterTargetIdRange.TextRange.Range.Paragraphs.First.Range;
            insertTargetRange.FormattedText = sourceIdRange.TextRange.Range.FormattedText;

            //2. delete old
            //deleteSource(sourceMoveable, sourceIsDocEnd);
            sourceIdRange.TextRange.Range.Delete();

            //reassign the old ids to the copied ranges and the following (old target) range
            var forcedIds = new List<IdFragmentDescription>();
            foreach (Word.Paragraph paragraph in insertTargetRange.Paragraphs)
            {
                forcedIds.Add(new IdRangeDescription(paragraph.Range.Start, paragraph.Range.End, sourceIds.Dequeue()));
            }
            Word.Range newTargetRange = insertTargetRange.Next().Paragraphs.First.Range; //this is the following (old target) range, which has now a wrong id
            forcedIds.Add(new IdRangeDescription(newTargetRange.Start, newTargetRange.End, targetId));
            _wordDocAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr.RefreshFragments(forcedIds);

            ActionRefreshM3SOutline(null,null);
        }


        protected override void HandleModelCanDrop(object sender, ModelDropEventArgs e)
        {
            e.Handled = true;
            e.Effect = DragDropEffects.None;

            Debug.Print(e.DropTargetLocation.ToString());
            if (e.DropTargetLocation == DropTargetLocation.AboveItem && !DragNDropHelper.IsDropBeforeItself(e) &&
                !DragNDropHelper.IsDropAfterItself(e))
            {
                if (e.TargetModel is M3SNode)
                {
//                    M3sNode m3sNode = (M3sNode)e.TargetModel;
//                    IEnumerable<M3sNode> sourceModels = e.SourceModels.Cast<M3sNode>();
                    e.Effect = DragDropEffects.Move;
                }
            }
        }

        private void TreeList_DoubleClick(object sender, MouseEventArgs e)
        {
            int index = M3SOutline.TreeList.GetItemAt(e.X, e.Y).Index;
            OLVListItem item = M3SOutline.TreeList.GetItem(index);
            if (item != null && item.RowObject != null && item.RowObject is M3SNode)
            {
                int fragmentId = ((M3SNode)item.RowObject).Id;
                var range = _wordDocAdapter.WordIdFragmentMgrFacade.GetRangeById(fragmentId);
                if (range != null)
                {
                    _wordDocAdapter.WordInstance.Range(range.Start, range.Start).Select();
                }
            }
        }



        //private void SelectionChanged(Selection sel)
        //{
        //    ActionRefreshOutline(null,null);
        //    ActionRefreshM3sOutline(null, null);

        //    var idRange = _wordDocAdapter.IdFragmentMgr.WordDocText.GetIdRange(sel.Start);
        //    //TODO expand subtree which incldes fragment with id

        //    var roots=(IList)m3sOutline.TreeList.Roots;
        //    List<M3sNode> hierarchierNodes = new List<M3sNode>();
        //    foreach (M3sNode root in roots)
        //    {
        //        hierarchierNodes.Add(root);
        //        ExpandReeNodeWithId(hierarchierNodes, idRange.Id);

        //        hierarchierNodes.RemoveAt(hierarchierNodes.Count-1);
        //    }
        //}

        //private void ExpandReeNodeWithId(List<M3sNode> hierarchierNodes, int id)
        //{
        //    var currentNode=hierarchierNodes.Last();
        //    if (currentNode.Id == id)
        //    {
        //        //runnign through the hierarchie and exapnd all parent trees
        //        foreach (M3sNode node in hierarchierNodes)
        //        {
        //            m3sOutline.TreeList.Expand(node);
        //        }

        //        m3sOutline.TreeList.SelectObject(currentNode);
        //        m3sOutline.TreeList.EnsureModelVisible(currentNode);
        //    }
        //    else
        //    {
        //        foreach (M3sNode node in currentNode.Children)
        //        {
        //            hierarchierNodes.Add(node);
        //            ExpandReeNodeWithId(hierarchierNodes, id);

        //            hierarchierNodes.RemoveAt(hierarchierNodes.Count - 1);
        //        }

        //    }
        //}

        private void ActionRefreshOutline(object sender, EventArgs e)
        {
            InstanceAdapter.IdFragmentMgr.RefreshFragments();
            List<AbstractStructTreeNode> treeRoots = _treeStructBuilder.BuildNewTree();
            _outlineView.TreeList.CanExpandGetter = delegate(object x) { return ((AbstractStructTreeNode)x).ChildrenCount > 0; };
            _outlineView.TreeList.ChildrenGetter = delegate(object x) { return ((AbstractStructTreeNode)x).Children; };

            _outlineView.TreeList.SetObjects(treeRoots);
        }

        private void ActionRefreshShapesOutline(object sender, EventArgs e)
        {
            _shapeTreeView.Nodes.Clear();
            TreeNode rootNode = new TreeNode("shapes");
           _shapeTreeView.Nodes.Add(rootNode);

            foreach (var entry in _wordDocAdapter.WordIdFragmentMgrFacade.WordShapeIdMgr.WordShapes)
            {

                TreeNode shapeNode = new TreeNode("shapes "+entry.Shape.AlternativeText);

                rootNode.Nodes.Add(shapeNode);
            }

         
        }

        private void ActionRefreshInfo(object sender, EventArgs e)
        {
            _docInfoFetcher.refreshWordDocInfoTree();
        }


    }
}
