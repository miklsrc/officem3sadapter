﻿using System.Diagnostics;
using OfficeCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;
using WordM3sAddIn.DocAdapter.Persistence;
using WordM3sAddIn.DocAdapter.Structure;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter
{

    class ParagraphMover
    {
        private WordTextRangeIdMgr _idFragmentMgr;

        public ParagraphMover(WordTextRangeIdMgr idFragmentMgr)
        {
            this._idFragmentMgr = idFragmentMgr;
        }

       

        private Word.Range insertBeforeTarget(StructureNode targetMoveable, StructureNode sourceMoveable)
        {
            Word.Range insertTargetRange = null;
            Word.Range range = sourceMoveable.getRange();

            if (targetMoveable is TableNode)
            {
                TableNode tableNode = ((TableNode)targetMoveable);
                

                //there are big problems inserting text BEFORE a table without inserting INTO it
                //main idea from here: http://go4answers.webhost4life.com/Example/word-2007vba-insert-text-before-59715.aspx
                Word.Table table = tableNode.Table;
                table.Rows.Add(table.Rows[1]);

                //splits the table into 2 tables, there appears a empty idRange between both tables 
                //(the new first seperated empty 1-row table belongs now to the "table" var. the "old" table isn't any longer referenced by this variable)
                table.Split(2);

                // get the "old" table
                Word.Range oldTableRange = table.Range.Next().Next();

                //delete the empty "new" table
                table.Delete();

                oldTableRange.Previous().InsertParagraphBefore();
                insertTargetRange = oldTableRange.Previous().Previous();
                insertTargetRange.FormattedText = range.FormattedText;

                //don't know why, but a empty idRange is left => delete
                insertTargetRange.Next().Delete();
            }
            else
            {

                targetMoveable.getRange().InsertParagraphBefore();
                if (sourceMoveable is TableNode)
                {
                    insertTargetRange = targetMoveable.getRange().Previous(Word.WdUnits.wdParagraph);
                }
                else
                {
                    //inserting a paragrpah before a range, has the consequence, 
                    //that the range addresses afterwars itself and the before added paragraph.
                    //this means that after adding the paragraph before, the range contains 2 paragrpahs
                    insertTargetRange = targetMoveable.getRange().Paragraphs.First.Range;
                }

                insertTargetRange.FormattedText = range.FormattedText;
            }

            return insertTargetRange;
        }


        private void deleteSource(StructureNode sourceMoveable, bool sourceIsDocEnd)
        {
            Word.Range range = sourceMoveable.getRange();

            if (!sourceIsDocEnd && (bool)range.Next().Information[Word.WdInformation.wdWithInTable])
            {
                //for some strange reason deletion directly before a table leaves an empty line => delete this too
                range.Previous().Delete();
                range.Delete();
            }
            else
            {
                sourceMoveable.delete();
            }
        }

        public Word.Range moveParagraph(StructureNode sourceMoveable, StructureNode targetMoveable, Target direction)
        {

            if (direction == Target.Top)
            {

                Word.Range sourceRange = sourceMoveable.getRange();
                bool sourceIsDocEnd = sourceRange.Paragraphs.Last.Next() == null;

                //after copying, all the new inserted ranges AND the fallowing (old target) range have wrong IDs
                //for this reason, we need to memorize these IDs and reassign them manually after the copy-step
                int targetId = _idFragmentMgr.GetIdRange(targetMoveable.getRange().Paragraphs.First.Range).Id;
                Queue<int> sourceIds = new Queue<int>();
                foreach (Word.Paragraph paragraph in sourceRange.Paragraphs)
                {
                    sourceIds.Enqueue(_idFragmentMgr.GetIdRange(paragraph.Range).Id);
                }

                //copy ew and delete old
                Word.Range insertTargetRange = insertBeforeTarget(targetMoveable, sourceMoveable);
                deleteSource(sourceMoveable, sourceIsDocEnd);

                //reassign the old ids to the copied ranges and the following (old target) range
                var forcedIds = new List<IdFragmentDescription>();
                foreach (Word.Paragraph paragraph in insertTargetRange.Paragraphs)
                {
                    forcedIds.Add(new IdRangeDescription(paragraph.Range.Start, paragraph.Range.End, sourceIds.Dequeue()));
                }
                Word.Range newTargetRange = insertTargetRange.Next().Paragraphs.First.Range; //this is the following (old target) range, which has now a wrong id
                forcedIds.Add(new IdRangeDescription(newTargetRange.Start, newTargetRange.End, targetId));
                _idFragmentMgr.RefreshFragments(forcedIds);


                return insertTargetRange;
            }

            return null;
        }

    }
}
