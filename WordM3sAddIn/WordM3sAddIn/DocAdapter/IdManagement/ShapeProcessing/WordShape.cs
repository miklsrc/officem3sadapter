using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.IdManagement.ShapeProcessing
{
    public abstract class WordShape
    {
        private const string idRegexPattern = "\\[id=\\d+\\]";
        private const string baseversionRegexPattern = "\\[baseversion=\\d+\\]";

        public abstract Range GetRange();

        public string GetBaseversionAlternativeText()
        {
            Match match = Regex.Match(AlternativeText, baseversionRegexPattern);
            if (match.Success)
            {
                string str = match.Captures[0].Value;
                str = str.Substring(13, str.Length - 14);

                return str;
            }
            return "";
        }

        public string GetIdAlternativeText()
        {
            Match match = Regex.Match(AlternativeText, idRegexPattern);
            if (match.Success)
            {
                string str = match.Captures[0].Value;
                str = str.Substring(4, str.Length - 5);

                return str;
            }
            return "";
        }

        public void SetIdAlternativeText(int id)
        {
            var text = RemovePattern(idRegexPattern);

            text += "[id=" + id + "]";
            AlternativeText = text;
        }

        public void SetBaseversionAlternativeText(int baseversion)
        {
            var text = RemovePattern(baseversionRegexPattern);

            text += "[baseversion=" + baseversion + "]";
            AlternativeText = text;
        }

        private string RemovePattern(string regexPattern)
        {
            string text = AlternativeText;
            Regex rgx = new Regex(regexPattern);
            text = rgx.Replace(text, "");
            return text;
        }

        public abstract string AlternativeText { get; protected set; }

        public abstract void Select();
    }
}