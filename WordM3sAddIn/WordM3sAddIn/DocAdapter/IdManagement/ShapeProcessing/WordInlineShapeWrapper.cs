using Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.IdManagement.ShapeProcessing
{
    public class WordInlineShapeWrapper : WordShape
    {
        public InlineShape Shape { get; set; }

        public override Range GetRange()
        {
            return Shape.Range;
        }

        public WordInlineShapeWrapper(InlineShape shape)
        {
            Shape = shape;
        }

        public override string AlternativeText
        {
            get { return Shape.AlternativeText ?? ""; }
            protected set { Shape.AlternativeText = value; }
        }

        public override void Select()
        {
            Shape.Select();
        }
    }
}