using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Office.Interop.Word;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;

namespace WordM3sAddIn.DocAdapter.IdManagement.ShapeProcessing
{
    public class WordShapeIdMgr : AbstractIdFragmentMgr
    {
        private Document _document;
        public List<IdShape<WordShape>> WordShapes { get; private set; }

        public WordShapeIdMgr(Document document, IdCounter idCounter)
            : base(idCounter)
        {
            this._document = document;
            WordShapes = new List<IdShape<WordShape>>();
        }

        private List<WordShape> GetCurrentWordShapes()
        {
            List<WordShape> newWordShapes = new List<WordShape>();
            foreach (InlineShape shape in _document.Content.InlineShapes)
            {
                newWordShapes.Add(new WordInlineShapeWrapper(shape));
            }
            foreach (Shape shape in _document.Shapes)
            {
                newWordShapes.Add(new WordShapeWrapper(shape));
            }
            return newWordShapes;
        }

        private List<IdShape<WordShape>> GetCurrentIdWordShapes()
        {
            List<IdShape<WordShape>> newIdWordShapes = new List<IdShape<WordShape>>();

            foreach (WordShape newWordShape in GetCurrentWordShapes())
            {
                string alternativeText = newWordShape.GetIdAlternativeText();
                if (!int.TryParse(alternativeText, out int id))
                {
                    id = IdCounter.getNewId();
                    newWordShape.SetIdAlternativeText(id);
                }
                newIdWordShapes.Add(new IdShape<WordShape>(newWordShape, id));
            }
            return newIdWordShapes;
        }

        public override void RefreshFragments()
        {
            List<IdShape<WordShape>> newIdWordShapes = GetCurrentIdWordShapes();

            // remove old
            for (int i = WordShapes.Count - 1; i >= 0; i--)
            {
                IdShape<WordShape> oldOtherShape = WordShapes[i];
                if (newIdWordShapes.Count(n => n.Id == oldOtherShape.Id) == 0)
                {
                    int removeId = WordShapes[i].Id;
                    WordShapes.RemoveAt(i);
                    FireIdFragmentRemovedEvent(removeId);
                }
            }

            //add new
            foreach (IdShape<WordShape> newShape in newIdWordShapes)
            {
                IdShape<WordShape> oldIdWordShape = WordShapes.Find(n => n.Id == newShape.Id);
                if (oldIdWordShape == null)
                {
                    WordShapes.Add(newShape);
                    FireIdFragmentAddedEvent(newShape.Id);
                }
                else
                {
                    oldIdWordShape.Shape = newShape.Shape; //don't really  know why copying. Idea is: always to have the newest "shape"-reference inside the oldIdWordshape colelction (don't know if the shapes really can be changed since the last refresh)
                }
            }

            // assign ids manually added shapes
            if (_idAssignments.Any())
            {
                foreach (var idShape in _idAssignments)
                {
                    foreach (var idWordShape in WordShapes)
                    {
                        if (idWordShape.Shape.GetRange().Start == idShape.Value.Range.Start)
                        {
                            idWordShape.Id = idShape.Key;
                            idWordShape.Shape.SetIdAlternativeText(idShape.Key);
                        }
                    }
                }
                _idAssignments.Clear();
            }
        }

        public override HashSet<int> GetAllFragmentIds()
        {
            HashSet<int> fragmentIds = new HashSet<int>();

            foreach (var fragment in WordShapes)
            {
                if (!fragmentIds.Add(fragment.Id))
                {
                    throw new Exception("duplicate ids");
                }
            }

            return fragmentIds;
        }

        private Dictionary<int, InlineShape> _idAssignments = new Dictionary<int, InlineShape>();

        public void AssignIdToShape(int id, InlineShape shape)
        {
            _idAssignments.Add(id, shape);
        }

        public void AssignIdsToShapes(Dictionary<int, InlineShape> idShapes)
        {
            foreach (var idShape in idShapes)
            {
                foreach (var idWordShape in WordShapes)
                {
                    if (idWordShape.Shape.GetRange().Start == idShape.Value.Range.Start)
                    {
                        idWordShape.Id = idShape.Key;
                        idWordShape.Shape.SetIdAlternativeText(idShape.Key);
                    }
                }
            }
        }

        public IdShape<WordShape> GetIdShapeById(int searchId)
        {
            return WordShapes.Find(n => n.Id == searchId);
        }
    }
}