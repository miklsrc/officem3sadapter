using Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.IdManagement.ShapeProcessing
{
    public class WordShapeWrapper : WordShape
    {
        public Shape Shape { get; set; }

        public WordShapeWrapper(Shape shape)
        {
            Shape = shape;
        }

        public override Range GetRange()
        {
            return Shape.Anchor;
        }

        public override string AlternativeText
        {
            get { return Shape.AlternativeText ?? ""; }
            protected set { Shape.AlternativeText = value; }
        }

        public override void Select()
        {
            Shape.Select();
        }
    }
}