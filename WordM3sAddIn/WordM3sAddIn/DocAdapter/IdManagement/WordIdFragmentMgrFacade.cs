﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using BrightIdeasSoftware;
using Microsoft.Office.Interop.Word;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using WordM3sAddIn.DocAdapter.IdManagement.ShapeProcessing;
using WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.IdManagement
{

    //public class RangeComparer : IComparer<Range>
    //{

    //    //this method is REALLY SLOW... the Information access is somewhat slow
    //    public int Compare(Range r1, Range r2)
    //    {
    //        int r1Page=(int) r1.Information[WdInformation.wdActiveEndPageNumber];
    //        int r2Page = (int)r2.Information[WdInformation.wdActiveEndPageNumber];
    //        if (r1Page != r2Page)
    //        {
    //            return r1Page.CompareTo(r2Page);
    //        }

    //        int r1Vertical = (int)r1.Information[WdInformation.wdVerticalPositionRelativeToPage];
    //        int r2Vertical = (int)r2.Information[WdInformation.wdVerticalPositionRelativeToPage];

    //        return r1Vertical.CompareTo(r2Vertical);
    //    }
    //}

    public class RangeComparer2 : IComparer<KeyValuePair<int,float>>
    {

        public int Compare(KeyValuePair<int, float> r1, KeyValuePair<int, float> r2)
        {
            if (r1.Key != r2.Key)
            {
                return r1.Key.CompareTo(r2.Key);
            }

            return r1.Value.CompareTo(r2.Value);
        }
    }
    public class WordIdFragmentMgrFacade : IdFragmentMgrFacade
    {
        public WordTextRangeIdMgr WordTextRangeIdMgr { get; private set; }
        public WordShapeIdMgr WordShapeIdMgr { get; private set; }

        public WordIdFragmentMgrFacade(Microsoft.Office.Interop.Word.Document document)
        {
            WordTextRangeIdMgr = new WordTextRangeIdMgr(document, IdCounter);
            WordShapeIdMgr = new WordShapeIdMgr(document, IdCounter);

            AddIdFragmentMgr(WordTextRangeIdMgr);
            AddIdFragmentMgr(WordShapeIdMgr);
        }

        public Range GetRangeById(int searchId)
        {
            IdRange<Range> textRange = WordTextRangeIdMgr.GetTextRangeById(searchId);
            if (textRange != null)
            {
                return textRange.TextRange.Range;
            }

            IdShape<WordShape> shape = WordShapeIdMgr.GetIdShapeById(searchId);
            if (shape != null)
            {
               return shape.Shape.GetRange();
            }

            return null;
        }

        public void FetchSortedRealizations(out Dictionary<int, Realization> realizations, out Dictionary<int, int> headings)
        {

            // using System.Diagnostics;
            Stopwatch watch = new Stopwatch();
            watch.Start();




            realizations = new Dictionary<int, Realization>();
            headings = new Dictionary<int, int>();

            //collection is required becasue sorting texts and images the belonging range object is needed
            var fragmentRanges = new Dictionary<int, KeyValuePair<int,float>>();

            Window activeWindow = Globals.ThisAddIn.Application.ActiveWindow;
            foreach (var idTextRange in WordTextRangeIdMgr.WordDocText.TextRanges)
            {
                realizations.Add(idTextRange.Id, Utf8Realization.FromUtf8String(idTextRange.TextRange.Text));

                //TODO really slow
                int page = (int) idTextRange.TextRange.Range.Information[WdInformation.wdActiveEndPageNumber];
                float verticalPos = (float)idTextRange.TextRange.Range.Information[WdInformation.wdVerticalPositionRelativeToPage];
                fragmentRanges[idTextRange.Id] = new KeyValuePair<int, float>(page, verticalPos);

             
                int outlineLevel = WordHelper.GetOutlineLevel(idTextRange.TextRange.Range.Paragraphs.First);
                 if (outlineLevel > 0)
                 {
                     headings.Add(idTextRange.Id, outlineLevel);
                 }
            }

            foreach (IdShape<WordShape> idWordShape in WordShapeIdMgr.WordShapes)
            {
                string shapeImgBase64Data = "";
                ImageRealization m3SImageRealization = ImageRealization.FromBase64(shapeImgBase64Data);
//                string baseversionText = idWordShape.Shape.GetBaseversionAlternativeText();
//                int baseVersion;
//                if (int.TryParse(baseversionText, out baseVersion))
//                {
//                    m3SImageRealization.BaseVersion = baseVersion;
//                }

                realizations.Add(idWordShape.Id, m3SImageRealization);

                //TODO really slow
                int page = (int)idWordShape.Shape.GetRange().Information[WdInformation.wdActiveEndPageNumber];
                float verticalPos = (float)idWordShape.Shape.GetRange().Information[WdInformation.wdVerticalPositionRelativeToPage];
                fragmentRanges[idWordShape.Id] = new KeyValuePair<int, float>(page, verticalPos);

            }

            var rangeComparer = new RangeComparer2();

            //TODO really slow
            realizations = realizations.OrderBy(x => fragmentRanges[x.Key], rangeComparer).ToDictionary(pair => pair.Key, pair => pair.Value); ;

            

            watch.Stop();
            Debug.Print("Time spent: " + watch.Elapsed);

        }
    }



  
}
