﻿using System.Collections.Generic;
using System.Diagnostics;
using OfficeCommon.IdManagement.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing
{
    public class WordDocText : TextRangesContainer<Word.Range>
    {

        public WordDocText()
        {
        }

        public WordDocText(Word.Document document)
        {
            var inlineShapes = new Dictionary<int, Word.InlineShape>();
            foreach (Word.InlineShape shape in document.Content.InlineShapes)
            {
                inlineShapes.Add(shape.Range.Start, shape);
            }

            foreach (Word.Paragraph para in document.Paragraphs)
            {
                if (!inlineShapes.ContainsKey(para.Range.Start)) //only add paragraph when this is not a placeholder for a inline shape
                {
                    textRanges.Add(new IdRange<Word.Range>(new WordTextRange(para.Range)));
                }
            }
        }

    }
}
