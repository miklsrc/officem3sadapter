﻿using OfficeCommon.IdManagement.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing
{
    public class WordTextRange : AbstractTextRange<Word.Range>
    {
        private Word.Range range;

        public WordTextRange(Word.Range range)
        {
            this.range = range;
        }

        public override string Text
        {
            get
            {
                if (range.Text == null)
                {
                    return "";
                }
                return range.Text;
            }
            set
            {
                range.Text = value;
            }
        }

        public override int End
        {
            get => range.End; 
        }

        public override int Start
        {
            get => range.Start; 
        }

        public override int Length
        {
            get => range.End - range.Start; 
        }

        public override Word.Range Range
        {
            get => range;
            set => range = value;
        }
    }
}
