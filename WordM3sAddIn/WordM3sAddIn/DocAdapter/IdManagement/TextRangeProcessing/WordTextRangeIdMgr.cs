﻿using System;
using System.Collections.Generic;
using System.Linq;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.IdManagement.TextRangeProcessing
{

    public class WordTextRangeIdMgr : AbstractTextRangeIdMgr<Word.Range>
    {
        public WordDocText WordDocText { get; private set; }
        public Word.Document Document { get; private set; }

        public WordTextRangeIdMgr(Word.Document document, IdCounter idCounter)
                : base(idCounter)
        {
            this.Document = document;
            WordDocText = new WordDocText();
        }

        protected override void DoUpdateFragments()
        {
            WordDocText currentWordDocText = new WordDocText(Document);

            //repair old text range base
            base.DeleteEmptyRanges(WordDocText); //not needed, because empty ranges get merged in the combineMergedRanges step

            DeleteInlineShapePlaceholders(WordDocText); //delete when already existing ranges got converted to a inline shape placeholder

            removeNeedlessLineBreaks(WordDocText, currentWordDocText);

            var newAddedRangesPerBoxes = AddNewInsertedRanges(WordDocText, currentWordDocText);

            base.FixAtRageStartInsertedText(newAddedRangesPerBoxes, WordDocText);
            FixLineBreakAtRangeStart(newAddedRangesPerBoxes, WordDocText);

            base.combineMergedRanges(WordDocText, currentWordDocText);

            base.CutOffOverlappingRanges(WordDocText, currentWordDocText);

            base.CheckTextRangeLegality(WordDocText, currentWordDocText);
        }




        private void DeleteInlineShapePlaceholders(WordDocText wordDocText)
        {
            var inlineShapes = new Dictionary<int, Word.InlineShape>();
            foreach (Word.InlineShape shape in Document.Content.InlineShapes)
            {
                inlineShapes.Add(shape.Range.Start, shape);
            }


            for (int i = wordDocText.TextRanges.Count - 1; i >= 0; i--)
            {
                if (inlineShapes.ContainsKey(wordDocText.TextRanges[i].TextRange.Start)) //only add paragraph when this is not a placeholder for a inline shape
                {
                    FireIdFragmentRemovedEvent(wordDocText.TextRanges[i].Id);
                    wordDocText.TextRanges.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// when the user inserts a LineBreak (Enter) at the beginning of a line (OldRange), Word inserts this Line-Break at the beginning of the "old"-range,
        /// a newRange gets found without this LineBreak  [This is the complete opposite behaviour to no-LineBreak-characters,  see FixAtRageStartInsertedText] 
        /// (btw: it's different in PowerPoint. In PowerPoint the beginning LineBreak gets added at the end of the range before the "old"-Range)
        /// 
        /// example:    (before)         ->       ( after inserting the lineBreak)   (this example wrongly calculates the linebreak "\r" as 2 characters length)
        /// 
        ///          0---------1----              0---------1------
        ///          012345678901234              01234567890123456
        ///          This_is_a_range              \rThis_is_a_range
        ///          ^             ^              ^ ^             ^   
        ///          startOld=0    endOld=14      startOld=0   startNew=2   endOld=endNew=16
        /// 
        /// The problem is, the newRange gets a new ID and in the oldRange all text after the linebreak gets removed in CutOffOverlappingRanges.
        /// The result is: the oldRange contains only the empty line "\r" with the old ID and the newRange which contains the whole text gets a new ID.
        ///
        /// this method fixes this, be switching the IDs of oldRange and newRange
        /// </summary>
        protected void FixLineBreakAtRangeStart(List<IdRange<Word.Range>> newAddedTrs, TextRangesContainer<Word.Range> trContainerOld)
        {
            trContainerOld.TextRanges.Sort();
            foreach (IdRange<Word.Range> newTr in newAddedTrs)
            {

                int indexOfNewTr = trContainerOld.TextRanges.FindIndex(n => n.TextRange.Start == newTr.TextRange.Start && n.TextRange.End == newTr.TextRange.End);

                if (indexOfNewTr >= 1)
                {
                    IdRange<Word.Range> beforeTr = trContainerOld.TextRanges[indexOfNewTr - 1];

                    string beforeTrText = beforeTr.TextRange.Text;
                    string newTrText = newTr.TextRange.Text;

                    if (beforeTrText.Equals(Constants.LINE_BREAK + newTrText))
                    {
                        //switch IDs
                        int temp = beforeTr.Id;
                        beforeTr.Id = newTr.Id;
                        newTr.Id = temp;
                    }
                }

            }
        }

        protected override void PostProcessAssignForcedIds(IdManager idManager)
        {
            foreach (IdRangeDescription forcedIdDesc in idManager.GetForcedIdAssignments<IdRangeDescription>())
            {
                IdRange<Word.Range> idRange = WordDocText.GetIdRange(forcedIdDesc.Start, forcedIdDesc.End);
                idRange.Id = forcedIdDesc.Id;
            }
        }

        protected override IdFragmentDescription FindAndRemoveIdRangeAssignment(IdRange<Word.Range> curRange, IdManager idManager)
        {
            var forcedIdDescs = idManager.GetForcedIdAssignments<IdRangeDescription>(); ;
            IdRangeDescription idRangeDesc = forcedIdDescs.Find(n => n.Start == curRange.TextRange.Start && n.End == curRange.TextRange.End);
            idManager.RemoveForcedIdAssignment(idRangeDesc);
            return idRangeDesc;
        }

        public override HashSet<int> GetAllFragmentIds()
        {
            HashSet<int> fragmentIds = new HashSet<int>();

            foreach (var fragment in WordDocText.TextRanges)
            {
                if (!fragmentIds.Add(fragment.Id))
                {
                    throw new Exception("duplicate ids");
                }
            }

            return fragmentIds;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldTextBox"></param>
        /// <param name="curTextBox"></param>
        private void removeNeedlessLineBreaks(WordDocText oldTextBox, WordDocText curTextBox)
        {

            int indexOld = 0;
            int indexCur = 0;
            List<IdRange<Word.Range>> oldRanges = oldTextBox.TextRanges;
            List<IdRange<Word.Range>> curRanges = curTextBox.TextRanges;

            IdRange<Word.Range> oldRange;
            IdRange<Word.Range> curRange;

            while (indexOld < oldRanges.Count && indexCur < curRanges.Count)
            {
                oldRange = oldRanges[indexOld];
                curRange = curRanges[indexCur];

                //line breaks at the beginning of a range. example '\rNew Line'
                if ((curRange.TextRange.Start == oldRange.TextRange.Start + Constants.LINE_BREAK.Length
                    && curRange.TextRange.End == oldRange.TextRange.End
                    && oldRange.TextRange.Text.StartsWith(Constants.LINE_BREAK)
                    && oldRange.TextRange.Text.Length > Constants.LINE_BREAK.Length)
                ||  //line breaks at the end of a range. example 'New Line\r'
                  (curRange.TextRange.Start == oldRange.TextRange.Start
                    && curRange.TextRange.End == oldRange.TextRange.End + Constants.LINE_BREAK.Length
                    && curRange.TextRange.Text.EndsWith(Constants.LINE_BREAK)
                    && curRange.TextRange.Text.Length > Constants.LINE_BREAK.Length))
                {
                    oldRange.TextRange = curRange.TextRange;
                    indexOld++;
                    indexCur++;
                }
                else
                {
                    if (curRange.TextRange.Start > oldRange.TextRange.Start)
                    {
                        indexOld++;
                    }
                    else if (curRange.TextRange.Start < oldRange.TextRange.Start)
                    {
                        indexCur++;
                    }
                    else
                    {
                        indexOld++;
                        indexCur++;
                    }
                }
            }

        }

        public IdRange<Word.Range> GetIdRange(Word.Range range)
        {
            return WordDocText.GetIdRange(range.Start, range.End);
        }

        public override IdRange<Word.Range> GetTextRangeById(int searchId)
        {
            return this.WordDocText.GetIdRangeById(searchId);
        }



    }
}
