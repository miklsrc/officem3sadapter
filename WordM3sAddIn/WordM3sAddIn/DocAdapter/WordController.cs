﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OfficeCommon;
using OfficeCommon.IdManagement.TextRangeProcessing;
using OfficeCommon.m3s.structure;
using WordM3sAddIn.DocAdapter.IdManagement;
using Word = Microsoft.Office.Interop.Word;
using WordM3sAddIn.DocAdapter.Persistence;
using OfficeCommon.Gui;

namespace WordM3sAddIn.DocAdapter
{
    class WordController : Controller<Word.Document, WordIdFragmentMgrFacade>
    {

        private WordDocumentAdapter _docAdapter;

        public WordController(WordDocumentAdapter wordDocumentAdapter)
            : base(wordDocumentAdapter)
        {
            this._docAdapter = wordDocumentAdapter;
        }

       
    }
}
