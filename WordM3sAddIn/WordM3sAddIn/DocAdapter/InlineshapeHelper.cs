﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeCommon.IdManagement.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter
{
    class InlineshapeHelper
    {
        //it is faster to cache the ranges of the inlineshapes before as access the inlineshaper directly every time again (don't know why)
        private Dictionary<Word.Range, Word.InlineShape> inlineShapeRanges;

        public InlineshapeHelper(Word.Document document)
        {
            InitInlineShapes(document);
        }
        
        private void InitInlineShapes(Word.Document document)
        {
            inlineShapeRanges = new Dictionary<Word.Range, Word.InlineShape>();
            foreach (Word.InlineShape inlineShape in document.Content.InlineShapes)
            {
                inlineShapeRanges[inlineShape.Range] = inlineShape;
            }

        }

        public Word.InlineShape GetInlineShapeAtRange(AbstractTextRange<Word.Range> range)
        {
            foreach (Word.Range inlineShpaeRange in inlineShapeRanges.Keys)
            {
                if (range.Start <= inlineShpaeRange.Start && range.End >= inlineShpaeRange.End)
                {
                    return inlineShapeRanges[inlineShpaeRange];
                }
            }
            return null;
        }
    }
}
