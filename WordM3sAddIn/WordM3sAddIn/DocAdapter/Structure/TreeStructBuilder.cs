using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;
using OfficeCommon.IdManagement.TextRangeProcessing;

namespace WordM3sAddIn.DocAdapter.Structure
{
    public class TreeStructBuilder
    {
        private List<AbstractStructTreeNode> roots = new List<AbstractStructTreeNode>();
        private WordDocumentAdapter _docAdapter;
        private List<Table> tables;
        private List<Range> inlineShapeRanges;
        private TableNode LastAppearedTable;
        private Dictionary<int, AbstractStructTreeNode> lastLevels = new Dictionary<int, AbstractStructTreeNode>();

        public TreeStructBuilder(WordDocumentAdapter docAdapter)
        {
            _docAdapter = docAdapter;
        }

        private void addFragment(AbstractStructTreeNode structTreeNode, int level)
        {
            if (level == 0)
            {
                roots.Add(structTreeNode);
            }
            else
            {

                lastLevels[level].Children.Add(structTreeNode);
            }
        }


        public List<AbstractStructTreeNode> BuildNewTree()
        {
            LastAppearedTable = null;
            roots.Clear();
            lastLevels.Clear();


            //
            //this copy is needed to check references with == operator.
            //it looks like the document.tables and document.inlineShapes are recreated on every call.
            //this is the reason why i copy all stuff before in additional lists
            InitTables(_docAdapter.WordInstance);
            InitInlineShapes(_docAdapter.WordInstance);

            //
            foreach (IdRange<Range> idRange in _docAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr.WordDocText.TextRanges)
            {
                FragmentStructTreeNode structTreeNode = new FragmentStructTreeNode()
                {
                    LabelCol = idRange.TextRange.Text,
                    Id = idRange.Id,
                    InfoCol = "[" + idRange.TextRange.Start + "/" + idRange.TextRange.End + "]"
                };


                int headingLevel = WordHelper.GetOutlineLevel(idRange.TextRange.Range.Paragraphs.First);


                if (headingLevel >= 1)
                {
                    while (lastLevels.Any() && lastLevels.Last().Key >= headingLevel) lastLevels.Remove(lastLevels.Last().Key);
                }

                int level = lastLevels.Any()?lastLevels.Last().Key:0;

                if (headingLevel >= 1)
                {

                    while (headingLevel - level > 1)
                    {
                        EmptyHeadingStructTreeNode emptyHeadingStructTreeNode = new EmptyHeadingStructTreeNode();
                        addFragment(emptyHeadingStructTreeNode,level);
                        lastLevels[++level] = emptyHeadingStructTreeNode;
                    }
                    lastLevels[headingLevel] = structTreeNode;
                }
                addFragment(structTreeNode, level);

            }
            return roots;
        }



        private void AddParagraphInTable(IdRangeNode idRangeNode, Microsoft.Office.Interop.Word.Table table)
        {
            //if (LastAppearedTable == null || LastAppearedTable.Table != table)
            //{
            //    LastAppearedTable = new TableNode(table);
            //    LastAppearedHeading.children.Add(LastAppearedTable);
            //}
            //LastAppearedTable.children.Add(registerParagraphStructure(idRangeNode));
        }

        private void InsertHeading(HeadingNode headingNode, int outlineLevel)
        {
            //StructureNode currentLastNodeOnLevel = Root;

            //for (int curLevel = 1; curLevel < outlineLevel; curLevel++)
            //{
            //    if (currentLastNodeOnLevel.children.Count == 0 || !(currentLastNodeOnLevel.children.Last() is Heading))
            //    {
            //        currentLastNodeOnLevel.children.Add(new EmptyHeadingNode());
            //    }
            //    currentLastNodeOnLevel = currentLastNodeOnLevel.children.Last();
            //}

            //StructureNode newHeading = registerParagraphStructure(headingNode);
            //currentLastNodeOnLevel.children.Add(newHeading);
            //LastAppearedHeading = newHeading;
        }


        private void InitTables(Microsoft.Office.Interop.Word.Document document)
        {
            this.tables = new List<Table>();
            foreach (Microsoft.Office.Interop.Word.Table table in document.Tables)
            {
                tables.Add(table);
            }
        }

        private void InitInlineShapes(Microsoft.Office.Interop.Word.Document document)
        {
            this.inlineShapeRanges = new List<Range>();
            foreach (Microsoft.Office.Interop.Word.InlineShape inlineShape in document.Content.InlineShapes)
            {
                inlineShapeRanges.Add(inlineShape.Range);
            }

        }

        private bool IsaInlineShape(AbstractTextRange<Range> range)
        {

            foreach (Microsoft.Office.Interop.Word.Range inlineShpaeRange in inlineShapeRanges)
            {
                if (range.Start <= inlineShpaeRange.Start && range.End >= inlineShpaeRange.End)
                {
                    return true;
                }
            }
            return false;
        }

        private Microsoft.Office.Interop.Word.Table IsInTable(AbstractTextRange<Range> range)
        {
            //not used becasue Information access is really slow

            //bool inTable = (bool)idRange.Range.Information[Word.WdInformation.wdWithInTable];
            //if (!inTable)
            //{
            //    return null;
            //}


            foreach (Microsoft.Office.Interop.Word.Table table in tables)
            {
                if (range.Start >= table.Range.Start && range.End <= table.Range.End)
                {
                    return table;
                }
            }

            return null;
        }


    }

    public abstract class AbstractStructTreeNode
    {
        protected AbstractStructTreeNode()
        {

            InfoCol = "";
            LabelCol = "";
        }

        public virtual string IdCol { get { return ""; } }

        public string LabelCol { get; set; }

        public string InfoCol { get; set; }

        public List<AbstractStructTreeNode> Children
        {
            get { return children; }
        }

        public int ChildrenCount
        {
            get { return children.Count; }
        }

        protected List<AbstractStructTreeNode> children = new List<AbstractStructTreeNode>();

    }

    class EmptyHeadingStructTreeNode : AbstractStructTreeNode
    {

        public new string LabelCol { get { return "-----"; } }

        public new string InfoCol { get { return "missing heading"; } }

    }

    class FragmentStructTreeNode : AbstractStructTreeNode
    {



        public int Id { get; set; }

        public override string IdCol { get { return Id.ToString(); } }



    }
}