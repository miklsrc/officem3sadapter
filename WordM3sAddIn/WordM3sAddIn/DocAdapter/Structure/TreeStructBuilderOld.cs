﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using OfficeCommon.IdManagement.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Structure
{
  

    /// <summary>
    /// ////////////////////////////////////////////////////////
    /// </summary>

    public struct ParagraphStructureBind
    {
        public Word.Paragraph paragraph;
        public IdRangeNode structureNode;
    }

    public class TreeStructBuilderOld
    {

        private StructureNode LastAppearedHeading;
        private TableNode LastAppearedTable;

        private EmptyHeadingNode Root = new EmptyHeadingNode();

        private List<Word.Table> tables;
        private List<Word.Range> inlineShapeRanges;

        private List<ParagraphStructureBind> paragraphStructureMap;
        private WordDocumentAdapter _docAdapter;


        public TreeStructBuilderOld(WordDocumentAdapter docAdapter)
        {

            this._docAdapter = docAdapter;
        }

        private StructureNode registerParagraphStructure(IdRangeNode idRangeNode)
        {
            //ParagraphStructureBind paraStruct = new ParagraphStructureBind();
            //paraStruct.structureNode = idRangeNode;
            //paraStruct.idRangeNode = idRangeNode.idRange;
            //this.paragraphStructureMap.Add(paraStruct);
            return idRangeNode;

        }

        public List<StructureNode> GetTopLevelParagraphs()
        {
            return Root.children;
        }

        private void InitTables(Word.Document document)
        {
            this.tables = new List<Word.Table>();
            foreach (Word.Table table in document.Tables)
            {
                tables.Add(table);
            }
        }

        private void InitInlineShapes(Word.Document document)
        {
            this.inlineShapeRanges = new List<Word.Range>();
            foreach (Word.InlineShape inlineShape in document.Content.InlineShapes)
            {
                inlineShapeRanges.Add(inlineShape.Range);
            }

        }

        private bool IsaInlineShape(AbstractTextRange<Word.Range> range)
        {

            foreach (Word.Range inlineShpaeRange in inlineShapeRanges)
            {
                if (range.Start <= inlineShpaeRange.Start && range.End >= inlineShpaeRange.End)
                {
                    return true;
                }
            }
            return false;
        }

        private Word.Table IsInTable(AbstractTextRange<Word.Range> range)
        {
            //not used becasue Information access is really slow

            //bool inTable = (bool)idRange.Range.Information[Word.WdInformation.wdWithInTable];
            //if (!inTable)
            //{
            //    return null;
            //}


            foreach (Word.Table table in tables)
            {
                if (range.Start >= table.Range.Start && range.End <= table.Range.End)
                {
                    return table;
                }
            }

            return null;
        }

        public void Build()
        {
            LastAppearedTable = null;
            Root = new EmptyHeadingNode();
            paragraphStructureMap = new List<ParagraphStructureBind>();
            LastAppearedHeading = Root;


            //
            //this copy is needed to check references with == operator.
            //it looks like the document.tables and document.inlineShapes are recreated on every call.
            //this is the reason why i copy all stuff before in additional lists
            InitTables(_docAdapter.WordInstance);
            InitInlineShapes(_docAdapter.WordInstance);

            //
            foreach (IdRange<Word.Range> idRange in _docAdapter.WordIdFragmentMgrFacade.WordTextRangeIdMgr.WordDocText.TextRanges)
            {
                AddRange(idRange);
            }
        }


        private void AddRange(IdRange<Word.Range> idRange)
        {

            int outlineLevel = WordHelper.GetOutlineLevel(idRange.TextRange.Range.Paragraphs.First);

            if (outlineLevel > 0) //check if Heading
            {
                InsertHeading(new HeadingNode(idRange, outlineLevel), outlineLevel);
            }
            else // no Heading
            {
                Word.Table table = IsInTable(idRange.TextRange);
                if (table != null)
                {
                    AddParagraphInTable(new IdRangeNode(idRange, outlineLevel), table);
                }
                else
                    if (IsaInlineShape(idRange.TextRange))
                    {
                        LastAppearedHeading.children.Add(registerParagraphStructure(new ShapeNode(idRange, outlineLevel)));
                    }
                    else
                    {
                        LastAppearedHeading.children.Add(registerParagraphStructure(new IdRangeNode(idRange, outlineLevel)));
                    }
            }
        }


        private void AddParagraphInTable(IdRangeNode idRangeNode, Word.Table table)
        {
            if (LastAppearedTable == null || LastAppearedTable.Table != table)
            {
                LastAppearedTable = new TableNode(table);
                LastAppearedHeading.children.Add(LastAppearedTable);
            }
            LastAppearedTable.children.Add(registerParagraphStructure(idRangeNode));
        }

        private void InsertHeading(HeadingNode headingNode, int outlineLevel)
        {
            StructureNode currentLastNodeOnLevel = Root;

            for (int curLevel = 1; curLevel < outlineLevel; curLevel++)
            {
                if (currentLastNodeOnLevel.children.Count == 0 || !(currentLastNodeOnLevel.children.Last() is Heading))
                {
                    currentLastNodeOnLevel.children.Add(new EmptyHeadingNode());
                }
                currentLastNodeOnLevel = currentLastNodeOnLevel.children.Last();
            }

            StructureNode newHeading = registerParagraphStructure(headingNode);
            currentLastNodeOnLevel.children.Add(newHeading);
            LastAppearedHeading = newHeading;
        }

       
    }
}
