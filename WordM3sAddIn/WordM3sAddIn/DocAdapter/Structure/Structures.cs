﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OfficeCommon.IdManagement.TextRangeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter.Structure
{
    public interface Heading
    {

    }

    public abstract class StructureNode
    {
        public List<StructureNode> children { get; set; }

        public StructureNode()
        {
            children = new List<StructureNode>();
        }

        public virtual Font GetFont()
        {
            return new Font("Arial", 9, FontStyle.Regular);
        }


        public virtual Color GetColor()
        {
            return Color.Black;
        }

        public abstract Word.Range getRange();

        public abstract void delete();
    }

    public class IdRangeNode : StructureNode
    {
        public IdRange<Word.Range> IdRange { get; set; }
        public int OutlineLevel { get; set; }

        public IdRangeNode(IdRange<Word.Range> idRange, int outlineLevel)
        {
            this.IdRange = idRange;
            this.OutlineLevel = outlineLevel;
        }

        public override string ToString()
        {
            return IdRange.TextRange.Text + " [id:" + IdRange.Id + " - " + IdRange.TextRange.Start + "/" + IdRange.TextRange.End + "]";
        }
        public override void delete()
        {
            getRange().Delete();
        }

        public override Word.Range getRange()
        {
            return IdRange.TextRange.Range;
        }


    }

    public class HeadingNode : IdRangeNode, Heading
    {
        public HeadingNode(IdRange<Word.Range> idRange, int outlineLevel)
            : base(idRange, outlineLevel)
        {

        }

        public override Font GetFont()
        {
            return new Font("Arial", 9, FontStyle.Bold);
        }

        public override Word.Range getRange()
        {
            if (children.Any())
            {
                int start = base.getRange().Start;
                int end = children.Last().getRange().End;
                return Globals.ThisAddIn.Application.ActiveDocument.Range(start, end);
            }
            else
            {
                return base.getRange();
            }
        }
    }


    public class EmptyHeadingNode : StructureNode, Heading
    {

        public override string ToString()
        {
            return "---------";
        }

        public override Font GetFont()
        {
            return new Font("Arial", 9, FontStyle.Bold);
        }

        public override Word.Range getRange()
        {

            if (children.Any())
            {
                int start = children.First().getRange().End;
                int end = children.Last().getRange().End;
                return Globals.ThisAddIn.Application.ActiveDocument.Range(start, end);
            }
            else
            {
               throw new Exception("there should never be a emptyheading without children");

            }
        }

        public override void delete()
        {
            //nothing to do, it's empty :)
        }
    }


    public class TableNode : StructureNode
    {
        public Word.Table Table { get; private set; }

        public TableNode(Word.Table table)
        {
            this.Table = table;
        }

        public override string ToString()
        {
            return "TABLE";
        }

        public override Color GetColor()
        {
            return Color.Green;
        }

        public override void delete()
        {
            Table.Delete();
        }
        public override Word.Range getRange()
        {
            return Table.Range;
        }
    }

    public class ShapeNode : IdRangeNode
    {

        public ShapeNode(IdRange<Word.Range> idRange, int outlineLevel)
            : base(idRange, outlineLevel)
        {
        }

        public override string ToString()
        {
            return "SHAPE " + base.ToString();
        }

        public override Color GetColor()
        {
            return Color.Red;
        }
    }
}
