﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn.DocAdapter
{
    class WordDocInfoFetcher
    {
        private TreeView treeViewInfo;


        public WordDocInfoFetcher()
        {

        }

        public WordDocInfoFetcher(TreeView treeViewInfo)
        {
            this.treeViewInfo = treeViewInfo;
        }


        public delegate string ToTextDelegate<TypeEntry>(TypeEntry entry);

        public delegate object ToWdInformationDelegate<TypeEntry>(TypeEntry entry,Word.WdInformation information);

        public int GetEnumerableCount(IEnumerable Enumerable)
        {
            return (from object Item in Enumerable
                    select Item).Count();
        }

        private void AddNode<TypeEntry>(IEnumerable collection, string name, ToTextDelegate<TypeEntry> toText,ToWdInformationDelegate<TypeEntry> toWdInformation= null)
        {
            //
            if (GetEnumerableCount(collection) > 0)
            {
                TreeNode rootNode = new TreeNode(name);
                this.treeViewInfo.Nodes.Add(rootNode);
                foreach (TypeEntry entry in collection)
                {

                    TreeNode newChildNode = new TreeNode(toText(entry));
                    rootNode.Nodes.Add(newChildNode);
                    if (toWdInformation != null)
                    {
                        foreach (Word.WdInformation val in Enum.GetValues(typeof(Word.WdInformation)))
                        {
                            object wdInfoResult = toWdInformation(entry,val);
                            TreeNode newInfoNode = new TreeNode(val.ToString() + ":" + wdInfoResult.ToString());
                            newChildNode.Nodes.Add(newInfoNode);
                        }
                    }
                }
            }
        }



        public void refreshWordDocInfoTree()
        {
            Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;
            this.treeViewInfo.Nodes.Clear();


            ToTextDelegate<Word.Range> RangeToText = delegate(Word.Range range) { return range.Text.ToString(); };
            ToTextDelegate<Word.Paragraph> ParagraphToText = delegate(Word.Paragraph paragraph) { return RangeToText(paragraph.Range); };


            AddNode(document.XMLNodes, "XML Nodes", (Word.XMLNode xmlNode) => { return xmlNode.ToString(); });
            AddNode(document.Content.Paragraphs, "paragraphs", ParagraphToText);
            AddNode(document.Content.Tables, "tables", (Word.Table table) => { return RangeToText(table.Range); });
            AddNode(document.Content.Sentences, "Sentences", RangeToText);
            AddNode(document.Shapes, "shapes", (Word.Shape shape) => { return "shape"; }, (shape, info) => shape.Anchor.Information[info]);
            AddNode(document.Content.Sections, "Sections", (Word.Section section) => { return RangeToText(section.Range); });
            AddNode(document.Content.ListParagraphs, "ListParagraphs", ParagraphToText);
            AddNode(document.Content.InlineShapes, "InlineShapes", (Word.InlineShape shape) => { return "InlineShape: ";} ,(shape, info) => shape.Range.Information[info]);
            AddNode(document.Content.Fields, "Fields", (Word.Field field) => { return field.Code.Start.ToString() + " " + field.Code.End.ToString(); });
            AddNode(document.Content.Frames, "Frames", (Word.Frame frame) => { return RangeToText(frame.Range); });
            AddNode(document.Content.ShapeRange, "ShapeRanges", (Word.Shape shape) => { return shape.ToString(); });
            AddNode(document.TablesOfContents, "TOCs", (Word.TableOfContents field) => { return field.Range.Start.ToString() + " " + field.Range.End.ToString(); });


            if (document.Content.Paragraphs.Count > 0)
            {
                TreeNode rootNode = new TreeNode("paragraphs with attributes");
                this.treeViewInfo.Nodes.Add(rootNode);
                foreach (Word.Paragraph paragraph in document.Content.Paragraphs)
                {

                    TreeNode newChildNode = new TreeNode(ParagraphToText(paragraph));
                    rootNode.Nodes.Add(newChildNode);

                    //  WdInformation
                    TreeNode wdInfoNode = new TreeNode("WdInformation");
                    newChildNode.Nodes.Add(wdInfoNode);

                    foreach (Word.WdInformation val in Enum.GetValues(typeof(Word.WdInformation)))
                    {
                        object wdInfoResult = paragraph.Range.Information[val];
                        TreeNode newInfoNode = new TreeNode(val.ToString() + ":" + wdInfoResult.ToString());
                        wdInfoNode.Nodes.Add(newInfoNode);
                    }

                    // Style
                    Microsoft.Office.Interop.Word.Style style = paragraph.Range.get_Style() as Microsoft.Office.Interop.Word.Style;
                    if (style != null)
                    {

                        TreeNode styleNode = new TreeNode("Style: " + style.NameLocal);
                        newChildNode.Nodes.Add(styleNode);
                    }
                    //outline level
                    TreeNode outlineLevelNode = new TreeNode("Level: " + paragraph.OutlineLevel.ToString());
                    newChildNode.Nodes.Add(outlineLevelNode);

                    //ID
                    TreeNode idLevelNode = new TreeNode("ID: " + paragraph.ID);
                    newChildNode.Nodes.Add(idLevelNode);
                }
            }

        }
    }
}
