﻿using Microsoft.Office.Core;
using OfficeCommon;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;
using WordM3sAddIn.DocAdapter.IdManagement;
using System.Collections.Generic;
using WordM3sAddIn.DocAdapter.Gui;
using WordM3sAddIn.DocAdapter.Persistence;
using Word = Microsoft.Office.Interop.Word;
using WordM3sAddIn.DocAdapter.ContentBridge;
using CustomTaskPane = Microsoft.Office.Tools.CustomTaskPane;

namespace WordM3sAddIn.DocAdapter
{

    public class WordDocumentAdapter : InstanceAdapter
    {
        private WordSideBarAdapter _sideBar;
        public Word.Document WordInstance { get; }

        public WordIdFragmentMgrFacade WordIdFragmentMgrFacade { get; }

        public override AbstractIdFragmentMgr IdFragmentMgr => WordIdFragmentMgrFacade;

        public WordDocumentAdapter(Word.Document instance)
            : this(instance,new WordIdFragmentMgrFacade(instance), new FragmentPropertiesMngr())
        {
            WordInstance = instance;
        }

        private WordDocumentAdapter(Word.Document instance, WordIdFragmentMgrFacade wordIdFragmentMgrFacade, FragmentPropertiesMngr fragmentPropertiesMngr)
            : base( new WordDocumentPersistence(fragmentPropertiesMngr, wordIdFragmentMgrFacade), fragmentPropertiesMngr)
        {
            WordIdFragmentMgrFacade = wordIdFragmentMgrFacade;
            ContentBridgeInvoker.RegisterContentBridge(Utf8Realization.TYPE, new WordContentBridgeUtf8(this));
            ContentBridgeInvoker.RegisterContentBridge(ImageRealization.TYPE, new WordContentBridgeImage(this));

        }

        public override string GetFilePath()
        {
            return WordInstance.FullName;
        }

        public override DocumentProperties GetDocumentProperties()
        {
            return (Microsoft.Office.Core.DocumentProperties)WordInstance.BuiltInDocumentProperties;
        }


        protected override CustomTaskPane InitTaskPane()
        {
            WordController controller = new WordController(this);

            _sideBar = new WordSideBarAdapter(this);
            _sideBar.AddUserActions(controller.GetUserActions());

            return Globals.ThisAddIn.CustomTaskPanes.Add(_sideBar, "M3S Extension");
        }

        protected override void RefreshM3SDocsStructureBase()
        {
            IdFragmentMgr.RefreshFragments();
            WordIdFragmentMgrFacade.FetchSortedRealizations(out Dictionary<int, Realization> realizations, out Dictionary<int, int> headings);

            var groups = new Dictionary<int, int>();

            foreach (var fragment in realizations)
            {
                FragmentProperties fragmentProperties = FragmentPropertiesMngr.GetPropertiesFor(fragment.Key);
              
                int groupNr = fragmentProperties.GroupNr;
                if (groupNr > 0)
                {
                    groups.Add(fragment.Key, groupNr);
                }
            }

            M3SStructureBase.Refresh(realizations, headings, groups);
        }


    }
}
