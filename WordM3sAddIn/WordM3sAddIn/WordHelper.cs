﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using OfficeCommon;
using WordM3sAddIn.DocAdapter.IdManagement.ShapeProcessing;
using Word = Microsoft.Office.Interop.Word;

namespace WordM3sAddIn
{
    class WordHelper
    {

        public static void MoveToParagraph(Microsoft.Office.Interop.Word.Document d, Microsoft.Office.Interop.Word.Range r)
        {
            object dir = Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseStart;

            r.Collapse(ref dir);
            r.Select();
        }


        public static bool checkParagraphsIdState()
        {
            HashSet<string> paragraphIds = new HashSet<string>();

            Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;

            foreach (Word.Paragraph paragraph in document.Content.Paragraphs)
            {
                string id = paragraph.ID;

                //check if no id is set
                if (id == null || id.Length == 0)
                {
                    MessageBox.Show("no id found for idRange [" + paragraph.Range.Start + "|" + paragraph.Range.End + "] : " + paragraph.Range.Text.ToString());
                    return false;
                }

                //check for duplicates
                if (paragraphIds.Contains(id))
                {
                    MessageBox.Show("dubplicate id: " + id + " second found in idRange [" + paragraph.Range.Start + "|" + paragraph.Range.End + "] : " + paragraph.Range.Text.ToString());
                    return false;
                }
                else
                {
                    paragraphIds.Add(id);
                }
            }
            return true;
        }

        public static int GetOutlineLevel(Word.Paragraph paragraph)
        {
            switch (paragraph.OutlineLevel)
            {
                case Word.WdOutlineLevel.wdOutlineLevel1:
                    return 1;
                case Word.WdOutlineLevel.wdOutlineLevel2:
                    return 2;
                case Word.WdOutlineLevel.wdOutlineLevel3:
                    return 3;
                case Word.WdOutlineLevel.wdOutlineLevel4:
                    return 4;
                case Word.WdOutlineLevel.wdOutlineLevel5:
                    return 5;
                case Word.WdOutlineLevel.wdOutlineLevel6:
                    return 6;
                case Word.WdOutlineLevel.wdOutlineLevel7:
                    return 7;
                case Word.WdOutlineLevel.wdOutlineLevel8:
                    return 8;
                case Word.WdOutlineLevel.wdOutlineLevel9:
                    return 9;
                case Word.WdOutlineLevel.wdOutlineLevelBodyText:
                    return 0;
                default: throw new Exception("unkown OutlineLevel:" + paragraph.OutlineLevel.ToString());
            }
        }

        //very risky... clipboard access can throw exception on various situations
        public static string ConvertInlineShapeToJpgBase64(WordShape inlineShape, Word.Document wordDocument)
        {
            inlineShape.Select();
            wordDocument.Application.Selection.Copy();

            if (Clipboard.GetDataObject() != null)
            {
                System.Windows.Forms.IDataObject data = Clipboard.GetDataObject();
                if (data.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap))
                {
                    Image image = (Image)data.GetData(System.Windows.Forms.DataFormats.Bitmap, true);
                    return Helper.ImageToBase64(image, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                   throw new Exception("no image in clipboard");
                }
            }
            else
            {
                throw new Exception("The Clipboard was empty");
            }
        }


        public static void SetInlineshapeAlternativetext(Word.Range range,string alternativeText)
        {
            foreach (Word.InlineShape inlineShape in range.Document.InlineShapes)
            {
                if (range.Start <= inlineShape.Range.Start && range.End >= inlineShape.Range.End)
                {
                    inlineShape.AlternativeText = alternativeText;
                }
            }
        }


    }
}
