﻿using System.Collections.Generic;

namespace OfficeCommon
{
    public class Group
    {
        public int Id { get; set; }
        public Group Parent { get; set; }
        private List<Group> _childGroups = new List<Group>();

        public Group(int id)
        {
            Id = id;
        }

        public List<Group> ChildGroups => _childGroups;
    }


    public class GroupMgr
    {
        private static GroupMgr instance;

        //<group id, group>
        private Dictionary<int,Group> _allGroups = new Dictionary<int, Group>();

        private int idCounter = 0;

        private GroupMgr()
        {
        }

        public static GroupMgr Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GroupMgr();
                }
                return instance;
            }
        }

        public int CreateNewGroup(int parentGroupId=-1)
        {
            idCounter++;
            Group newGroup = new Group(idCounter);

            if (_allGroups.ContainsKey(parentGroupId))
            {
                newGroup.Parent = _allGroups[parentGroupId];
            }
            _allGroups[idCounter] = newGroup;

            return idCounter;
        }

        public int GetParentGroupId(int groupId)
        {
            if (_allGroups.ContainsKey(groupId))
            {
                Group parentGroup = _allGroups[groupId].Parent;
                if (parentGroup != null)
                {
                    return parentGroup.Id;
                }
            }
            return -1;
        }

        public void SetNewGroupParent(int groupId, int newParentGroupId)
        {
            Group group = _allGroups[groupId];
            Group oldParent = group.Parent;
            Group newParent = _allGroups[newParentGroupId];

            //remove from old parent group
            if (oldParent != null)
            {
                oldParent.ChildGroups.FindAndRemove(t => t.Id == groupId);
            }

            //set new parent
            group.Parent = newParent;

            //
            newParent.ChildGroups.Add(group);
        }

        //return a list of nested group ids
        public List<int> GetGroupHierarchy(int groupId)
        {
            List<int> groupHierarchy=new List<int>();
            Group currentGroup = _allGroups[groupId];
            do
            {
                groupHierarchy.Insert(0, currentGroup.Id);
                currentGroup = currentGroup.Parent;
            } while (currentGroup != null);

            return groupHierarchy;
        }

    }
}
