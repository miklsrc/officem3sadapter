﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace OfficeCommon
{
    public static class Helper
    {

        public static string EncodeUtf8ToBase64(string str)
        {
            return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(str));
        }

        public static string DecodeBase64ToUtf8(string str)
        {
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }

        public static bool ContainsLineBreak(string line)
        {
            return line.Contains(Constants.LINE_BREAK);
        }

        public static bool HasExactlyOneLineBreakAtEnd(string line)
        {
            if (!ContainsLineBreak(line))
            {
                return false;
            }

            return line.IndexOf(Constants.LINE_BREAK) == line.Length - Constants.LINE_BREAK.Length;

        }

        public static string RemoveTerminatingLineBreak(string line)
        {
            if (!ContainsLineBreak(line))
            {
                return line;
            }

            int firstLineBreakPos = line.IndexOf(Constants.LINE_BREAK);
            if (firstLineBreakPos == line.Length - Constants.LINE_BREAK.Length)
            {
                return line.Substring(0, firstLineBreakPos);
            }
            else
            {
                
                //throw new Exception("Line contains a line break which is not at the end.");
                //TODO in word in table cells is soem strange stuff liek "text\r\a", so line break is not the last. to avoid exception i use for test this solution

                return line.Substring(0, firstLineBreakPos);
            }
        }


        public static T FindAndRemove<T>(this List<T> lst, Predicate<T> match)
        {
            T ret = lst.Find(match);
            lst.RemoveAll(match);
            return ret;
        }

        public static string ImageToBase64(Image image,System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }


        public static Image Base64ToImage(string base64String)
        {
            if (base64String == null || base64String.Length == 0)
            {
                return null;
            }

            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static string CreateTempFolder(string docPath)
        {
            String containingFolder = Path.GetDirectoryName(docPath);
            Directory.CreateDirectory(containingFolder + "\\temp");
            return containingFolder + "\\temp";
        }

    }
}
