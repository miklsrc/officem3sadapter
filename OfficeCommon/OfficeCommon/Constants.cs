﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OfficeCommon
{
    public class Constants
    {
        public static readonly String LINE_BREAK = "\r";

        public static readonly String M3S_FILE_SUFFIX = "m3s.xml";

        public static readonly String M3S_METAINFO_FILE_SUFFIX = "paragraphid.xml";
    }
}
