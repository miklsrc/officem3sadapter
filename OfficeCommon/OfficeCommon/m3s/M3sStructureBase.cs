﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;

namespace OfficeCommon.m3s
{
    public class M3sStructureBase
    {
        private SessionInfo _sessionInfo;
        private Dictionary<int, M3SNode> lastLevels = new Dictionary<int, M3SNode>();
        private FragmentPropertiesMngr _fragmentPropertiesMngr;
        public List<M3SNode> M3SNodesLinear { get; private set; }
        public List<M3SNode> M3SStructureRoots { get; }
        public List<AbstractM3SNode> GuiTreeStructureRoots { get; }

        public M3sStructureBase(SessionInfo sessionInfo, FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            _sessionInfo = sessionInfo;
            M3SNodesLinear = new List<M3SNode>();
            M3SStructureRoots = new List<M3SNode>();
            GuiTreeStructureRoots = new List<AbstractM3SNode>();
            _fragmentPropertiesMngr = fragmentPropertiesMngr;
        }

        public void Init(List<M3SNode> m3SRootNodes)
        {
            M3SNodesLinear.Clear();
            AddTreeToLinearNodes(m3SRootNodes);
        }

        private void AddTreeToLinearNodes(List<M3SNode> m3SRootNodes)
        {
            foreach (var m3SNode in m3SRootNodes)
            {
                M3SNodesLinear.Add(m3SNode);
                AddTreeToLinearNodes(m3SNode.Children);
            }
        }

        public void Refresh(Dictionary<int, Realization> refreshedRealizations, Dictionary<int, int> headings, Dictionary<int, int> groups)
        {
            var newM3SNodesLinear = new List< M3SNode>();
            foreach (var idWithRefreshedReals in refreshedRealizations)
            {
                int id = idWithRefreshedReals.Key;
                Realization refreshedReal = idWithRefreshedReals.Value;
                M3SNode m3SNode = M3SNodesLinear.Find(a => a.Id == id);

                if (m3SNode == null)  //fragment does't already exist
                {
                    m3SNode = new M3SNode(_fragmentPropertiesMngr)
                    {
                        Id = id,
                    };
                }
                m3SNode.Realization=refreshedReal;
                newM3SNodesLinear.Add(m3SNode);
            }

            //add the removed refreshedRealizations
            int lastExistingM3SNodeIndex = -1;
            foreach ( M3SNode idWithOldM3SNode in M3SNodesLinear)
            {
                M3SNode m3SNode = idWithOldM3SNode;
                int id = m3SNode.Id;

                int index = newM3SNodesLinear.IndexOf(newM3SNodesLinear.Find(a => a.Id == id));
                if (index >= 0)
                {
                    lastExistingM3SNodeIndex = index;
                }
                if (!refreshedRealizations.ContainsKey(id))
                {
                    if (m3SNode.M3SNodeState != M3SNodeState.New) //if old reals was "added new", we can leave it out
                    {
                        m3SNode.Children.Clear();
                        m3SNode.Realization=null;
                        newM3SNodesLinear.Insert(lastExistingM3SNodeIndex+1, m3SNode);
                        lastExistingM3SNodeIndex++;
                    }
                }
            }
            M3SNodesLinear = newM3SNodesLinear;
            BuildTreeStruct(M3SNodesLinear, headings,groups);
        }


        private void BuildTreeStruct(List< M3SNode> m3SNodes, Dictionary<int, int> headings, Dictionary<int, int> groups)
        {

            M3SStructureRoots.Clear();
            GuiTreeStructureRoots.Clear();
            lastLevels.Clear();
            Dictionary<int, TreeGroupM3SNode> currentGroups = new Dictionary<int, TreeGroupM3SNode>();

            foreach (M3SNode structTreeM3SNode in m3SNodes)
            {
                int id = structTreeM3SNode.Id;

                structTreeM3SNode.Children.Clear();
                structTreeM3SNode.GuiTreeChildren.Clear();

                int headingLevel = headings.ContainsKey(id) ? headings[id] : -1;
                int groupNr = groups.ContainsKey(id) ? groups[id] : -1;

                //removes all nodes form the lastLevels-stack, with a higher headinglevel than the current element
                if (headingLevel >= 1)
                {
                    while (lastLevels.Any() && lastLevels.Last().Key >= headingLevel) lastLevels.Remove(lastLevels.Last().Key);
                }
                //set the current heading level depth
                int level = lastLevels.Any() ? lastLevels.Last().Key : 0;

                if (headingLevel >= 1)
                {

                    while (headingLevel - level > 1)
                    {
                        M3SNode emptyHeadingStructTreeM3SNode = new M3SNode(_fragmentPropertiesMngr)
                        {
                            Sync = true
                        };
                        AddGuiTreeNode(level, emptyHeadingStructTreeM3SNode);
                        AddM3SNode(level, emptyHeadingStructTreeM3SNode);
                        lastLevels[++level] = emptyHeadingStructTreeM3SNode;
                        //currentHeadingLevel = headingLevel;
                    }
                    lastLevels[headingLevel] = structTreeM3SNode;
                }


                if (groupNr >= 0)
                {
                    Dictionary<int, TreeGroupM3SNode> currentGroupsNew = new Dictionary<int, TreeGroupM3SNode>();

                    List<AbstractM3SNode> parentChildrenList;
                    bool parentM3SNodeIsSameGroup = false;
                    if (level == 0)
                    {
                        parentChildrenList = GuiTreeStructureRoots;
                    }
                    else
                    {
                        parentChildrenList = lastLevels[level].GuiTreeChildren;

                        int parentM3SNodeId = lastLevels[level].Id;
                        int parentGroupNr = groups.ContainsKey(parentM3SNodeId) ? groups[parentM3SNodeId] : -1;
                        parentM3SNodeIsSameGroup = parentGroupNr == groupNr;
                    }

                    if (!parentM3SNodeIsSameGroup) //when parent m3s node is already in the same group, we can directly add child nodes to it (because the parent already hanging on the TreeGroupM3SNode)
                    {
                        List<int> groupHierarchy = GroupMgr.Instance.GetGroupHierarchy(groupNr);
                        foreach (var groupId in groupHierarchy)
                        {
                            if (currentGroups.ContainsKey(groupId))
                            {
                                currentGroupsNew[groupId] = currentGroups[groupId];
                                parentChildrenList = currentGroups[groupId].GuiTreeChildren;
                            }
                            else
                            {
                                TreeGroupM3SNode groupTreeGroupM3STreeTreeGroupM3SNode = new TreeGroupM3SNode(groupId);
                                parentChildrenList.Add(groupTreeGroupM3STreeTreeGroupM3SNode);
                                currentGroupsNew[groupId] = groupTreeGroupM3STreeTreeGroupM3SNode;
                                parentChildrenList = groupTreeGroupM3STreeTreeGroupM3SNode.GuiTreeChildren;
                            }
                        }
                        currentGroups = currentGroupsNew;
                    }
                    parentChildrenList.Add(structTreeM3SNode );
                }
                else
                {
                    currentGroups.Clear();
                    AddGuiTreeNode(level, structTreeM3SNode);
                }
                AddM3SNode(level,structTreeM3SNode);
            }
        }

        private void AddM3SNode(int level,M3SNode node)
        {
            if (level == 0)
            {
                M3SStructureRoots.Add(node);
            }
            else
            {
                lastLevels[level].Children.Add(node);
            }
        }

        private void AddGuiTreeNode(int level, M3SNode node)
        {
            if (level == 0)
            {
                GuiTreeStructureRoots.Add(node);
            }
            else
            {
                lastLevels[level].GuiTreeChildren.Add(node);
            }
        }

    }
}
