﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using OfficeCommon.IdManagement;

namespace OfficeCommon.m3s.structure
{

    public abstract class AbstractM3SNode
    {
        public List<AbstractM3SNode> GuiTreeChildren { get; } = new List<AbstractM3SNode>();

        public abstract M3SNodeState M3SNodeState { get; }

        public abstract string Label { get; }

        public abstract bool Sync { get; set; }

        public abstract string IdString { get; }

        public abstract int BaseVersionString { get; }
    }

    public class TreeGroupM3SNode : AbstractM3SNode
    {

        public int GroupId { get; private set; }

        public TreeGroupM3SNode(int groupId)
        {
            GroupId = groupId;
        }

        public override M3SNodeState M3SNodeState
        {
            get => M3SNodeState.NOT_SET;

        }

        public override string Label => "Group: " + GroupId;
        public override bool Sync { get; set; }

        public override string IdString => "";

        public override int BaseVersionString => -1;
    }

    public class M3SNode : AbstractM3SNode
    {
        public override M3SNodeState M3SNodeState
        {
            get
            {
                if (IsRepoConnected == false)
                {
                    return M3SNodeState.New;
                }
                if (Realization == null)
                {
                    return M3SNodeState.Deleted;
                }
                if (SharedContent != null && SharedContent.Equals(Realization))
                {
                    return M3SNodeState.Unchanged;
                }
                return M3SNodeState.Changed;

            }
        }

        //current realization in instance
        public Realization Realization;
        //shared content from repo
        public Realization SharedContent;

        public bool IsRepoConnected = false;

        public List<M3SNode> Children { get; } = new List<M3SNode>();
        private readonly FragmentPropertiesMngr _fragmentPropertiesMngr;
        private M3SNodeState _m3SNodeState;

        public M3SNode(FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            _fragmentPropertiesMngr = fragmentPropertiesMngr;
        }

        public override string Label => Realization?.ToString() ?? SharedContent?.ToString() ?? "[[NOT SET]]";
        public override bool Sync
        {
            get
            {
                var sync = _fragmentPropertiesMngr.GetPropertiesFor(Id).Sync;
                return sync == 1;
            }
            set => _fragmentPropertiesMngr.GetPropertiesFor(Id).Sync = value ? 1 : 0;
        }

        public override int BaseVersionString
        {
            get
            {
                int version = _fragmentPropertiesMngr.GetPropertiesFor(Id).BaseVersion;
                return version;
            }
        }

        public override string IdString => Id.ToString();

        public int Id { get; set; }



    }

}
