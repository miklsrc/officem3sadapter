﻿using System.Linq;
using System.Text;
using OfficeCommon.IdManagement.TextRangeProcessing;

namespace OfficeCommon.m3s.structure
{
    public abstract class Realization
    {
        public string Data { get; set; }
        private readonly string type;

        public string Base64Data { get; set; }

        protected Realization(string type)
        {
            this.type = type;
            Base64Data = "";
        }

        public string Type => type;

        protected virtual bool Equals(Realization other)
        {
            //return string.Equals(data, other.data) && string.Equals(type, other.type);

            return string.Equals(type, other.type);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj is Realization == false) return false;

            var other = (Realization)obj;
            if (!type.Equals(other.type)) return false;

            return Equals(other);
        }

        public override string ToString()
        {
            return Data;
        }

   }

    public class Utf8Realization  : Realization
    {
        public const string TYPE = "utf8";


        private Utf8Realization()
            : base(TYPE)
        {
        }

        public static Utf8Realization FromBase64(string base64DataStr)
        {
            var m3SUtf8Realization = new Utf8Realization();
            m3SUtf8Realization.Base64Data = base64DataStr;
            m3SUtf8Realization.Data = Helper.DecodeBase64ToUtf8(base64DataStr);
            return m3SUtf8Realization;
        }

        public static Utf8Realization FromUtf8String(string utf8String)
        {
            var m3SUtf8Realization = new Utf8Realization();
            m3SUtf8Realization.Data = utf8String.Trim();
            return m3SUtf8Realization;
        }

        protected override bool Equals(Realization other)
        {
            string dataStr = Helper.RemoveTerminatingLineBreak(Data);
            string otherDataStr = Helper.RemoveTerminatingLineBreak(other.Data);
            return string.Equals(dataStr, otherDataStr) && string.Equals(Type, other.Type);
        }
    }

    public class ImageRealization : Realization
    {
        //TODO equals concept. at the moment this realization doesn contains "data".. this means the equals method only checking for type equaling


        public const string TYPE = "image";

        private ImageRealization()
            : base(TYPE)
        {

        }

        public static ImageRealization FromBase64(string base64DataStr)
        {
            var m3SImageRealization = new ImageRealization();
            m3SImageRealization.Base64Data = base64DataStr;
            return m3SImageRealization;
        }

        public override string ToString()
        {
            return "[[" + TYPE + "]]";
        }

    }


    public class EmptyRealization : Realization
    {
        public EmptyRealization(string data, string type)
            : base(type)
        {
            this.Data = data;
        }

        public override string ToString()
        {
            return "[[" + Type + "]]";
        }
    }
}
