﻿using System;
using System.Collections.Generic;
using OfficeCommon.Service_References.M3s;

namespace OfficeCommon.m3s.structure
{
    /// <summary>
    /// factory
    /// </summary>
    public class RealizationFactory
    {
        public static Realization Create(Sharedcontent real)
        {
            if (M3STypeCreators.ContainsKey(real.type))
            {
                Func<Sharedcontent, Realization> m3SCreationFunc= M3STypeCreators[real.type];
                return m3SCreationFunc(real);
            }
            return new EmptyRealization(real.data, real.type);
        }

        static RealizationFactory()
        {
            M3STypeCreators[ImageRealization.TYPE] = real => ImageRealization.FromBase64(real.data);
            M3STypeCreators[Utf8Realization.TYPE] = real => Utf8Realization.FromBase64(real.data);
        }

        public static Dictionary<String, Func<Sharedcontent, Realization>> M3STypeCreators = new Dictionary<string, Func<Sharedcontent, Realization>>();
    }
}