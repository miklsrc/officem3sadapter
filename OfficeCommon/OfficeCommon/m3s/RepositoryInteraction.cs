﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OfficeCommon.Gui;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;
using OfficeCommon.Service_References.M3s;


namespace OfficeCommon.m3s
{
    /**
     * updates/commit current document to/from the repo
     */
    public class RepositoryInteraction
    {
        public StructNode UpdateFromRepository(InstanceAdapter instanceAdapter)
        {
            StructNode structNode = null;
            M3sServiceProxy.Using(delegate(M3sWebServiceClient m3SService)
            {
                string m3SPath = M3SRepoPath(instanceAdapter);
                if (!File.Exists(m3SPath))
                {
                    ChooseFileDialog chooseM3SDialog = new ChooseFileDialog(m3SPath);
                    if (chooseM3SDialog.ShowDialog() == DialogResult.OK)
                    {
                        m3SPath = chooseM3SDialog.getFilePath();
                    }
                    else
                    {
                        return;
                    }
                }
                structNode = m3SService.update(m3SPath);
                instanceAdapter.SessionInfo.M3sFileLocation = m3SPath;
               
            });
            return structNode;
        }

        public void CommitToRepository(List<M3SNode> roots, InstanceAdapter instanceAdapter)
        {
            //            StructNode rootNode = instanceAdapter.Converter.ConvertM3SToRepoStruct(roots);
            //            M3sServiceProxy.Using(delegate (M3sWebServiceClient m3SService)
            //            {
            //                var m3SPath = M3SRepoPath(instanceAdapter);
            //                ChooseFileDialog chooseM3SDialog = new ChooseFileDialog(m3SPath);
            //                if (chooseM3SDialog.ShowDialog() == DialogResult.OK)
            //                {
            //                    m3SService.commit(chooseM3SDialog.getFilePath(), rootNode);
            //                    instanceAdapter.SessionInfo.M3sFileLocation = chooseM3SDialog.getFilePath();
            //                }
            //                chooseM3SDialog.Dispose();
            //            });


            StructNode rootNode = instanceAdapter.Converter.ConvertM3SToRepoStruct(roots);
            M3sServiceProxy.Using(delegate (M3sWebServiceClient m3SService)
            {
                string m3SPath = M3SRepoPath(instanceAdapter);
                if (!File.Exists(m3SPath))
                {
                    ChooseFileDialog chooseM3SDialog = new ChooseFileDialog(m3SPath);
                    if (chooseM3SDialog.ShowDialog() == DialogResult.OK)
                    {
                        m3SPath = chooseM3SDialog.getFilePath();
                    }
                    else
                    {
                        return;
                    }
                }
                m3SService.commit(m3SPath, rootNode);
                instanceAdapter.SessionInfo.M3sFileLocation = m3SPath;

            });
        }

        private string M3SRepoPath(InstanceAdapter instanceAdapter)
        {
            string m3SPath;
            if (string.IsNullOrEmpty(instanceAdapter.SessionInfo.M3sFileLocation)) //means initial checkout/checkin
            {
                m3SPath = instanceAdapter.GetFilePath() + "." + Constants.M3S_FILE_SUFFIX;
            }
            else
            {
                m3SPath = instanceAdapter.SessionInfo.M3sFileLocation;
            }
            return m3SPath;
        }
    }

}
