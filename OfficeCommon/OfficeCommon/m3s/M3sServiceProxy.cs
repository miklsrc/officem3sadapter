﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;
using OfficeCommon.Service_References.M3s;

namespace OfficeCommon.m3s
{
    public class M3sServiceProxy
    {
        public static void Using(Action<M3sWebServiceClient> action)
        {
            M3sWebServiceClient service = new M3sWebServiceClient();
            try
            {
                action(service);

            }
            catch (EndpointNotFoundException ex)
            {
                
                MessageBox.Show("can't connect to M3S Service. Is Service running? \n\n Error: ("+ ex.Message+")");
            }
            catch (Exception ex)
            {
                MessageBox.Show("unknown error during service connection");
                throw ex;
            }


          
        }
    }
}
