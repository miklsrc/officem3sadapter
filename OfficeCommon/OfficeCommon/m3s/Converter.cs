﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;
using OfficeCommon.Service_References.M3s;

namespace OfficeCommon.m3s
{
    public class Converter
    {

        private readonly ContentBridgeInvoker _contentBridgeInvoker;
        private readonly FragmentPropertiesMngr _fragmentPropertiesMngr;

        public Converter(ContentBridgeInvoker contentBridgeInvoker, IdManagement.FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            _contentBridgeInvoker = contentBridgeInvoker;
            _fragmentPropertiesMngr = fragmentPropertiesMngr;
        }

        public StructNode ConvertM3SToRepoStruct(List<M3SNode> rootM3SNodes)
        {
            StructNode m3SRoot = new StructNode();
            m3SRoot.description = "container all content";
            AddRepoStructNodeChildren(m3SRoot, rootM3SNodes);

            return m3SRoot;
        }

        private void AddRepoStructNodeChildren(StructNode m3SCommitNode, List<M3SNode> m3SStructNodesToAdd)
        {
            m3SCommitNode.children = new StructNode[m3SStructNodesToAdd.Count];

            for (int i = 0; i < m3SStructNodesToAdd.Count; i++)
            {
                StructNode structChildNode = new StructNode();
                m3SCommitNode.children[i] = structChildNode;

                M3SNode m3SNode = m3SStructNodesToAdd[i];
                Realization realization = m3SNode.Realization;

                structChildNode.id = m3SNode.Id;

                Sharedcontent commitReal = null;
                if (realization != null && m3SNode.Sync)
                {
                    var data = _contentBridgeInvoker.ToBase64(realization.Type, m3SNode.Id);

                    if (data.Length > 0)
                    {
                        commitReal = new Sharedcontent()
                        {
                            type = realization.Type,
                            data = data,
                        };
                    }
                }

                structChildNode.sharedcontent = commitReal;

                AddRepoStructNodeChildren(structChildNode, m3SNode.Children);
            }
        }


        public List<M3SNode> ConvertRepoTreeToM3STree(StructNode updateStructNode)
        {
            var m3SNodes = new List<M3SNode>();
            if (updateStructNode.children != null)
            {
                foreach (var updateNodeChild in updateStructNode.children)
                {
                    var m3SNode = new M3SNode(_fragmentPropertiesMngr)
                    {
                        Id = updateNodeChild.id,
                        IsRepoConnected = true
                    };

                    Sharedcontent sharedContentRepo = updateNodeChild.sharedcontent;
                    if (sharedContentRepo != null && sharedContentRepo.data.Length>0)
                    {
                        Realization sharedContentInstance = RealizationFactory.Create(sharedContentRepo);
                        m3SNode.SharedContent = sharedContentInstance;
                        _fragmentPropertiesMngr.GetPropertiesFor(updateNodeChild.id).BaseVersion = sharedContentRepo.version;
                    }

                    m3SNodes.Add(m3SNode);
                    m3SNode.Children.AddRange(ConvertRepoTreeToM3STree(updateNodeChild));
                }
            }
            return m3SNodes;
        }
    }
}
