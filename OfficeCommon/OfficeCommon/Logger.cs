﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace OfficeCommon
{
    public enum LogLevel
    {
        Info ,
        Trace ,
        Warning,
        Error ,
    }

    public class Logger
    {
        public static void Log(string msg, LogLevel level)
        {
            Debug.Print("Log ["+level.ToString("G")+"]: " +msg);
        }
    }
}
