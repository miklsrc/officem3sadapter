﻿using System;
using System.Collections.Generic;
using System.IO;
using OfficeCommon.IdManagement;

namespace OfficeCommon
{
    public abstract class InstanceManager<TInstance>
    {
        protected Dictionary<TInstance, InstanceAdapter> Adapters = new Dictionary<TInstance, InstanceAdapter>();

        protected abstract InstanceAdapter CreateConcreteInstanceAdapter(TInstance instance);

        protected abstract string GetNameForNewInstance();

        public abstract string GetFilePath(TInstance presentation);

        public abstract string GetFolderPath(TInstance presentation);

        public void InstanceSavedEvent(TInstance instance)
        {
            var filePath = GetFilePath(instance);
            if (Adapters.ContainsKey(instance) && File.Exists(filePath))
            {

                String xmlFilePath = filePath + "."+Constants.M3S_METAINFO_FILE_SUFFIX;
                Adapters[instance].SaveM3SMetadata(xmlFilePath);
            }
        }

        public void InstanceOpenedEvent(TInstance instance)
        {
            String xmlFilePath = GetFilePath(instance) + "." + Constants.M3S_METAINFO_FILE_SUFFIX;
            if (File.Exists(xmlFilePath)) //test if this file is under paragraph control
            {
                var instanceAdapter = CreateConcreteInstanceAdapter(instance);
                Adapters.Add(instance, instanceAdapter);
                instanceAdapter.InitFromXml(xmlFilePath);
                instanceAdapter.ShowTaskPane();
            }
        }


        public InstanceAdapter ActivateDocumentAdapter(TInstance instance)
        {
            InstanceAdapter instanceAdapter;

            if (Adapters.ContainsKey(instance))
            {
                instanceAdapter = Adapters[instance];
            }
            else
            {
                instanceAdapter = CreateConcreteInstanceAdapter(instance);
                instanceAdapter.Init();
                Adapters.Add(instance, instanceAdapter);
            }
            instanceAdapter.ShowTaskPane();

            return instanceAdapter;
        }

    }
}
