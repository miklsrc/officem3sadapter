﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;

namespace OfficeCommon
{
    public interface IContentBridge
    {
        string ToBase64(int id);
        void Replace(int id, Realization real);

        /// <summary>
        /// inserts a new fragment
        /// </summary>
        /// <param name="previousFragmentIds">
        /// predecessorFragmentId==-1 -> (1) new fragment inserted at the beginning of the document
        /// predecessorFragmentId>= -> (2) new fragment get inserted after fragment with id==predecessorFragmentId
        /// when no predecessorFragment can be found -> (3) new fragment get inserted at end of the document
        /// </param>
        /// <param name="insertFragmentId"></param>
        /// <param name="real"></param>
        /// <param name="level">heading level (0 -> default text)</param>
        void Insert(List<int> previousFragmentIds, int insertFragmentId, Realization real, int level, List<IdRangeDescription> idRangeDescs);
    }

    public class ContentBridgeInvoker
    {

        public string ToBase64(string type, int id)
        {
            if (_contentBridges.ContainsKey(type))
            {
                return _contentBridges[type].ToBase64(id);
            }

            return "";
        }

        public void Replace(int id, Realization real)
        {
            if (_contentBridges.ContainsKey(real.Type))
            {
                _contentBridges[real.Type].Replace(id, real);
            }
        }

        public void Insert(List<int> previousFragmentIds, int insertFragmentId, Realization real, int level, List<IdRangeDescription> idRangeDescs)
        {
            if (_contentBridges.ContainsKey(real.Type))
            {
                _contentBridges[real.Type].Insert(previousFragmentIds, insertFragmentId, real, level,idRangeDescs);
            }
        }

        public Boolean IsTypeSupported(string type)
        {
            return _contentBridges.ContainsKey(type);
        }

        public void RegisterContentBridge(string type,IContentBridge contentBridge )
        {
            _contentBridges.Add(type,contentBridge);
        }

        private readonly Dictionary<String,IContentBridge>  _contentBridges= new Dictionary<string, IContentBridge>();

    }
}
