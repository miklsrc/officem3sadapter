using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Core;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s;
using OfficeCommon.m3s.structure;
using OfficeCommon.Service_References.M3s;
using CustomTaskPane = Microsoft.Office.Tools.CustomTaskPane;

namespace OfficeCommon
{
    /*
     * adapter gets assigned to a single document instance and enables m3s functionality for it
     */
    public abstract class InstanceAdapter
    {
        private InstanceXmlPersistence _persistence;
        public abstract AbstractIdFragmentMgr IdFragmentMgr { get; }

        private CustomTaskPane _myCustomTaskPane;
        public SessionInfo SessionInfo { get; private set; }
        public M3sStructureBase M3SStructureBase { get; }
        public FragmentPropertiesMngr FragmentPropertiesMngr { get; protected set; }
        public ContentBridgeInvoker ContentBridgeInvoker { get; } = new ContentBridgeInvoker();
        public Converter Converter { get; }

        public InstanceAdapter(InstanceXmlPersistence persistence, FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            _persistence = persistence;
            FragmentPropertiesMngr = fragmentPropertiesMngr;
            Converter = new Converter(ContentBridgeInvoker, FragmentPropertiesMngr);
            //
            SessionInfo = new SessionInfo();
            //
            M3SStructureBase = new M3sStructureBase(SessionInfo, FragmentPropertiesMngr);
        }

        public List<M3SNode> M3SDocsStructureBaseRoots
        {
            get
            {
                RefreshM3SDocsStructureBase();
                return M3SStructureBase.M3SStructureRoots;
            }
        }

        public List<AbstractM3SNode> GuiTreeStructureBaseRoots
        {
            get
            {
                RefreshM3SDocsStructureBase();
                return M3SStructureBase.GuiTreeStructureRoots;
            }
        }

        public void InitFromXml(string paragraphIdFile)
        {
            if (File.Exists(paragraphIdFile)) //test if this file is under m3s control
            {
                _persistence.LoadSessionInfo(SessionInfo, paragraphIdFile);
                IdFragmentMgr.RefreshFragments();
                _persistence.LoadInstanceM3SMetadata(IdFragmentMgr, paragraphIdFile);
                RefreshM3SDocsStructureBase();
                this._myCustomTaskPane = InitTaskPane();
            }
        }

        public void Init()
        {
            this._myCustomTaskPane = InitTaskPane();
        }

        public void SaveM3SMetadata(String xmlFilePath)
        {
            IdFragmentMgr.RefreshFragments();
            RefreshM3SDocsStructureBase();

            _persistence.SaveInstanceM3SMetadata(IdFragmentMgr, M3SStructureBase.M3SNodesLinear, SessionInfo, xmlFilePath);

            Logger.Log("m3s metadata saved: " + xmlFilePath, LogLevel.Info);
        }

        public void UpdateFromM3S(StructNode repoStructNode)
        {
            List<M3SNode> m3SRootNodes = Converter.ConvertRepoTreeToM3STree(repoStructNode);
            int level = 1;
            var idRangeDescs=new List<IdRangeDescription>();
            ApplyM3SContentToInstance(m3SRootNodes, level,new List<int>(), idRangeDescs);
            M3SStructureBase.Init(m3SRootNodes);
            RefreshM3SDocsStructureBase();
        }

        protected abstract CustomTaskPane InitTaskPane();

        protected abstract void RefreshM3SDocsStructureBase();

        /*
         * this updates the content inside of the current document instance.
         */
        protected void ApplyM3SContentToInstance(List<M3SNode> updateStructnodes, int level,
            List<int> previousFragmentIds, List<IdRangeDescription> idRangeDescs)
        {
            foreach (M3SNode node in updateStructnodes)
            {
                if (node.Sync && node.SharedContent != null) //TODO SharedContent == null means deletion
                {
                    if (IdFragmentMgr.GetAllFragmentIds().Contains(node.Id))
                    {
                        ContentBridgeInvoker.Replace(node.Id, node.SharedContent);
                    }
                    else
                    {
                        int heading = -1;
                        if (node.Children != null && node.Children.Any())
                        {
                            heading = level;
                        }
                        ContentBridgeInvoker.Insert(previousFragmentIds, node.Id, node.SharedContent, heading, idRangeDescs);
                    }
                }
                previousFragmentIds.Add(node.Id);
                ApplyM3SContentToInstance(node.Children, level + 1, previousFragmentIds, idRangeDescs);
            }
        }

        public abstract string GetFilePath();

        public abstract Microsoft.Office.Core.DocumentProperties GetDocumentProperties();

        public void ShowTaskPane()
        {
            _myCustomTaskPane.Visible = true;
        }

    }
}