﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrightIdeasSoftware;

namespace OfficeCommon.Gui
{
    public class DragNDropHelper
    {
        public static bool IsDropOntoItself(ModelDropEventArgs e)
        {
            if (e.DropTargetLocation == DropTargetLocation.Item)
            {
                OLVListItem targetItem = ((TreeListView)e.ListView).GetItem(e.DropTargetIndex);
                return e.SourceModels.Contains(targetItem.RowObject);
            }
            return false;
        }

        public static bool IsDropAfterItself(ModelDropEventArgs e)
        {
            return IsDropNearItself(e, true);
        }
        public static bool IsDropBeforeItself(ModelDropEventArgs e)
        {
            return IsDropNearItself(e, false);
        }

        private static bool IsDropNearItself(ModelDropEventArgs e, bool after)
        {
            OLVListItem targetItem = ((TreeListView)e.ListView).GetItem(e.DropTargetIndex);

            if (e.DropTargetLocation == (after ? DropTargetLocation.AboveItem : DropTargetLocation.BelowItem))
            {
                int targetNeighborIndex = e.DropTargetIndex + (after ? -1 : +1);
                if (0 <= targetNeighborIndex && e.ListView.GetItemCount() > targetNeighborIndex)
                {
                    OLVListItem nextToTargetItem = e.ListView.GetItem(targetNeighborIndex);
                    if (nextToTargetItem.IndentCount == targetItem.IndentCount)
                    {
                        return e.SourceModels.Contains(nextToTargetItem.RowObject);
                    }
                }

            }
            if (e.DropTargetLocation == (after ? DropTargetLocation.BelowItem : DropTargetLocation.AboveItem))
            {
                return e.SourceModels.Contains(targetItem.RowObject);
            }
            return false;
        }
    }
}
