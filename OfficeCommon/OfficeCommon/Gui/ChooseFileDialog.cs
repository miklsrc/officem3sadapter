﻿using System;
using System.Windows.Forms;

namespace OfficeCommon.Gui
{
    public partial class ChooseFileDialog : Form
    {
        public ChooseFileDialog() : this("")
        {
        }

        public ChooseFileDialog(String defaultPath)
        {
            InitializeComponent();
            tfPath.Text = defaultPath;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofDlg = new OpenFileDialog();
            if (DialogResult.OK == ofDlg.ShowDialog())
            {
                string filePath = ofDlg.FileName;
                tfPath.Text = filePath;
            }
        }

        public string getFilePath()
        {
            return tfPath.Text;
        }


    }
}
