using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Forms;
using BrightIdeasSoftware;
using OfficeCommon.Gui.RightClickInteraction;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;

namespace OfficeCommon.Gui
{
    public partial class M3sOutline : UserControl
    {

        public BrightIdeasSoftware.TreeListView TreeList => this.treeListView1;
        public RightClickInteractionManager RightClickInteractionManager { get; } = new RightClickInteractionManager();

        public M3sOutline()
        {

            InitializeComponent();

            treeListView1.CanExpandGetter = delegate(object x) { return ((AbstractM3SNode)x).GuiTreeChildren.Count > 0; };
            treeListView1.ChildrenGetter = delegate(object x) { return ((AbstractM3SNode)x).GuiTreeChildren; };

            ColState.Renderer = new MappedImageRenderer(new Object[] { M3SNodeState.New, Properties.Resources.add, M3SNodeState.Unchanged, Properties.Resources.check, M3SNodeState.Changed, Properties.Resources.update, M3SNodeState.Deleted, Properties.Resources.delete });

            treeListView1.CellRightClick += RightClickInteractionManager.OnTreeRightClick;



            treeListView1.BooleanCheckStateGetter = delegate (Object rowObject) {
                return ((AbstractM3SNode)rowObject).Sync;
            };

            treeListView1.BooleanCheckStatePutter = delegate (Object rowObject, bool newValue) {
                ((AbstractM3SNode)rowObject).Sync = newValue;
                return newValue; // return the value that you want the control to use
            };




        }

    }
}
