using System.Collections.Generic;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace OfficeCommon.Gui.RightClickInteraction
{
    public class RightClickInteractionManager
    {
        private List<IRightClickInteraction> rightClickInteractions = new List<IRightClickInteraction>();

        internal void OnTreeRightClick(object sender, CellRightClickEventArgs e)
        {
            ContextMenu contextMenu = new ContextMenu();

            foreach (var p in rightClickInteractions)
            {
                p.OnRightClicked(sender, e, contextMenu);
            }

            if (contextMenu.MenuItems.Count > 0)
            {
                contextMenu.Show(e.ListView, e.Location);
            }
        }

        public void registerInteraction(IRightClickInteraction interaction)
        {

            rightClickInteractions.Add(interaction);
        }
        public void unregisterInteraction(IRightClickInteraction interaction)
        {
            rightClickInteractions.Remove(interaction);
        }

    }
}