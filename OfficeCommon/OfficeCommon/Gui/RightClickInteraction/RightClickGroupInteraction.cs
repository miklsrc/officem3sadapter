using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;

namespace OfficeCommon.Gui.RightClickInteraction
{
    public class RightClickGroupInteraction : IRightClickInteraction
    {
        private IList _selectedObjects;
        private FragmentPropertiesMngr _fragmentPropertiesMngr;

        public RightClickGroupInteraction(FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            _fragmentPropertiesMngr = fragmentPropertiesMngr;
        }
        public void OnRightClicked(object sender, CellRightClickEventArgs e, ContextMenu contextMenu)
        {
            if (sender is TreeListView)
            {
                TreeListView treeView = (TreeListView)sender;
                _selectedObjects = treeView.SelectedObjects;

                if (_selectedObjects.Count > 1 && allSelectedWithourGap(treeView))
                {
                    contextMenu.MenuItems.Add(new MenuItem("Create Group", CMCreateGroup));
                }
                if (_selectedObjects.Count == 1 && _selectedObjects[0] is TreeGroupM3SNode && !hasGuiGroupChildren((TreeGroupM3SNode)_selectedObjects[0]))
                {

                    contextMenu.MenuItems.Add(new MenuItem("Remove Group", CMRemoveGroup));
                }

            }
        }

        private bool allSelectedWithourGap(TreeListView treeView)
        {
            ListView.SelectedIndexCollection selIndices = treeView.SelectedIndices;
            int high = selIndices[selIndices.Count - 1];
            int low = selIndices[0];
            return selIndices.Count -1==high  - low;

        }

        private bool hasGuiGroupChildren(TreeGroupM3SNode treeGroupM3SNode)
        {
            return treeGroupM3SNode.GuiTreeChildren.OfType<TreeGroupM3SNode>().Any();
        }

        private void CMCreateGroup(object sender, EventArgs e)
        {
            int oldGroupId;
            if (_selectedObjects[0] is M3SNode)
            {
                oldGroupId = _fragmentPropertiesMngr.GetPropertiesFor(((M3SNode)_selectedObjects[0]).Id).GroupNr;
            } else if (_selectedObjects[0] is TreeGroupM3SNode)
            {
                oldGroupId = GroupMgr.Instance.GetParentGroupId( ((TreeGroupM3SNode) _selectedObjects[0]).GroupId);
            }
            else
            {
                throw new Exception("should never happen, unkown node type");
            }

            List<int> visitedGroupIds = new List<int>();

            int newGroupId = GroupMgr.Instance.CreateNewGroup(oldGroupId);//currentGroupId become parent of new goup
            foreach (var selectedObject in _selectedObjects)
            {
                if (selectedObject is M3SNode)
                {
                    M3SNode m3SNode = (M3SNode) selectedObject;
                    if (!visitedGroupIds.Contains(_fragmentPropertiesMngr.GetPropertiesFor(m3SNode.Id).GroupNr)) //only change m3snodes which are NOT in a selected group
                    {
                        _fragmentPropertiesMngr.GetPropertiesFor(m3SNode.Id).GroupNr = newGroupId;
                    }
                }
                if (selectedObject is TreeGroupM3SNode)
                {

                   TreeGroupM3SNode treeGroupM3SNode = (TreeGroupM3SNode)selectedObject;
                   int parentGroupId = GroupMgr.Instance.GetParentGroupId(treeGroupM3SNode.GroupId);

                    if (!visitedGroupIds.Contains(parentGroupId))
                    {
                        GroupMgr.Instance.SetNewGroupParent(treeGroupM3SNode.GroupId, newGroupId);
                        visitedGroupIds.Add(treeGroupM3SNode.GroupId);
                    }
                }

            }
        }
        private void CMRemoveGroup(object sender, EventArgs e)
        {
            TreeGroupM3SNode treeGroupM3SNode = (TreeGroupM3SNode)_selectedObjects[0];
            int parentGroupId=GroupMgr.Instance.GetParentGroupId(treeGroupM3SNode.GroupId);

            foreach (var guiTreeGroupNodeChild in treeGroupM3SNode.GuiTreeChildren)
            {
                if (guiTreeGroupNodeChild is M3SNode)
                {
                    M3SNode m3SNode = (M3SNode) guiTreeGroupNodeChild;
                    _fragmentPropertiesMngr.GetPropertiesFor(m3SNode.Id).GroupNr = parentGroupId;
                }
            }

        }


    }
}