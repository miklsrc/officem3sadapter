using System.Windows.Forms;
using BrightIdeasSoftware;

namespace OfficeCommon.Gui.RightClickInteraction
{
    public interface IRightClickInteraction
    {
        void OnRightClicked(object sender, CellRightClickEventArgs e, ContextMenu contextMenu);
    }
}