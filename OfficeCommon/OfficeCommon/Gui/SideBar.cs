﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using BrightIdeasSoftware;
using OfficeCommon.IdManagement;
using OfficeCommon.IdManagement.TextRangeProcessing;
using Forms = System.Windows.Forms;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using OfficeCommon;
using Microsoft.Office.Core;
using System.Windows.Forms;
using System.Diagnostics;

namespace OfficeCommon.Gui
{

    public abstract partial class SideBar<TInstance, TIdFragmentMgr> : UserControl where TIdFragmentMgr : AbstractIdFragmentMgr
    {

        private ContextMenu _contextMenu = new ContextMenu();
        protected M3sOutline M3SOutline = new M3sOutline();
        protected InstanceAdapter InstanceAdapter;

        public SideBar(InstanceAdapter instanceAdapter)
        {
            InstanceAdapter = instanceAdapter;

            InitializeComponent();

            M3SOutline.TreeList.MouseEnter += ActionRefreshM3SOutline;

            //drag n drop
            M3SOutline.TreeList.ModelCanDrop += HandleModelCanDrop;
            M3SOutline.TreeList.ModelDropped += HandleModelDropped;
            M3SOutline.TreeList.IsSimpleDragSource = true;
            M3SOutline.TreeList.IsSimpleDropSink = true;

            SimpleDropSink sink2 = (SimpleDropSink)M3SOutline.TreeList.DropSink;
            sink2.AcceptExternal = false;
            sink2.CanDropBetween = true;
            sink2.CanDropOnBackground = false;

            AddTab(M3SOutline, "M3sOutline");
        }

        protected virtual void ActionRefreshM3SOutline(object sender, EventArgs e)
        {
            M3SOutline.TreeList.SetObjects(InstanceAdapter.GuiTreeStructureBaseRoots);
        }

        protected abstract void HandleModelDropped(object sender, ModelDropEventArgs e);

        protected abstract void HandleModelCanDrop(object sender, ModelDropEventArgs e);

        public void AddTab(System.Windows.Forms.Control control, String title)
        {
            System.Windows.Forms.TabPage newTab = new System.Windows.Forms.TabPage();

            this.tabControl1.Controls.Add(newTab);
            control.Dock = System.Windows.Forms.DockStyle.Fill;

            newTab.Controls.Add(control);
            newTab.Text = title;
        }

        public void AddUserActions(Dictionary<string, EventHandler> userActions)
        {
            foreach (var action in userActions)
            {
                _contextMenu.MenuItems.Add(new MenuItem(action.Key, action.Value));
            }
        }

        public void AddUserAction(string label, EventHandler action)
        {
            _contextMenu.MenuItems.Add(new MenuItem(label, action));
        }

        private void btnActions_Click(object sender, EventArgs e)
        {
            _contextMenu.Show((Button)sender, ((Button)sender).PointToClient(Cursor.Position));
        }
    }
}
