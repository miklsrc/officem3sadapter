﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OfficeCommon.Gui
{
    public partial class InfoForm : Form
    {
        public InfoForm()
        {
            InitializeComponent();
        }

        public void AddProperty(string name, string value)
        {

            gridView1.RowCount++;

            gridView1.Rows[gridView1.RowCount-2].Cells[0].Value = name;
            gridView1.Rows[gridView1.RowCount-2].Cells[1].Value = value;
        }


        public void AddSessionInfo(SessionInfo sessionInfo)
        {
            AddProperty("m3s path", sessionInfo.M3sFileLocation);
        }
    }
}
