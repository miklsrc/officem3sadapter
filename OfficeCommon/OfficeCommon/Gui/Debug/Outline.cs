﻿using System.Windows.Forms;

namespace OfficeCommon.Gui.Debug
{
    public partial class Outline : UserControl
    {
        private SessionInfo _sessionInfo;

        public Outline(SessionInfo sessionInfo)
        {
            this._sessionInfo = sessionInfo;
            InitializeComponent();
        }

        public BrightIdeasSoftware.TreeListView TreeList => this.treeListView1;
    }
}
