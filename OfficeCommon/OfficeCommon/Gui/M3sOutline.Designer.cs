using System.Collections.Generic;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace OfficeCommon.Gui
{
    partial class M3sOutline
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gel�scht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode f�r die Designerunterst�tzung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeListView1 = new BrightIdeasSoftware.TreeListView();
            this.ColLabel = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColId = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColBaseVersion = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColState = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColSync = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListView1
            // 
            this.treeListView1.AllColumns.Add(this.ColLabel);
            this.treeListView1.AllColumns.Add(this.ColId);
            this.treeListView1.AllColumns.Add(this.ColBaseVersion);
            this.treeListView1.AllColumns.Add(this.ColState);
            this.treeListView1.AllColumns.Add(this.ColSync);
            this.treeListView1.CellEditUseWholeCell = false;
            this.treeListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColLabel,
            this.ColId,
            this.ColBaseVersion,
            this.ColState,
            this.ColSync});
            this.treeListView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListView1.Location = new System.Drawing.Point(0, 0);
            this.treeListView1.Name = "treeListView1";
            this.treeListView1.ShowGroups = false;
            this.treeListView1.Size = new System.Drawing.Size(409, 340);
            this.treeListView1.TabIndex = 0;
            this.treeListView1.UseCompatibleStateImageBehavior = false;
            this.treeListView1.View = System.Windows.Forms.View.Details;
            this.treeListView1.VirtualMode = true;
            // 
            // ColLabel
            // 
            this.ColLabel.AspectName = "Label";
            this.ColLabel.Sortable = false;
            this.ColLabel.Text = "Label";
            // 
            // ColId
            // 
            this.ColId.AspectName = "IdString";
            this.ColId.Sortable = false;
            this.ColId.Text = "Id";
            // 
            // ColBaseVersion
            // 
            this.ColBaseVersion.AspectName = "BaseVersionString";
            this.ColBaseVersion.Sortable = false;
            this.ColBaseVersion.Text = "BaseVersion";
            // 
            // ColState
            // 
            this.ColState.AspectName = "M3SNodeState";
            this.ColState.Sortable = false;
            this.ColState.Text = "State";
            // 
            // ColSync
            // 
            this.ColSync.AspectName = "Sync";
            this.ColSync.CheckBoxes = true;
            this.ColSync.Sortable = false;
            this.ColSync.Text = "Sync";
            this.ColSync.Width = 75;
            // 
            // M3sOutline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeListView1);
            this.Name = "M3sOutline";
            this.Size = new System.Drawing.Size(409, 340);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView treeListView1;
        private BrightIdeasSoftware.OLVColumn ColLabel;
        private BrightIdeasSoftware.OLVColumn ColId;
        private BrightIdeasSoftware.OLVColumn ColState;
        private BrightIdeasSoftware.OLVColumn ColSync;
        private OLVColumn ColBaseVersion;
    }
}
