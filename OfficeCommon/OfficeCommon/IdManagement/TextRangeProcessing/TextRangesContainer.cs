﻿using System.Collections.Generic;
using System.Linq;

namespace OfficeCommon.IdManagement.TextRangeProcessing
{
    public class TextRangesContainer<TRangeType>
    {
        protected List<IdRange<TRangeType>> textRanges = new List<IdRange<TRangeType>>();

        public List<IdRange<TRangeType>> TextRanges
        {
            get => textRanges;
            set { textRanges = value; }
        }

        public IdRange<TRangeType> GetIdRangeById(int searchId)
        {
            return textRanges.Find(n => n.Id == searchId);
        }

        public IdRange<TRangeType> GetIdRange(int start, int end)
        {
            return textRanges.Find(n => n.TextRange.Start == start && n.TextRange.End == end);
        }

        public IdRange<TRangeType> GetIdRange(int position)
        {
            return textRanges.Find(n => n.TextRange.Start <= position && n.TextRange.End >= position);
        }

        public void SortTextRanges()
        {
            textRanges = textRanges.OrderBy(o => o.TextRange.Start).ToList();
        }


    }
}
