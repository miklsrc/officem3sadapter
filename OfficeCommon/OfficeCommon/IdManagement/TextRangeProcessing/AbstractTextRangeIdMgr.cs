﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace OfficeCommon.IdManagement.TextRangeProcessing
{
    enum MergeDirection { First, Last };

    /// <summary>
    /// powerpoint&word can address text parts with ranges (1 range = 1 line). 
    /// this ranges can be stored and after some text changes, this ranges still address the "correct" text, like before.
    /// this makes it possible to "remember" special text parts and annotate them with unambiguous IDs.
    /// 
    /// it is important to know "how" powerpoint&word inserts new text, to identify which old range does belong to the new existing text
    /// 
    /// example (line break is \r):
    /// 
    /// S = Range Start, E = Range End
    /// 
    /// 0---------1---------2---------3---------4---------5---------
    /// 012345678901234567890123456789012345678901234567890123456789
    /// text_range_1\rand_this_is_text_range_2\rfallowed_by_range_3
    /// ^            ^^                        ^^                 ^          
    /// S            ES                        ES                 E
    /// 
    /// there are 3 ranges:       range1 Start=0 End=13,       range2 Start=14 End=39,       range3 Start=40 End=58
    /// 
    /// after inserting 4 (ABCD, EFGH, IJKL, MNOP) new Texts (1 text at begin of each line and one in the middle of range3) it looks like this:
    /// 
    /// 0---------1---------2---------3---------4---------5---------6---------7---------
    /// 01234567890123456789012345678901234567890123456789012345678901234567890123456789
    /// ABCDtext_range_1\rEFGHand_this_is_text_range_2\rIJKLfallowed_bIMNOPy_range_3
    /// ^  ^^                ^^                            ^^                      ^          
    /// S  ES                ES                            ES                      E
    /// 
    /// there are 4 ranges:       rangeNew Start=0 End=3,       range1 Start=4 End=21,       range2 Start=22 End=51,       range3 Start=52 End=75
    /// 
    /// range1, range2 and range3 still address the changed text ranges. additionally a new textrange was added (for ABCD).
    /// the problem is: the new inserted text before the lines (ABCD, EFGH, IJKL) are now addressed by the ranges before and not by itself.
    /// 
    /// this class identify this problems and reconstructs the textbase, to ensure that all textranges keep their unambiguous IDs.
    /// </summary>
    public abstract class AbstractTextRangeIdMgr<TRange> : AbstractIdFragmentMgr
    {
        static MergeDirection mergeDirection = MergeDirection.First;

        private IdManager idManager = new IdManager();


        Dictionary<TextRangesContainer<TRange>, List<IdRangeDescription>> _idRangeDescriptions = new Dictionary<TextRangesContainer<TRange>, List<IdRangeDescription>>();

        public AbstractTextRangeIdMgr(IdCounter idCounter)
            : base(idCounter)
        {
        }

        public abstract IdRange<TRange> GetTextRangeById(int searchId);

        protected abstract void DoUpdateFragments();

        protected abstract void PostProcessAssignForcedIds(IdManager idManager);

        protected abstract IdFragmentDescription FindAndRemoveIdRangeAssignment(IdRange<TRange> curRange, IdManager idManager);

        public override void RefreshFragments()
        {
            RefreshFragments(new List<IdFragmentDescription>());
        }

        public void RefreshFragments(List<IdFragmentDescription> idRangeDescs)
        {
            idManager.Clear();
            foreach (var fragmentIdDescription in idRangeDescs)
            {
                idManager.AddForcedIdAssignment(fragmentIdDescription);
            }

            DoUpdateFragments();

            //assign ids for maually inserted fragments
            if (_idRangeDescriptions.Any())
            {
                foreach (var trContainerWithRangeDescriptions in _idRangeDescriptions)
                {
                    TextRangesContainer<TRange> trContainer = trContainerWithRangeDescriptions.Key;
                    List<IdRangeDescription> idRangeDescriptions = trContainerWithRangeDescriptions.Value;
                    AssignIdsToTextRangesContainer(idRangeDescriptions, trContainer);
                }

                _idRangeDescriptions.Clear();
            }

            PostProcessAssignForcedIds(idManager);
        }

        public void AssignIdToTextRange(IdRangeDescription idRangeDescription, TextRangesContainer<TRange> trContainer)
        {
            if (!_idRangeDescriptions.ContainsKey(trContainer))
            {
                _idRangeDescriptions.Add(trContainer, new List<IdRangeDescription>());
            }
            _idRangeDescriptions[trContainer].Add(idRangeDescription);
        }

        protected void DeleteEmptyRanges(TextRangesContainer<TRange> trContainer)
        {
            for (int i = trContainer.TextRanges.Count - 1; i >= 0; i--)
            {
                if (trContainer.TextRanges[i].TextRange.End - trContainer.TextRanges[i].TextRange.Start <= 0)
                {
                    FireIdFragmentRemovedEvent(trContainer.TextRanges[i].Id);
                    trContainer.TextRanges.RemoveAt(i);
                }
            }
        }


        protected List<IdRange<TRange>> AddNewInsertedRanges(TextRangesContainer<TRange> trContainerOld, TextRangesContainer<TRange> trContainerCur)
        {
            List<IdRange<TRange>> newAddedRanges = new List<IdRange<TRange>>();

            foreach (IdRange<TRange> curRange in trContainerCur.TextRanges)
            {
                if (!rangeIsInList(curRange, trContainerOld.TextRanges)) //new range found
                {
                    int newId = GetIdForRange(curRange);

                    //
                    IdRange<TRange> newAddedTextRange = new IdRange<TRange>(curRange.TextRange, newId);
                    trContainerOld.TextRanges.Add(newAddedTextRange);
                    //
                    FireIdFragmentAddedEvent(newId);
                    //
                    newAddedRanges.Add(newAddedTextRange);
                }
            }
            return newAddedRanges;
        }

        private int GetIdForRange(IdRange<TRange> curRange)
        {
            int newId;
            //check if this found idRange should be assign to a forced id
            IdFragmentDescription rangeDesc = FindAndRemoveIdRangeAssignment(curRange, idManager);

            if (rangeDesc != null)
            {
                newId = rangeDesc.Id;
            }
            else
            {
                newId = IdCounter.getNewId();
            }
            return newId;
        }



        private bool rangeIsInList(IdRange<TRange> searchRange, List<IdRange<TRange>> textRanges)
        {
            return textRanges.BinarySearch(searchRange) >= 0;
        }

        /// <summary>
        /// when adding text at the begin of a range, powerpoint&word interprets this as "before" the "old"-range. 
        /// this means powerpoint increases the start-position of the "old"-range with every new inserted character.
        /// 
        /// example:    (before)         ->       ( after inserting new text)
        /// 
        ///          0---------1----              0---------1---------2-------
        ///          012345678901234              0123456789012345678901234567
        ///          This_is_a_range              new_is_a_textThis_is_a_range
        ///          ^             ^              ^            ^             ^   
        ///          startOld=0    endOld=14      startNew=0   startOld=13   endOld=endNew=27
        ///   
        /// as result, the new text does not belong to the "old"-range and powerpoint&word now have a "new"-range, which contains all inserted text.
        /// the problem is, this new range gets added as a new inserted range with a new ID.
        /// 
        /// this method assigns the IDs of the "old"-ranges to the "new"-ranges and deletes all the "old"-ranges.
        /// </summary>
        /// <param name="newAddedRangesPerBoxes">all new inserted ranges</param>
        protected void FixAtRageStartInsertedText(List<IdRange<TRange>> newAddedTrs, TextRangesContainer<TRange> trContainerOld)
        {
            trContainerOld.TextRanges.Sort();
            foreach (IdRange<TRange> newTr in newAddedTrs)
            {

                int indexOfNewTr = trContainerOld.TextRanges.FindIndex(n => n.TextRange.Start == newTr.TextRange.Start && n.TextRange.End == newTr.TextRange.End);

                //check if newRange is last elemente and has no nextOldRange-> this means it is a new inserted range at the end
                if (trContainerOld.TextRanges.Count > indexOfNewTr + 1)
                {
                    IdRange<TRange> nextOldTr = trContainerOld.TextRanges[indexOfNewTr + 1];

                    string newTrText = newTr.TextRange.Text;
                    string nextOldTrText = cutAfterLineBreak(nextOldTr.TextRange.Text);

                    int newTrEnd = newTr.TextRange.End;
                    int nextOldTrEnd = nextOldTr.TextRange.Start + nextOldTrText.Length;

                    if (nextOldTr.TextRange.Start <= newTrEnd && newTrEnd == nextOldTrEnd && newTrText.EndsWith(nextOldTrText))
                    {
                        //assigns the ID of the "old"-ranges to the "new"-range and delete the "old"-ranges.
                        newTr.Id = nextOldTr.Id;
                        trContainerOld.TextRanges.RemoveAt(indexOfNewTr + 1);
                    }
                }
            }
        }

        /// <summary>
        /// when a string contains a line break and this line break is not the terminating last char, this methods cuts away all chars after the first line break
        /// 
        /// this method is required, because in some cases text (which gets inserted at the begin of a (old) range) gets assigned at the end of the (old) previous range after the line break (see fixAtRageStartInsertedText)
        /// 
        /// example:    (before)              ->       ( after inserting new text A at begin of line 2 and new text BC at begin of line 3)
        /// 
        ///          0---------1---------              0---------1---------2--
        ///          01234567890123456789              01234567890123456789012
        ///          First\rSecond\rThird              First\rASecond\rBCThird
        ///          ^     ^^      ^^   ^              ^      ^^        ^^   ^   
        ///          S     ES      ES   E              S      ES        ES   E
        ///                                                     
        ///         text range2 old: 'Second\r'        text range2 old: 'Second\rBC'
        ///         
        ///  ! In my observations this behaviour only occurs, when text gets inserted at the begin of 2 (or more) following ranges in the same updating step !
        /// </summary>
        /// <param name="rangeText"></param>
        /// <returns></returns>
        private string cutAfterLineBreak(string rangeText)
        {
            if (rangeText.Contains(Constants.LINE_BREAK) && !rangeText.EndsWith(Constants.LINE_BREAK))
            {
                rangeText = rangeText.Substring(0, rangeText.IndexOf(Constants.LINE_BREAK) + Constants.LINE_BREAK.Length);
            }
            return rangeText;
        }



        /// <summary>
        /// example: when there are two fallowing old ranges OldR1 and OldR2, they are splittet with a LineBreak-char at the end of OldR1.
        /// after deleting this LineBreak-char, the text of the Fallowing OldR2 jumps up and the both Ranges get merged to a new Range NewR.
        /// 
        /// this method deletes all old ranges involved in the merge and assigns to the new emerged range an ID from a old ranges
        /// </summary>
        protected void combineMergedRanges(TextRangesContainer<TRange> oldTextBox, TextRangesContainer<TRange> curTextBox)
        {

            curTextBox.SortTextRanges();
            oldTextBox.SortTextRanges();

            foreach (IdRange<TRange> curRange in curTextBox.TextRanges)
            {
                List<int> indicesOfMergeParts = isMergedRange(curRange.TextRange, oldTextBox.TextRanges);
                if (indicesOfMergeParts.Count > 1)
                {
                    //save remaining range identifier
                    int oldRangeIdentifier;
                    if (mergeDirection == MergeDirection.First)
                        oldRangeIdentifier = oldTextBox.TextRanges[indicesOfMergeParts.First()].Id;
                    else //==MergeDirection.Last
                        oldRangeIdentifier = oldTextBox.TextRanges[indicesOfMergeParts.Last()].Id;

                    //remove parts which are involved in merge
                    foreach (int indexOfMergePart in Enumerable.Reverse(indicesOfMergeParts))
                    {
                        int movedRangeId = oldTextBox.TextRanges[indexOfMergePart].Id;

                        oldTextBox.TextRanges.RemoveAt(indexOfMergePart);

                        if (movedRangeId != oldRangeIdentifier)
                        {
                            FireIdFragmentRemovedEvent(movedRangeId);
                        }

                    }

                    //add new merged range with old identifier
                    oldTextBox.TextRanges.Insert(indicesOfMergeParts.First(), new IdRange<TRange>(curRange.TextRange, oldRangeIdentifier));

                }
            }

        }

        /// <summary>
        /// return a empty list, when this is not merged, returns the parts which are involved in the merge (indexes of the list)
        /// 
        /// this has the side effect, that empty ranges got removed (because they get merged)
        /// </summary>
        private List<int> isMergedRange(AbstractTextRange<TRange> possibleMergedRange, List<IdRange<TRange>> textRanges)
        {
            List<int> indicesOfMergeParts = new List<int>();
            for (int i = 0; i < textRanges.Count; i++)
            {
                IdRange<TRange> range = textRanges[i];
                if (possibleMergedRange.Start == range.TextRange.Start && possibleMergedRange.Length >= range.TextRange.Length) //must be a merged
                {
                    int counter = i + 1;

                    int mergeLength = possibleMergedRange.Start + possibleMergedRange.Length;
                    while (textRanges.Count > i && textRanges[i].TextRange.End <= mergeLength)
                    {
                        indicesOfMergeParts.Add(i);
                        i++;
                    }
                    break;
                }
            }
            return indicesOfMergeParts;
        }

        protected void CutOffOverlappingRanges(TextRangesContainer<TRange> trContainerOld, TextRangesContainer<TRange> trContainerCur)
        {
            trContainerCur.TextRanges.Sort();
            trContainerOld.TextRanges.Sort();

            if (trContainerCur.TextRanges.Count != trContainerOld.TextRanges.Count)
            {
                throw new Exception("textrange count per textbox does not equal");
            }

            for (int i = 0; i < trContainerCur.TextRanges.Count; i++)
            {
                var curRange = trContainerCur.TextRanges[i].TextRange;
                var oldRange = trContainerOld.TextRanges[i].TextRange;

                if (curRange.Start != oldRange.Start)
                    throw new Exception("should never happen, old and current textrange have different start");

                if (curRange.Length < oldRange.Length) //oldRange contains addionally all stuff inserted after itself
                {
                    //repair: use current range
                    trContainerOld.TextRanges[i].TextRange = trContainerCur.TextRanges[i].TextRange;
                }
            }
        }

        protected void CheckTextRangeLegality(TextRangesContainer<TRange> trContainerOld, TextRangesContainer<TRange> trContainerCur)
        {

            if (trContainerCur.TextRanges.Count != trContainerOld.TextRanges.Count)
            {
                throw new Exception("textrange count per textbox does not equal");
            }

            for (int i = 0; i < trContainerCur.TextRanges.Count; i++)
            {
                var curRange = trContainerCur.TextRanges[i].TextRange;
                var oldRange = trContainerOld.TextRanges[i].TextRange;

                if (curRange.Start != oldRange.Start)
                    throw new Exception("start of ranges does not equal");
                if (curRange.End != oldRange.End)
                    throw new Exception("length of ranges does not equal");
                if (!curRange.Text.Equals(oldRange.Text))
                    throw new Exception("text does not equal");

            }
        }

        private void AssignIdsToTextRangesContainer(List<IdRangeDescription> textRangeDescriptions, TextRangesContainer<TRange> trContainer)
        {
            foreach (var idRangeDescription in textRangeDescriptions)
            {
                int textRangeId = idRangeDescription.Id;
                int start = idRangeDescription.Start;
                int end = idRangeDescription.End;
                IdRange<TRange> idTextRange = trContainer.TextRanges.Find(n => n.TextRange.Start == start);
                idTextRange.Id = textRangeId;
            }
        }


    }
}
