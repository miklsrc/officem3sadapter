﻿using System;

namespace OfficeCommon.IdManagement.TextRangeProcessing
{

    public abstract class IdFragment
    {
        private int id = -1;

        public IdFragment()
        {
        }

        public IdFragment(int id)
        {
            this.id = id;
        }

        public int Id
        {
            get => id;
            set { id = value; }
        }
    }

    public class IdRange<TRange> : IdFragment, IComparable<IdRange<TRange>>
    {

        public AbstractTextRange<TRange> TextRange { get; set; }

        public IdRange(AbstractTextRange<TRange> textRange)
        {
            this.TextRange = textRange;
        }

        public IdRange(AbstractTextRange<TRange> textRange, int id) : base(id)
        {
            this.TextRange = textRange;
        }


        public int CompareTo(IdRange<TRange> other)
        {
            return TextRange.Start - other.TextRange.Start;
        }
    }

    public class IdShape<TShape> : IdFragment
    {
        public TShape Shape { get; set; }

        public IdShape(TShape shape, int id)
            : base(id)
        {
            this.Shape = shape;

        }

    }
}
