﻿using System;

namespace OfficeCommon.IdManagement.TextRangeProcessing
{

    public interface IRange<out TRange>
    {
        TRange Range { get;  }
    }

    public abstract class AbstractTextRange<TRange> : IRange<TRange>
    {

        public abstract int End { get; }

        public abstract int Start { get; }

        public abstract String Text { get; set; }

        public abstract int Length { get; }

        public abstract TRange Range { get; set; }
    }
}
