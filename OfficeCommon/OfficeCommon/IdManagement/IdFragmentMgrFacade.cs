﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OfficeCommon.IdManagement
{
    public abstract class IdFragmentMgrFacade : AbstractIdFragmentMgr
    {
        private List<AbstractIdFragmentMgr> _idFragmentManagers = new List<AbstractIdFragmentMgr>();

        public IdFragmentMgrFacade() : base(new IdCounter())
        {
        }

        public void AddIdFragmentMgr(AbstractIdFragmentMgr idFragmentMgr)
        {
            _idFragmentManagers.Add(idFragmentMgr);

            //delegate add/remove events
            //idFragmentMgr.IdFragmentAdded += FireIdFragmentAddedEvent;
            //idFragmentMgr.IdFragmentRemoved += FireIdFragmentRemovedEvent;

        }

        public override void RefreshFragments()
        {
            foreach (var idFragmentManager in _idFragmentManagers)
            {
                idFragmentManager.RefreshFragments();
            }
        }

        public override HashSet<int> GetAllFragmentIds()
        {
            HashSet<int> allFragmentIds = new HashSet<int>();

            foreach (var idFragmentManager in _idFragmentManagers)
            {
                HashSet<int> gragmentIds = idFragmentManager.GetAllFragmentIds();
                allFragmentIds.UnionWith(gragmentIds);
            }
            return allFragmentIds;
        }

    }
}
