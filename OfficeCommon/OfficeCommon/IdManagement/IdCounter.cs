﻿namespace OfficeCommon.IdManagement
{
    public class IdCounter
    {
        private int counter = 1;

        public int getNewId()
        {
            return counter++;
        }

        public void setCounter(int maxCounter)
        {
            counter = maxCounter + 1;
        }
    }
}
