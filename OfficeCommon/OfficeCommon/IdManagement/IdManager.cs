using System;
using System.Collections.Generic;

namespace OfficeCommon.IdManagement
{
    public class IdManager
    {
        private Dictionary<int, IdFragmentDescription> _forcedIdAssignments =new Dictionary<int, IdFragmentDescription>() ;

        public void Clear()
        {
            _forcedIdAssignments.Clear();
        }

        public List<T> GetForcedIdAssignments<T> ()
        {
            List<T> forcedIdAssignments=new List<T>();

            foreach (int id in _forcedIdAssignments.Keys)
            {
                Object value = _forcedIdAssignments[id];
                if (ReferenceEquals(value.GetType(), typeof (T)))
                {
                    forcedIdAssignments.Add(((T)value));
                }
            }
            return forcedIdAssignments;
        }

        public void AddForcedIdAssignment(IdFragmentDescription idFragmentDescription)
        {
            _forcedIdAssignments.Add(idFragmentDescription.Id, idFragmentDescription);
        }

        public void RemoveForcedIdAssignment(IdFragmentDescription idFragmentDesc)
        {
            if (idFragmentDesc != null)
            {
                _forcedIdAssignments.Remove(idFragmentDesc.Id);
            }
        }
    }
}