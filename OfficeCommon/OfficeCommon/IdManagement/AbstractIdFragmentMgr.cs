﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Office.Interop.Word;

namespace OfficeCommon.IdManagement
{
    public abstract class AbstractIdFragmentMgr
    {
        public IdCounter IdCounter { get; private set; }

        public abstract void RefreshFragments();

        public abstract HashSet<int> GetAllFragmentIds();

        public delegate void IdFragmentAddedHandler(int rangeId);
        public delegate void IdFragmentRemovedHandler(int rangeId);

        public event IdFragmentAddedHandler IdFragmentAdded;
        public event IdFragmentRemovedHandler IdFragmentRemoved;

        public AbstractIdFragmentMgr(IdCounter idCounter)
        {
            IdCounter = idCounter;
        }

        protected void FireIdFragmentAddedEvent(int fragmentId)
        {
            Debug.Print("Fragemnt added:"+ fragmentId);
            if (IdFragmentAdded != null)
            {
                IdFragmentAdded(fragmentId);
            }
        }

        protected void FireIdFragmentRemovedEvent(int fragmentId)
        {
            Debug.Print("Fragemnt removed:" + fragmentId);
            if (IdFragmentRemoved != null)
            {
                IdFragmentRemoved(fragmentId);
            }
        }


    }
}