﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Text;
using System.Security.Cryptography.X509Certificates;

namespace OfficeCommon.IdManagement
{
    public class FragmentPropertiesMngr
    {
        public Dictionary<int, FragmentProperties> AllFragmentProperties { get; } = new Dictionary<int, FragmentProperties>();

        public FragmentProperties GetPropertiesFor(int id)
        {
            if (!AllFragmentProperties.ContainsKey(id))
            {
                AllFragmentProperties[id] = FragmentProperties.CreateDefault();

            }
            return AllFragmentProperties[id];

        }

//        public void PrintDebugInfo()
//        {
//            foreach (var idAndFragmentPropertiese in AllFragmentProperties)
//            {
//                var id = idAndFragmentPropertiese.Key;
//                FragmentProperties fragmentProperties = idAndFragmentPropertiese.Value;
//
//                string str = "";
//                str += "BaseVersion: "+fragmentProperties.BaseVersion;
//                str += " GroupNr: " + fragmentProperties.GroupNr;
//                str += "HeadingLevel: " +  fragmentProperties.HeadingLevel;
//                str += "SlideNr: " + fragmentProperties.SlideNr;
//                str += "Sync: " + fragmentProperties.Sync;
//                Logger.Log("FragmentProperties["+id+"] : "+str, LogLevel.Info);
//            }
//        }
    }

    public class FragmentProperties
    {
        public enum Types { HeadingLevel, SlideNr, GroupNr, Sync, BaseVersion };

        public int HeadingLevel;

        public int SlideNr;

        public int GroupNr;

        public int Sync;

        public int BaseVersion;

        public FragmentProperties(int headingLevel)
        {
            HeadingLevel = headingLevel;
            GroupNr = -1;
            BaseVersion = -1;
            Sync = 1;
        }

        public int GetProperty(Types propertyType)
        {
            switch (propertyType)
            {
                case Types.HeadingLevel:
                    return HeadingLevel;
                case Types.SlideNr:
                    return SlideNr;
                case Types.GroupNr:
                    return GroupNr;
                case Types.Sync:
                    return Sync;
                case Types.BaseVersion:
                    return BaseVersion;
                default:
                    throw new Exception("property id unkown:" + propertyType);
            }
        }

        public void SetProperty(Types propertyType, int value)
        {
            switch (propertyType)
            {
                case Types.HeadingLevel:
                    HeadingLevel=value;
                    return;
                case Types.SlideNr:
                    SlideNr=value;
                    return;
                case Types.GroupNr:
                    GroupNr=value;
                    return;
                case Types.Sync:
                    Sync = value;
                    return;
                case Types.BaseVersion:
                    BaseVersion = value;
                    return;
                default:
                    throw new Exception("property id unkown:" + propertyType);
            }
        }

        public static FragmentProperties CreateDefault()
        {
            return new FragmentProperties(-1);
        }
    }

}
