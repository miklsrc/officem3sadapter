﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Office.Core;

namespace OfficeCommon.IdManagement
{
    public class IdFragmentDescription
    {
        public int Id { get; set; }
    }


    public class IdRangeDescription : IdFragmentDescription, IComparable<IdRangeDescription>
    {
        public int Start { get; set; }
        public int End { get; set; }

        public IdRangeDescription(int start, int end, int id)
        {
            this.Start = start;
            this.End = end;
            this.Id = id;
        }

        public int CompareTo(IdRangeDescription other)
        {
            return this.Start - other.Start;
        }

        public override string ToString()
        {
            return "ID:"+Id+" start:"+Start+" end:"+End;
        }
    }



}
