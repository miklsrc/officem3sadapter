﻿using System.ServiceModel;
using OfficeCommon.Gui;
using OfficeCommon.IdManagement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OfficeCommon.m3s;
using OfficeCommon.m3s.structure;
using OfficeCommon.Service_References.M3s;

namespace OfficeCommon
{
    public class Controller<TInstance, TIdFragmentMgr> where TIdFragmentMgr : AbstractIdFragmentMgr
    {
        protected Dictionary<string, EventHandler> actions = new Dictionary<string, EventHandler>();
        private InstanceAdapter _instanceAdapter;
        private RepositoryInteraction _repositoryInteraction = new RepositoryInteraction();

        public Dictionary<string, EventHandler> GetUserActions()
        {
            return actions;
        }

        public Controller(InstanceAdapter instanceAdapter)
        {
            this._instanceAdapter = instanceAdapter;
            actions.Add("Ping", ActionPing);
//            actions.Add("Session Info", ActionShowSessionInfo);
            actions.Add("Commit", ActionCommitM3SStructBase);
            actions.Add("Update", ActionUpdateM3SStructBase);
        }
                

        private void ActionUpdateM3SStructBase(object sender, EventArgs e)
        {
            StructNode repoStructNode = _repositoryInteraction.UpdateFromRepository(_instanceAdapter);

            _instanceAdapter.UpdateFromM3S(repoStructNode);
        }

        private void ActionCommitM3SStructBase(object sender, EventArgs e)
        {
            _repositoryInteraction.CommitToRepository(_instanceAdapter.M3SDocsStructureBaseRoots, _instanceAdapter);

            //do refresh to refresh the m3soutline data
            ActionUpdateM3SStructBase(null,null); 
        }


        private void ActionShowSessionInfo(object sender, EventArgs e)
        {
            InfoForm infoForm = new InfoForm();
            infoForm.AddSessionInfo(_instanceAdapter.SessionInfo);
            infoForm.Show();
        }

        private void ActionPing(object sender, EventArgs e)
        {
            M3sServiceProxy.Using((delegate(M3sWebServiceClient service)
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                String pong = service.ping();
                watch.Stop();
                MessageBox.Show(pong + " (" + watch.Elapsed + " sec)");
            }));
        }

    }

   
}
