﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using OfficeCommon.IdManagement;
using OfficeCommon.m3s.structure;

namespace OfficeCommon
{

    public abstract class InstanceXmlPersistence
    {
        protected FragmentPropertiesMngr FragmentPropertiesMngr;
        private List<M3SNode> _m3sNodesLinear;
        private SessionInfo _sessionInfo;
        //<name to persist the property,fragmentproperty-type>
        protected Dictionary<string, FragmentProperties.Types> PropertyAccessors = new Dictionary<string, FragmentProperties.Types>();

        protected abstract XElement CreateFragmentIdsXml();
        protected abstract void LoadFragmentIds(XDocument xmlDoc);
        protected abstract string PersistenceType {get;}

        public InstanceXmlPersistence(FragmentPropertiesMngr fragmentPropertiesMngr)
        {
            this.FragmentPropertiesMngr = fragmentPropertiesMngr;
            PropertyAccessors["GroupNr"] = FragmentProperties.Types.GroupNr; //TODO fix loading
            PropertyAccessors["Sync"] = FragmentProperties.Types.Sync;
            PropertyAccessors["BaseVersion"] = FragmentProperties.Types.BaseVersion;

        }

        public void LoadInstanceM3SMetadata(AbstractIdFragmentMgr managerFacade, string xmlFilePath)
        {
            if (!File.Exists(xmlFilePath))
            {
                Debug.WriteLine("instance property file does not exist:" + xmlFilePath);
            }
            XDocument xmlDoc = XDocument.Load(xmlFilePath);
            LoadFragmentIds(xmlDoc);
            LoadFragmentProperties(xmlDoc);

        }

        public void SaveInstanceM3SMetadata(AbstractIdFragmentMgr managerFacade, List<M3SNode> m3SNodesLinear, SessionInfo sessionInfo, string xmlFilePath)
        {
            if (!isCurrentDocReadyToSave(managerFacade))
                return;

            this._m3sNodesLinear = m3SNodesLinear;
            this._sessionInfo = sessionInfo;

            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("instance",
                CreateSessionInfoXmlAttributes(sessionInfo),
                new XAttribute("persistenceType", PersistenceType),
                CreateFragmentIdsXml(),
                CreateFragmentPropertiesXml()
            ));

            xmlDoc.Save(xmlFilePath);

        }

        private void LoadFragmentProperties(XDocument xmlDoc)
        {
            foreach (var fragmentNode in xmlDoc.Descendants("fragment"))
            {
                int id = Convert.ToInt32(fragmentNode.Attribute("id").Value);

                foreach (var propertyNode in fragmentNode.Descendants("property"))
                {

                    string propertyName = propertyNode.Attribute("name").Value;
                    int propertyValue =Convert.ToInt32(propertyNode.Attribute("value").Value);
                    FragmentPropertiesMngr.GetPropertiesFor(id).SetProperty(PropertyAccessors[propertyName], propertyValue);

                }
            }
        }

        private XElement CreateFragmentPropertiesXml()
        {
            XElement fragmentPropertiesRoot = new XElement("properties");

            foreach (KeyValuePair<int, FragmentProperties> fragmentProps in FragmentPropertiesMngr.AllFragmentProperties)
            {
                XElement fragment = new XElement("fragment", new XAttribute("id", fragmentProps.Key.ToString()));
                fragmentPropertiesRoot.Add(fragment);

                foreach (string propertyName in PropertyAccessors.Keys)
                {
                    fragment.Add(new XElement("property",
                        new XAttribute("name", propertyName),
                        new XAttribute("value", FragmentPropertiesMngr.GetPropertiesFor(fragmentProps.Key).GetProperty(PropertyAccessors[propertyName]).ToString()))
                    );
                }
            }

            return fragmentPropertiesRoot;
        }


        //can get overriden in child class
        public virtual bool isCurrentDocReadyToSave(AbstractIdFragmentMgr managerFacade)
        {
            return true;
        }

        public void LoadSessionInfo(SessionInfo sessionInfo, string xmlFilePath)
        {
            XDocument xmlDoc = XDocument.Load(xmlFilePath);
            XElement xmlPresentation = xmlDoc.Descendants("instance").First();
            sessionInfo.M3sFileLocation = xmlPresentation.Attribute("m3spath").Value;
        }

        protected List<XAttribute> CreateFragmentCorePropertiesXmlAttributes(int id)
        {
            M3SNode m3SNode = _m3sNodesLinear.Find(n => n.Id == id);

            List<XAttribute> attributes = new List<XAttribute>();

            attributes.Add(new XAttribute("id", id));

            return attributes;
        }

        protected List<XAttribute> CreateSessionInfoXmlAttributes(SessionInfo sessionInfo)
        {
            List<XAttribute> attributes= new List<XAttribute>();
            attributes.Add(new XAttribute("m3spath", sessionInfo.M3sFileLocation));
            return attributes;
        }

    }
}
