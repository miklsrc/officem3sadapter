﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OfficeCommon
{
    public enum Target { Top, Bottom, Into };

    public class ExtDragNDropBehaviour
    {

        public delegate bool DropAllowedHandler(Tuple<TreeNode, Target> target, TreeNode source);
        public delegate bool BeforeDropHandler(Tuple<TreeNode, Target> target, TreeNode source);
        public delegate void AfterDropHandler(TreeNode dropped);

        public event AfterDropHandler AfterDrop;
        public event DropAllowedHandler DropAllowed;
        public event BeforeDropHandler BeforeDrop;

        private TreeView TreeView = null;
        private Tuple<TreeNode, Target> LastTargetDrop = null;
        public bool ExpandFoldersOnMouseOver { get; set; }

        public ExtDragNDropBehaviour()
        {
            ExpandFoldersOnMouseOver = false;
        }

        public void BindToTreeView(TreeView treeView)
        {
            this.TreeView = treeView;
            LastTargetDrop = null;

            this.TreeView.AllowDrop = true;

            //add treeview handlers
            this.TreeView.DragOver += treeViewDragOver;
            this.TreeView.DragEnter += treeViewDragEnter;
            this.TreeView.ItemDrag += treeViewItemDrag;
            this.TreeView.DragDrop += treeViewDragDrop;
            this.TreeView.DragLeave += treeViewDragLeave;
        }

        public TreeView DetachTreeView()
        {
            //remove treeview handlers
            this.TreeView.DragOver -= treeViewDragOver;
            this.TreeView.DragEnter -= treeViewDragEnter;
            this.TreeView.ItemDrag -= treeViewItemDrag;
            this.TreeView.DragDrop -= treeViewDragDrop;
            this.TreeView.DragLeave -= treeViewDragLeave;

            TreeView returning = TreeView;
            TreeView = null;
            return returning;
        }

        private void treeViewDragLeave(object sender, EventArgs e)
        {
            TreeView.Refresh();
        }

        private void treeViewItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e)
        {
            TreeView.SelectedNode = (TreeNode)e.Item;
            TreeView.DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void treeViewDragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {
            RefreshPlaceholder(LastTargetDrop);
        }

        private void treeViewDragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {
            TreeNode draggedNode = (TreeNode)e.Data.GetData(typeof(TreeNode));

            if (draggedNode != null && LastTargetDrop != null)
            {
                Tuple<TreeNode, Target> dropTarget = LastTargetDrop;

                if (BeforeDrop != null && !BeforeDrop(dropTarget, draggedNode)) //Before Drop Handler
                {
                    TreeView.Refresh();
                }
                else
                {

                    TreeNode NodeOver = dropTarget.Item1;
                    TreeNodeCollection Nodes = null;
                    int targetIndex = 0;

                    if (dropTarget.Item2 == Target.Into)
                    {
                        //targetIndex = 0; already set through default 0
                        Nodes = NodeOver.Nodes;
                    }
                    else
                    {
                        if (NodeOver.Level == 0)
                        {
                            Nodes = TreeView.Nodes;
                        }
                        else
                        {
                            Nodes = NodeOver.Parent.Nodes;
                        }
                        //
                        targetIndex = Nodes.IndexOf(NodeOver);
                        //
                        if (dropTarget.Item2 == Target.Bottom)
                        {
                            targetIndex++;
                        }
                    }

                    Nodes.Insert(targetIndex, (TreeNode)draggedNode.Clone());
                    TreeView.SelectedNode = Nodes[targetIndex];
                    draggedNode.Remove();

                    //After Drop Handler
                    if (AfterDrop != null)
                    {
                        AfterDrop(Nodes[targetIndex]);
                    }
                }
            }
            LastTargetDrop = null;
        }


        private Tuple<TreeNode, Target> evaluateDragOverPosition(TreeNode NodeMoving)
        {
            TreeNode NodeOver = TreeView.GetNodeAt(TreeView.PointToClient(Cursor.Position));

            //deagged mouse not over a node (for example down below the last node, but still over the treeview component)
            if (NodeOver == null)
                return LastTargetDrop;

            if (NodeMoving == NodeOver || ContainsNode(NodeMoving, NodeOver))
                return null;

            int OffsetY = TreeView.PointToClient(Cursor.Position).Y - NodeOver.Bounds.Top;

            if (OffsetY < (NodeOver.Bounds.Height / 3))
            {
                return new Tuple<TreeNode, Target>(NodeOver, Target.Top);
            }
            else if ((NodeOver.Nodes.Count == 0) && OffsetY > (NodeOver.Bounds.Height - (NodeOver.Bounds.Height / 3)))
            {
                return new Tuple<TreeNode, Target>(NodeOver, Target.Bottom);
            }
            else
            {
                if (ExpandFoldersOnMouseOver && NodeOver.Nodes.Count > 0)
                {
                    NodeOver.Expand();
                }
                return new Tuple<TreeNode, Target>(NodeOver, Target.Into);
            }

        }

        private void treeViewDragOver(object sender, DragEventArgs e)
        {
            TreeNode NodeMoving = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
            Tuple<TreeNode, Target> target = evaluateDragOverPosition(NodeMoving);

            bool legal = (target != null) && (DropAllowed == null || DropAllowed(target, NodeMoving)); //check if this is a legal move

            if (!legal)
            {
                target = null;
            }

            bool changed = ((target == null) != (LastTargetDrop == null) || //when target XOR lastTarget is null
                (target != null && (target.Item1 != LastTargetDrop.Item1 || target.Item2 != LastTargetDrop.Item2))); //targetDirection or nodeOver changed

            if (changed)
            {
                e.Effect = legal ? DragDropEffects.Move : DragDropEffects.None;
                RefreshPlaceholder(target);
            }

            LastTargetDrop = target;
        }

        private void RefreshPlaceholder(Tuple<TreeNode, Target> target)
        {
            TreeView.Refresh();
            if (target != null)
            {
                switch (target.Item2)
                {
                    case Target.Top:
                        DrawTopPlaceholders(target.Item1);
                        break;
                    case Target.Bottom:
                        DrawBottomPlaceholders(target.Item1, target.Item1);
                        break;
                    case Target.Into:
                        DrawAddToFolderPlaceholder(target.Item1);
                        break;
                }
            }
        }

        private bool ContainsNode(TreeNode node1, TreeNode node2)
        {
            if (node2.Parent == null) return false;
            if (node2.Parent.Equals(node1)) return true;

            return ContainsNode(node1, node2.Parent);
        }

        /**
         * this does not really return the left bound with image, because i don't know how to get the image width for every case.
         * so i only take the left-bound of the parent node (or 0 when it is a root node)
         * 
         * return -1, when no left-bound can be evaluated
         **/
        private int getLeftBoundWithImage(TreeNode Node)
        {
            if (Node != null)
            {
                if (Node.Parent != null)
                {
                    return Node.Parent.Bounds.Left;
                }
                else if (Node.Level == 0)
                {
                    return 0;
                }
            }
            return -1;
        }

        private void DrawBottomPlaceholders(TreeNode NodeOver, TreeNode ParentDragDrop)
        {
            Graphics g = TreeView.CreateGraphics();

            int LeftPos;
            if (ParentDragDrop != null)
                LeftPos = getLeftBoundWithImage(ParentDragDrop);
            else
                LeftPos = getLeftBoundWithImage(NodeOver);
            int RightPos = TreeView.Width - 4;

            Point[] LeftTriangle = new Point[5]{
                                                   new Point(LeftPos, NodeOver.Bounds.Bottom - 4),
                                                   new Point(LeftPos, NodeOver.Bounds.Bottom + 4),
                                                   new Point(LeftPos + 4, NodeOver.Bounds.Bottom),
                                                   new Point(LeftPos + 4, NodeOver.Bounds.Bottom - 1),
                                                   new Point(LeftPos, NodeOver.Bounds.Bottom - 5)};

            Point[] RightTriangle = new Point[5]{
                                                    new Point(RightPos, NodeOver.Bounds.Bottom - 4),
                                                    new Point(RightPos, NodeOver.Bounds.Bottom + 4),
                                                    new Point(RightPos - 4, NodeOver.Bounds.Bottom),
                                                    new Point(RightPos - 4, NodeOver.Bounds.Bottom - 1),
                                                    new Point(RightPos, NodeOver.Bounds.Bottom - 5)};


            g.FillPolygon(System.Drawing.Brushes.Black, LeftTriangle);
            g.FillPolygon(System.Drawing.Brushes.Black, RightTriangle);
            g.DrawLine(new System.Drawing.Pen(Color.Black, 2), new Point(LeftPos, NodeOver.Bounds.Bottom), new Point(RightPos, NodeOver.Bounds.Bottom));
        }

        private void DrawTopPlaceholders(TreeNode NodeOver)
        {
            Graphics g = TreeView.CreateGraphics();

            int LeftPos = getLeftBoundWithImage(NodeOver);
            int RightPos = TreeView.Width - 4;

            Point[] LeftTriangle = new Point[5]{
												   new Point(LeftPos, NodeOver.Bounds.Top - 4),
												   new Point(LeftPos, NodeOver.Bounds.Top + 4),
												   new Point(LeftPos + 4, NodeOver.Bounds.Y),
												   new Point(LeftPos + 4, NodeOver.Bounds.Top - 1),
												   new Point(LeftPos, NodeOver.Bounds.Top - 5)};

            Point[] RightTriangle = new Point[5]{
													new Point(RightPos, NodeOver.Bounds.Top - 4),
													new Point(RightPos, NodeOver.Bounds.Top + 4),
													new Point(RightPos - 4, NodeOver.Bounds.Y),
													new Point(RightPos - 4, NodeOver.Bounds.Top - 1),
													new Point(RightPos, NodeOver.Bounds.Top - 5)};


            g.FillPolygon(System.Drawing.Brushes.Black, LeftTriangle);
            g.FillPolygon(System.Drawing.Brushes.Black, RightTriangle);
            g.DrawLine(new System.Drawing.Pen(Color.Black, 2), new Point(LeftPos, NodeOver.Bounds.Top), new Point(RightPos, NodeOver.Bounds.Top));

        }

        private void DrawAddToFolderPlaceholder(TreeNode NodeOver)
        {
            Graphics g = TreeView.CreateGraphics();
            int RightPos = NodeOver.Bounds.Right + 6;
            Point[] RightTriangle = new Point[5]{
													new Point(RightPos, NodeOver.Bounds.Y + (NodeOver.Bounds.Height / 2) + 4),
													new Point(RightPos, NodeOver.Bounds.Y + (NodeOver.Bounds.Height / 2) + 4),
													new Point(RightPos - 4, NodeOver.Bounds.Y + (NodeOver.Bounds.Height / 2)),
													new Point(RightPos - 4, NodeOver.Bounds.Y + (NodeOver.Bounds.Height / 2) - 1),
													new Point(RightPos, NodeOver.Bounds.Y + (NodeOver.Bounds.Height / 2) - 5)};

            g.FillPolygon(System.Drawing.Brushes.Black, RightTriangle);
        }

    }
}
